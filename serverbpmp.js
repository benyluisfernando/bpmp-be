const conf = require("./config.json");

const express = require("express"),
  app = express(),
  port = process.env.port || conf.port;
const cors = require("cors");
var ApiLoggingExternalRoute = require("./routes/services/serv_log_external");
var logRoute = require("./routes/services/serv_log");

// BPMP //
var apiBpmpChannel = require("./routes/services/bpmp_channel");
var apiBpmpCompany = require("./routes/services/bpmp_company");
var apiBpmpCompanyReq = require("./routes/services/bpmp_companyreq");
var apiBpmpRules = require("./routes/services/bpmp_rules");
var apiBpmpRulesReq = require("./routes/services/bpmp_rulesreq");
var apiBpmpTrx = require("./routes/services/bpmp_transaction");

var path = require("path");
// const session = require("express-session");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
//-==================== PATH SETTING =================
//app.use(logger('dev'));
app.use(express.static(path.join(__dirname, "public")));

app.use("/komi/v1/external/logging", ApiLoggingExternalRoute);
app.use("/komi/v1/external/logging", ApiLoggingExternalRoute);
app.use("/komi/log", logRoute);

// BPMP //
app.use("/api/channel/", apiBpmpChannel);
app.use("/api/company/", apiBpmpCompany);
app.use("/api/companyreq/", apiBpmpCompanyReq);
app.use("/api/rules/", apiBpmpRules);
app.use("/api/rulesreq/", apiBpmpRulesReq);
app.use("/api/trx/", apiBpmpTrx);

var server = app.listen(port, function () {
  let host = server.address().address;
  let portname = server.address().port;
  console.log("Example server is running in http://%s:%s", host, portname);
});
