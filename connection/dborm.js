const nodemailer = require("nodemailer");
var conf = require("../config.json");

function postgre() {
  return require("knex")({
    client: "db",
    connection: {
      host: conf.dbConfig.db.host,
      database: conf.dbConfig.db.database,
      user: conf.dbConfig.db.user,
      password: conf.dbConfig.db.password,
    },
    pool: {
      min: 2,
      max: 10,
    },
  });
}

function postgreKKT() {
  return require("knex")({
    client: "db",
    version: "7.2",
    connection: {
      host: conf.dbConfig.dbkkt.host,
      database: conf.dbConfig.dbkkt.database,
      user: conf.dbConfig.dbkkt.user,
      password: conf.dbConfig.dbkkt.password,
    },
  });
}

function mssql() {
  return require("knex")({
    client: "mssql",
    connection: {
      host: conf.dbConfig.db.host,
      port: 1433,
      database: conf.dbConfig.db.database,
      user: conf.dbConfig.db.user,
      password: conf.dbConfig.db.password,
    },
  });
}

function mssqlKKT() {
  return require("knex")({
    client: "mssql",
    connection: {
      host: conf.dbConfig.dbkkt.host,
      port: 1433,
      database: conf.dbConfig.dbkkt.database,
      user: conf.dbConfig.dbkkt.user,
      password: conf.dbConfig.dbkkt.password,
    },
  });
}

function conn() {
  if (conf.db == "oracle") {
    return oracle();
  } else if (conf.db == "postgres") {
    return postgre();
  } else if (conf.db == "mssql") {
    return mssql();
  }
}

function connKKT() {
  if (conf.db == "oracle") {
    return oracleKKTORA();
  } else if (conf.db == "postgres") {
    return postgreKKT();
  } else if (conf.db == "mssql") {
    return mssqlKKT();
  }
}

module.exports = {
  conn,
  connKKT,
};
