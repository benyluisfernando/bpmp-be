const express = require("express"),
  app = express();
const router = express.Router();
const knex = require("../../connection/dborm");
const axios = require("axios");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const conn = knex.conn();
router.use(checkAuth);

router.get("/getCompanyTemp", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  console.log(">> GET COMPANY Temp >>");

  try {
    const resp = await conn.select("*").from("b_company");
    console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getCompanyTemp", async (req, res, next) => {
  const authHeader = req.headers.authorization;

  try {
    axios
      .post(
        "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getCompanyTemp/getCompanyTemp",
        {
          auth: { username: "Administrator", password: "manage" },
        }
      )
      .then(async (resp) => {
        console.log(resp);
        res.status(200).json({ status: 200, data: resp.data });
      })
      .catch((err) => {
        console.log(err);
        return;
      });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
