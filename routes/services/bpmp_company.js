const express = require("express"),
  app = express();
const router = express.Router();
const knex = require("../../connection/dborm");
const axios = require("axios");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const conn = knex.conn();
router.use(checkAuth);

router.get("/getCompany", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  console.log(">> GET COMPANY >>");
  try {
    // status
    // 1 = tampil
    // 2 = temp
    const resp = await conn.select("*").from("M_COMPANY").where("status", 1);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getCompanyTemp", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    console.log(">> GET COMPANY TEMP >>");
    const resp = await conn.select("*").from("M_COMPANY").where("status", 2);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getCompany/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    let id = req.params.id;
    const respCompany = await conn
      .select("*")
      .from("M_COMPANY")
      .where("id", id);

    if (respCompany.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: respCompany[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertCompany", async (req, res, next) => {
  var dcodeInfo = req.userData;
  // const change_who = dcodeInfo.id;
  // const idtenant = dcodeInfo.idtenant;
  try {
    console.log("INSERT COMPANY");
    var apps = dcodeInfo.apps[0];
    const {
      COMPANY_ID,
      HOST_CODE,
      COMPANY_NAME,
      CREATE_WHO,
      CREATE_DATE,
      COMPANY_CODE,
      id,
    } = req.body;

    if (apps.withapproval > 0) {
      const resp = await conn("M_COMPANY")
        .insert({
          COMPANY_ID: COMPANY_ID,
          HOST_CODE: HOST_CODE,
          COMPANY_NAME: COMPANY_NAME,
          CREATE_WHO: CREATE_WHO,
          CREATE_DATE: CREATE_DATE,
          COMPANY_CODE: COMPANY_CODE,
          active: 3,
          status: 2,
        })
        .returning(["id"]);
      console.log(resp);
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert COMPANY",
        });
      }
    } else {
      const resp = await conn("COMPANY")
        .insert({
          COMPANY_ID: COMPANY_ID,
          HOST_CODE: HOST_CODE,
          COMPANY_NAME: COMPANY_NAME,
          CREATE_WHO: CREATE_WHO,
          CREATE_DATE: CREATE_DATE,
          COMPANY_CODE: COMPANY_CODE,
          active: 1,
          status: 1,
        })
        .returning(["id"]);
      console.log(resp);
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert COMPANY",
        });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/updateCompany", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    const {
      id,
      companyId,
      companyCode,
      companyName,
      createWho,
      createDate,
      hostCode,
    } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const resp = await conn("M_COMPANY").where("id", id).update({
      COMPANY_ID: companyId,
      COMPANY_CODE: companyCode,
      COMPANY_NAME: companyName,
      CREATE_WHO: createWho,
      CREATE_DATE: createDate,
      HOST_CODE: hostCode,
    });

    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error update M_COMPANY",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.get("/deleteCompany/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  // var dcodeInfo = req.userData;
  let id = req.params.id;
  try {
    let resp = await conn("M_COMPANY").where("id", id).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.post("/getCompany", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  try {
    axios
      .get(
        "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getCompany/getCompany",
        {
          auth: { username: "Administrator", password: "manage" },
        }
      )
      .then(async (resp) => {
        console.log(resp);
        res.status(200).json({ status: 200, data: resp.data });
      })
      .catch((err) => {
        console.log(err);
        return;
      });
  } catch (err) {
    // console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
