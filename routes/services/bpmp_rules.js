const express = require("express"),
  app = express();
const router = express.Router();
const knex = require("../../connection/dborm");
const axios = require("axios");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const conn = knex.conn();
router.use(checkAuth);

router.get("/getRules", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  console.log(">> GET RULES >>");
  try {
    const resp = await conn.select("*").from("M_UBP_RULES");
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getRules/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    let id = req.params.id;
    const respChannel = await conn
      .select("*")
      .from("M_UBP_RULES")
      .where("id", id);

    if (respChannel.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: respChannel[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertRules", async (req, res, next) => {
  var dcodeInfo = req.userData;
  // const change_who = dcodeInfo.id;
  // const idtenant = dcodeInfo.idtenant;
  try {
    // var apps = dcodeInfo.apps[0];
    const {
      ruleId,
      channelId,
      companyId,
      enabledFlag,
      ruleNo,
      labelFormula,
      fieldFormula,
      ruleType,
      ruleComment,
      targetField,
      createWho,
      createDate,
      changeWho,
      changeDate,
    } = req.body;

    const resp = await conn("M_UBP_RULES")
      .insert({
        RULE_ID: ruleId,
        CHANNEL_ID: channelId,
        COMPANY_ID: companyId,
        ENABLED_FLAG: enabledFlag,
        RULE_NO: ruleNo,
        LABEL_FORMULA: labelFormula,
        FIELD_FORMULA: fieldFormula,
        RULE_TYPE: ruleType,
        RULE_COMMENT: ruleComment,
        TARGET_FIELD: targetField,
        CREATE_WHO: createWho,
        CREATE_DATE: createDate,
        CHANGE_WHO: changeWho,
        CHANGE_DATE: changeDate,
      })
      .returning(["id"]);
    console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert M_UBP_RULES ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert M_UBP_RULES" });
  }
});

router.post("/updateRules", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    const {
      id,
      ruleId,
      channelId,
      companyId,
      enabledFlag,
      ruleNo,
      labelFormula,
      fieldFormula,
      ruleType,
      ruleComment,
      targetField,
      createWho,
      createDate,
      changeWho,
      changeDate,
    } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const resp = await conn("M_UBP_RULES").where("id", id).update({
      RULE_ID: ruleId,
      CHANNEL_ID: channelId,
      COMPANY_ID: companyId,
      ENABLED_FLAG: enabledFlag,
      RULE_NO: ruleNo,
      LABEL_FORMULA: labelFormula,
      FIELD_FORMULA: fieldFormula,
      RULE_TYPE: ruleType,
      RULE_COMMENT: ruleComment,
      TARGET_FIELD: targetField,
      CREATE_WHO: createWho,
      CREATE_DATE: createDate,
      CHANGE_WHO: changeWho,
      CHANGE_DATE: changeDate,
    });

    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error update M_UBP_RULES",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.get("/deleteRules/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  // var dcodeInfo = req.userData;
  let id = req.params.id;
  try {
    let resp = await conn("M_UBP_RULES").where("id", id).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.post("/getRules", async (req, res, next) => {
  const authHeader = req.headers.authorization;

  try {
    axios
      .post(
        "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getRules/getRules",
        {
          auth: { username: "Administrator", password: "manage" },
        }
      )
      .then(async (resp) => {
        console.log(resp);
        res.status(200).json({ status: 200, data: resp.data });
      })
      .catch((err) => {
        console.log(err);
        return;
      });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
