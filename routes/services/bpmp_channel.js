const express = require("express"),
  app = express();
const router = express.Router();
const knex = require("../../connection/dborm");
const axios = require("axios");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const conn = knex.conn();
const conf = require("../../config.json");
router.use(checkAuth);

router.get("/getChannel", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  console.log(">> GET CHANNEL >>");
  try {
    // status
    // 1 = tampil
    // 2 = temp
    const resp = await conn.select("*").from("M_CHANNELS").where("status", 1);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getChannelTemp", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    console.log(">> GET CHANNEL TEMP >>");
    const resp = await conn.select("*").from("M_CHANNELS").where("status", 2);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getChannel/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    let id = req.params.id;
    const respChannel = await conn
      .select("*")
      .from("M_CHANNELS")
      .where("id", id);

    if (respChannel.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: respChannel[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertChannel", async (req, res, next) => {
  var dcodeInfo = req.userData;
  // const change_who = dcodeInfo.id;
  // const idtenant = dcodeInfo.idtenant;
  try {
    console.log("INSERT CHANNEL");
    var apps = dcodeInfo.apps[0];
    const {
      CHANNEL_ID,
      MERCHANT_TYPE,
      CHANNEL_NAME,
      CREATE_WHO,
      CREATE_DATE,
      CHANGE_WHO,
      CHANGE_DATE,
      CHANNEL_CODE,
      id,
    } = req.body;

    if (apps.withapproval > 0) {
      const resp = await conn("M_CHANNELS")
        .insert({
          CHANNEL_ID: CHANNEL_ID,
          MERCHANT_TYPE: MERCHANT_TYPE,
          CHANNEL_NAME: CHANNEL_NAME,
          CREATE_WHO: CREATE_WHO,
          CREATE_DATE: CREATE_DATE,
          CHANGE_WHO: CHANGE_WHO,
          CHANGE_DATE: CHANGE_DATE,
          CHANNEL_CODE: CHANNEL_CODE,
          active: 3,
          status: 2,
        })
        .returning(["id"]);
      console.log(resp);
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert CHANNELS",
        });
      }
    } else {
      const resp = await conn("CHANNELS")
        .insert({
          CHANNEL_ID: CHANNEL_ID,
          MERCHANT_TYPE: MERCHANT_TYPE,
          CHANNEL_NAME: CHANNEL_NAME,
          CREATE_WHO: CREATE_WHO,
          CREATE_DATE: CREATE_DATE,
          CHANGE_WHO: CHANGE_WHO,
          CHANGE_DATE: CHANGE_DATE,
          CHANNEL_CODE: CHANNEL_CODE,
          active: 1,
          status: 1,
        })
        .returning(["id"]);
      console.log(resp);
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert CHANNELS",
        });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/updateChannel", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    console.log("UPDATE CHANNEL");
    const {
      id,
      CHANNEL_ID,
      MERCHANT_TYPE,
      CHANNEL_NAME,
      CREATE_WHO,
      CREATE_DATE,
      CHANGE_WHO,
      CHANGE_DATE,
      CHANNEL_CODE,
    } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    var apps = dcodeInfo.apps[0];
    const reqData = {
      idapp: apps.idapp,
      idtenant: dcodeInfo.idtenant,
      cdmenu: "CHANNEL BPMP",
      jsondata: JSON.stringify(req.body),
      status: 2,
      typedata: 7,
    };
    if (apps.withapproval > 0) {
      axios({
        method: "post",
        url: conf.kktserver + "/adm/send/insertApprovalTmp",
        headers: {},
        data: {
          reqData: reqData,
        },
      }).then(async (resp) => {
        console.log("insertApprovalTmp", resp.status);
        if (resp.status == 200) {
          var userObj = resp.data.data[0];
          console.log("userObj", userObj);
          console.log("userObj", userObj.id);
          let idreturn = userObj.id;
          const updateChannel = await conn("M_CHANNELS")
            .returning(["id"])
            .where("id", id)
            .update({
              CHANGE_DATE: conn.fn.now(),
              active: 7,
              status: 2,
              idapproval: idreturn,
            });
          if (updateChannel > 0) {
            var userObj = updateChannel[0];
            res.status(200).json({ status: 200, data: userObj });
          }
        }
        // res.status(200).json({ status: 200, data: resp.data });
      });
    } else {
      const resp = await conn("M_CHANNELS").where("id", id).update({
        CHANNEL_ID: CHANNEL_ID,
        MERCHANT_TYPE: MERCHANT_TYPE,
        CHANNEL_NAME: CHANNEL_NAME,
        CREATE_WHO: CREATE_WHO,
        CREATE_DATE: CREATE_DATE,
        CHANGE_WHO: CHANGE_WHO,
        CHANGE_DATE: CHANGE_DATE,
        CHANNEL_CODE: CHANNEL_CODE,
        status: 1,
        active: 1,
      });
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error Update CHANNELS",
        });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/deleteChannel", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    console.log("Delete Channel");
    var apps = dcodeInfo.apps[0];

    const {
      id,
      CHANNEL_ID,
      MERCHANT_TYPE,
      CHANNEL_NAME,
      CREATE_WHO,
      CREATE_DATE,
      CHANGE_WHO,
      CHANGE_DATE,
      CHANNEL_CODE,
    } = req.body;
    // console.log(req.body);
    const reqData = {
      idapp: apps.idapp,
      idtenant: dcodeInfo.idtenant,
      cdmenu: "CHANNEL BPMP",
      jsondata: JSON.stringify(req.body),
      status: 2,
      typedata: 9,
    };
    if (apps.withapproval > 0) {
      axios({
        method: "post",
        url: conf.kktserver + "/adm/send/insertApprovalTmp",
        headers: {},
        data: {
          reqData: reqData,
        },
      }).then(async (resp) => {
        console.log("insertApprovalTmp", resp.status);
        if (resp.status == 200) {
          var userObj = resp.data.data[0];
          console.log("userObj", userObj);
          console.log("userObj", userObj.id);
          let idreturn = userObj.id;
          const updateChannel = await conn("M_CHANNELS")
            .returning(["id"])
            .where("id", id)
            .update({
              CHANGE_DATE: conn.fn.now(),
              active: 9,
              status: 2,
              idapproval: idreturn,
            });
          if (updateChannel > 0) {
            var userObj = updateChannel[0];
            res.status(200).json({ status: 200, data: userObj });
          }
        }
        // res.status(200).json({ status: 200, data: resp.data });
      });
    } else {
      const resp = await conn("M_CHANNELS").where("id", id).del();
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error Delete CHANNELS",
        });
      }
    }
    // res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/approveChannelTemp", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  var userObj;

  try {
    const { id, oldactive, isactive, idapproval } = req.body;

    let now = new Date();
    now.setHours(now.getHours() + 7);
    console.log(">>>>approveChannelTemp");
    switch (oldactive) {
      case 3:
        const respInsert3 = await conn("M_CHANNELS").where("id", id).update({
          status: 1,
          active: 1,
        });
        if (respInsert3 > 0) {
          res.status(200).json({ status: 200, data: "Success" });
        } else {
          res.status(500).json({ status: 500, data: "Error Update CHANNELS" });
        }
        break;

      case 5:
        break;
      case 7:
        const reqData = {
          id: idapproval,
          status: 2,
        };
        axios({
          method: "get",
          url: conf.kktserver + "/adm/send/getApprovalTmp",
          headers: {},
          data: {
            reqData: reqData,
          },
        }).then(async (resp) => {
          console.log("resp", resp.status);
          if (resp.status == 200) {
            let jsdata = resp.data.data[0];
            let jdata = JSON.parse(jsdata.jsondata);
            console.log("jdata", jdata);

            const cd = moment(jdata.CREATE_DATE).format("YYYY-MM-DD HH:mm:ss");
            const chd = moment(jdata.CHANGE_DATE).format("YYYY-MM-DD HH:mm:ss");

            const respInsert7 = await conn("M_CHANNELS")
              .where("id", id)
              .update({
                CHANNEL_ID: jdata.CHANNEL_ID,
                MERCHANT_TYPE: jdata.MERCHANT_TYPE,
                CHANNEL_NAME: jdata.CHANNEL_NAME,
                CREATE_WHO: jdata.CREATE_WHO,
                CREATE_DATE: cd,
                CHANGE_WHO: jdata.CHANGE_WHO,
                CHANGE_DATE: chd,
                CHANNEL_CODE: jdata.CHANNEL_CODE,
                status: 1,
                active: 1,
              });

            if (respInsert7 > 0) {
              // userObj = resp[0];
              res.status(200).json({ status: 200, data: "Success" });
            } else {
              res
                .status(500)
                .json({ status: 500, data: "Error Update CHANNELS" });
            }
          }
          // res.status(200).json({ status: 200, data: resp.data });
        });

        break;
      case 9:
        const respDelete9 = await conn("M_CHANNELS").where("id", id).del();

        if (respDelete9 > 0) {
          res.status(200).json({ status: 200, data: "Success" });
        } else {
          res.status(500).json({
            status: 500,
            data: "Error Update CHANNELS",
          });
        }
        break;
      default:
      // code block
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/rejectChannelTemp", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  var userObj;

  try {
    const { id, oldactive, isactive, idapproval } = req.body;

    let now = new Date();
    now.setHours(now.getHours() + 7);
    console.log(">>>>approveChannelTemp");
    // console.log(req.body);
    switch (oldactive) {
      case 3:
        const respInsert3 = await conn("M_CHANNELS").where("id", id).del();
        if (respInsert3 > 0) {
          res.status(200).json({ status: 200, data: "Success" });
        } else {
          res.status(500).json({ status: 500, data: "Error Update CHANNELS" });
        }
        break;

      case 5:
        break;
      case 7:
        const respInsert7 = await conn("M_CHANNELS").where("id", id).update({
          status: 1,
          active: 1,
        });
        if (respInsert7 > 0) {
          res.status(200).json({ status: 200, data: "Success" });
        } else {
          res.status(500).json({ status: 500, data: "Error Update CHANNELS" });
        }

        break;
      case 9:
        const respDelete9 = await conn("M_CHANNELS").where("id", id).update({
          status: 1,
          active: 1,
        });
        if (respDelete9 > 0) {
          res.status(200).json({ status: 200, data: "Success" });
        } else {
          res.status(500).json({
            status: 500,
            data: "Error Update CHANNELS",
          });
        }
        break;
      default:
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getChannelTemp/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    console.log("VIEW DATA");
    const id = req.params.id;
    const reqData = {
      id: req.params.id,
      status: 2,
    };
    axios({
      method: "get",
      url: conf.kktserver + "/adm/send/getApprovalTmp",
      headers: {},
      data: {
        reqData: reqData,
      },
    }).then(async (resp) => {
      console.log("resp", resp.status);
      if (resp.status == 200) {
        let jsdata = resp.data.data[0];
        let jdata = JSON.parse(jsdata.jsondata);
        console.log("jdata", jdata);
        res.status(200).json({ status: 200, data: jdata });
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: {} });
        }, 500);
      }
      // res.status(200).json({ status: 200, data: resp.data });
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getChannel", async (req, res, next) => {
  const authHeader = req.headers.authorization;

  try {
    axios
      .post(
        "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getChannel/getChannel",
        {
          auth: { username: "Administrator", password: "manage" },
        }
      )
      .then(async (resp) => {
        console.log(resp);
        res.status(200).json({ status: 200, data: resp.data });
      });
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getTrxApprovalTmp", async (req, res, next) => {
  const authHeader = req.headers.authorization;

  try {
    // axios.post("http://localhost:3000/adm/send/trxApprovalTmp", {
    //   auth: { username: "Administrator", password: "manage" },
    // });
    let resp2 = {
      id: "10055",
      CHANNEL_ID: "1",
      MERCHANT_TYPE: "2",
      CHANNEL_NAME: "Default",
      CREATE_WHO: "UPDATE",
      CREATE_DATE: "2021-12-21",
      CHANGE_WHO: "admin",
      CHANGE_DATE: "2021-12-21",
      CHANNEL_CODE: "5",
    };

    axios({
      method: "post",
      url: conf.kktserver + "/adm/send/trxApprovalTmp",
      headers: {},
      data: {
        resp2,
      },
    }).then(async (resp) => {
      // console.log(resp);
      res.status(200).json({ status: 200, data: resp.data });
    });
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getChannelTemp", async (req, res, next) => {
  const authHeader = req.headers.authorization;

  try {
    axios
      .post(
        "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getChannelTemp/getChannelTemp",
        {
          auth: { username: "Administrator", password: "manage" },
        }
      )
      .then(async (resp) => {
        console.log(resp);
        res.status(200).json({ status: 200, data: resp.data });
      })
      .catch((err) => {
        console.log(err);
        return;
      });
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
module.exports = router;
