const express = require("express"),
  app = express();
const router = express.Router();
const knex = require("../../connection/dborm");
const axios = require("axios");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const conn = knex.conn();
router.use(checkAuth);

router.get("/getTransaction", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  console.log(">> GET CHANNEL >>");
  try {
    const resp = await conn.select("*").from("LOG_UBP_TRX");
    if (resp.length > 0) {
      // res.status(200).json({ status: 200, data: resp });
      res.status(200).json({ status: 200, data: { results: resp } });
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    //console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getTransactionByParam", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    const { startDate, endDate, status, channel, trxtype, logUbpTrxId } =
      req.body;
    const respChannel = await conn
      .select("*")
      .from("LOG_UBP_TRX")
      .modify(function (queryBuilder) {
        if (startDate && endDate) {
          queryBuilder.whereBetween("TRX_TIMESTAMP", [startDate, endDate]);
        }
        if (logUbpTrxId) {
          queryBuilder.where("LOG_UBP_TRX_ID", logUbpTrxId);
        }
      });

    if (respChannel.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: respChannel });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getTransaction/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    let id = req.params.id;
    const respChannel = await conn
      .select("*")
      .from("M_CHANNELS")
      .where("id", id);

    if (respChannel.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: respChannel[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertTransaction", async (req, res, next) => {
  var dcodeInfo = req.userData;
  // const change_who = dcodeInfo.id;
  // const idtenant = dcodeInfo.idtenant;
  try {
    // var apps = dcodeInfo.apps[0];
    const {
      channelId,
      merchantType,
      channelName,
      createWho,
      createDate,
      changeWho,
      changeDate,
      channelCode,
    } = req.body;

    const resp = await conn("M_CHANNELS")
      .insert({
        CHANNEL_ID: channelId,
        MERCHANT_TYPE: merchantType,
        CHANNEL_NAME: channelName,
        CREATE_WHO: createWho,
        CREATE_DATE: createDate,
        CHANGE_WHO: changeWho,
        CHANGE_DATE: changeDate,
        CHANNEL_CODE: channelCode,
      })
      .returning(["id"]);
    console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert M_CHANNELS ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert M_CHANNELS" });
  }
});

router.post("/updateTransaction", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    const {
      id,
      channelId,
      merchantType,
      channelName,
      createWho,
      createDate,
      changeWho,
      changeDate,
      channelCode,
    } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const resp = await conn("M_CHANNELS").where("id", id).update({
      CHANNEL_ID: channelId,
      MERCHANT_TYPE: merchantType,
      CHANNEL_NAME: channelName,
      CREATE_WHO: createWho,
      CREATE_DATE: createDate,
      CHANGE_WHO: changeWho,
      CHANGE_DATE: changeDate,
      CHANNEL_CODE: channelCode,
    });

    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error update M_CHANNELS",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.get("/deleteTransaction/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  // var dcodeInfo = req.userData;
  let id = req.params.id;
  try {
    let resp = await conn("M_CHANNELS").where("id", id).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
