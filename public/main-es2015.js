(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+G24":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/forgotpassword/resetpassword/resetpassword.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ResetpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpasswordComponent", function() { return ResetpasswordComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/forgotpassword/forgotpassword.service */ "Jdzd");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/button */ "jIHw");
















function ResetpasswordComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ResetpasswordComponent_div_13_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ResetpasswordComponent_div_13_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Password not valid");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ResetpasswordComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ResetpasswordComponent_div_13_span_1_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ResetpasswordComponent_div_13_span_2_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.validatePass);
} }
function ResetpasswordComponent_div_19_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Confirm Password does not match with password");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ResetpasswordComponent_div_19_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Confirmed Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ResetpasswordComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ResetpasswordComponent_div_19_span_1_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ResetpasswordComponent_div_19_span_2_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.confirmPassword.errors.mustMatch);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.confirmPassword.errors.required);
} }
function ResetpasswordComponent_ng_template_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "360px" }; };
class ResetpasswordComponent {
    constructor(messageService, sessionStorage, route, activatedRoute, formBuilder, backend, authservice, forgotpasswordService, location) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.forgotpasswordService = forgotpasswordService;
        this.location = location;
        this.blockedDocument = false;
        this.password = '';
        this.confirmPassword = '';
        this.errorMsg = '';
        this.isProcess = false;
        this.submitted = false;
        this.sama = false;
        this.isPasswordSame = true;
        this.isValidatePass = true;
        this.id = '';
        this.cek = '';
    }
    ngOnInit() {
        this.userForm = this.formBuilder.group({
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        }, { validator: this.checkPassword('password', 'confirmPassword') });
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.userForm.controls;
    }
    get userFormControl() {
        return this.userForm.controls;
    }
    checkPassword(controlName, matchingControlName) {
        return (formGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
                this.isPasswordSame = matchingControl.status == 'VALID' ? true : false;
            }
        };
    }
    onSubmit(data) {
        var _a, _b;
        this.submitted = true;
        this.cek = this.userForm.controls.confirmPassword.status;
        // console.log('this.userForm', this.userForm.controls.confirmPassword.status);
        console.log('this.userForm', this.userForm);
        if (this.cek == 'INVALID') {
            this.submitted = true;
        }
        else {
            this.activatedRoute.params.subscribe((paramsId) => {
                this.id = paramsId.id;
            });
            let payload;
            payload = {
                password: (_a = this.userForm.get('password')) === null || _a === void 0 ? void 0 : _a.value,
                confirmPassword: (_b = this.userForm.get('confirmPassword')) === null || _b === void 0 ? void 0 : _b.value,
                id: this.id,
            };
            // console.log('payload ::', payload);
            this.forgotpasswordService
                .reset(payload)
                .subscribe((resp) => {
                console.log(resp);
                if (resp.status === 200) {
                    this.showTopCenterInfo('Password has beed changed');
                    setTimeout(() => {
                        this.route.navigate(['/auth/login']);
                    }, 4000);
                }
                else if (resp.status === 201) {
                    this.showTopCenterErr('Link Expried');
                    setTimeout(() => {
                        this.route.navigate(['/auth/login']);
                    }, 4000);
                }
                else if (resp.status === 422) {
                    console.log('cek');
                    const validateControl = this.userForm.controls.password;
                    validateControl.setErrors({ validatePass: true });
                    this.isValidatePass =
                        validateControl.status == 'VALID' ? true : false;
                }
            });
        }
        //  this.blockDocument();
    }
    showTopCenterInfo(message) {
        this.messageService.add({
            severity: 'info',
            summary: 'Confirmed',
            detail: message,
        });
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: message,
        });
    }
    blockDocument() {
        this.blockedDocument = true;
        //  setTimeout(() => {
        //      this.blockedDocument = false;
        //      this.showTopCenterErr("Invalid user and password!")
        //  }, 3000);
    }
}
ResetpasswordComponent.ɵfac = function ResetpasswordComponent_Factory(t) { return new (t || ResetpasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_7__["ForgotpasswordService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"])); };
ResetpasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ResetpasswordComponent, selectors: [["app-resetpassword"]], decls: 22, vars: 7, consts: [[3, "target", "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], [1, "form", 3, "formGroup", "ngSubmit"], ["header", "Please Insert for Reset Password", "styleClass", "p-card-shadow p-header-w50"], ["pTemplate", "header"], [1, "p-fluid"], [1, "p-field"], ["for", "password", 1, "labelpb"], ["id", "password", "name", "password", "type", "password", "required", "", "formControlName", "password", "pInputText", ""], ["class", "p-field", 4, "ngIf"], [2, "height", "0.3rem"], ["for", "confirmPassword", 1, "labelpb"], ["id", "confirmPassword", "type", "password", "formControlName", "confirmPassword", "pInputText", ""], ["pTemplate", "footer"], [1, "p-text-center"], ["alt", "tai", "src", "assets/logos/komiportal.png"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], [1, "p-text-right"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Login", 1, "p-primary-btn"]], template: function ResetpasswordComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-blockUI", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function ResetpasswordComponent_Template_form_ngSubmit_5_listener($event) { return ctx.onSubmit($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ResetpasswordComponent_ng_template_7_Template, 2, 0, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "New Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, ResetpasswordComponent_div_13_Template, 3, 2, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Confirmed Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, ResetpasswordComponent_div_19_Template, 3, 2, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, ResetpasswordComponent_ng_template_20_Template, 2, 0, "ng-template", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.userForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](6, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.confirmPassword.errors);
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_9__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_11__["Card"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__["InputText"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_13__["Messages"], primeng_button__WEBPACK_IMPORTED_MODULE_14__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNldHBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIn0= */", "body[_ngcontent-%COMP%] {\n        background: #007dc5\n    }\n\n    section[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%)\n    }"] });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Volumes/Yosyosaa/REPO/Krakatoa/Apk BPMP/FIX/bpmp-fe/src/main.ts */"zUnb");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "1LmZ":
/*!**********************************************!*\
  !*** ./src/app/pages/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");






const _c0 = function () { return { "padding": "5px!important" }; };
function HomeComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_div_9_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const item_r2 = ctx.$implicit; const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.showDialog(item_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/logos/", item_r2.appname, ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r2.applabel);
} }
function HomeComponent_ng_template_14_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_ng_template_14_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_ng_template_14_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.switchApp(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function () { return { width: "20vw" }; };
class HomeComponent {
    constructor(authservice) {
        this.authservice = authservice;
        this.appsByLiscense = [];
        this.display = false;
        this.userInfo = {};
        this.appIdSelected = "0";
        this.appLabelSelected = "unknown";
        this.appLabelRouteLink = "unknown";
        this.tokenID = "";
    }
    ngOnInit() {
        this.authservice.whoAmi().subscribe((value) => {
            console.log(">>> User Info : " + JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            if (this.userInfo.apps.length > 0) {
                this.appsByLiscense = this.userInfo.apps;
            }
        });
    }
    showDialog(payload) {
        // this.appIdSelected = this.userInfo.leveltenant == "0"?  payload.id_application:payload.idapp;
        console.log(">>>>>>> " + JSON.stringify(payload));
        this.appIdSelected = payload.id_application;
        this.appLabelSelected = payload.applabel;
        this.appLabelRouteLink = payload.routelink;
        // console.log(">>>>>>> Payload "+this.appIdSelected);
        // this.comparentchildservice.publish('call-parent', payload);
        this.display = true;
    }
    switchApp() {
        this.display = false;
        var payloadNumber = +this.appIdSelected;
        window.open(this.appLabelRouteLink + "/" + this.tokenID);
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 15, vars: 9, consts: [[1, "wrapper"], [2, "padding-left", "40px", "padding-right", "40px"], [2, "font-weight", "400!important"], [1, "p-grid"], [1, "p-col-12"], ["class", "p-col-1", 3, "style", 4, "ngFor", "ngForOf"], [2, "height", "20px"], ["header", "Switch Application", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [1, "p-col-1"], ["href", "javascript:void(0);", 2, "text-decoration", "none", 3, "click"], [1, "col"], [1, "row", "p-text-center"], ["alt", "Card", 2, "width", "110px", "padding", "15px 0 0 0", 3, "src"], [1, "row", "p-text-center", "btntitle"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h3", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Welcome to Krakatoa");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Choose your applications");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, HomeComponent_div_9_Template, 7, 5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p-dialog", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function HomeComponent_Template_p_dialog_visibleChange_11_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, HomeComponent_ng_template_14_Template, 2, 0, "ng-template", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.appsByLiscense);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Open \"", ctx.appLabelSelected, "\" application?, you could log of your current login from that application.");
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], primeng_dialog__WEBPACK_IMPORTED_MODULE_3__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_4__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_5__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "42A2":
/*!**********************************************************!*\
  !*** ./src/app/services/utils/aclmenuchecker.service.ts ***!
  \**********************************************************/
/*! exports provided: AclmenucheckerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AclmenucheckerService", function() { return AclmenucheckerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


class AclmenucheckerService {
    constructor() {
        this.allAcl = [];
        this.getAclMenu = (rlink) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return yield this.allAcl.find(obj => {
                // console.log(JSON.stringify(obj));
                return obj;
            });
        });
        // getAclMenu = async (sidesmenu) => {
        //   this.allAcl = []
        //   return Promise.all(sidesmenu.map(item => this.doSomethingAsync(item)))
        // }
        this.setAllMenus = (sidesmenu) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.allAcl = [];
            return Promise.all(sidesmenu.map(item => this.doSomethingAsync(item)));
        });
        this.doSomethingAsync = (item) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return this.functionPopulateACL(item);
        });
        this.functionPopulateACL = item => {
            // console.log(item.items);
            let menupermodule = item.items;
            menupermodule.map(minimenu => {
                let obj = { routelink: minimenu.routerLink, acl: minimenu.acl };
                this.allAcl.push(obj);
            });
            // this.allAcl.push(item.items);
            return Promise.resolve('ok');
        };
    }
    getAllMenus() {
        return this.allAcl;
    }
    getAclMenu1(rlink) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(">>>>>>>>> " + rlink);
            yield this.allAcl.find(obj => {
                // console.log(JSON.stringify(obj));
                return obj;
            });
        });
    }
}
AclmenucheckerService.ɵfac = function AclmenucheckerService_Factory(t) { return new (t || AclmenucheckerService)(); };
AclmenucheckerService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AclmenucheckerService, factory: AclmenucheckerService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "4ei0":
/*!*******************************************************!*\
  !*** ./src/app/services/root/applications.service.ts ***!
  \*******************************************************/
/*! exports provided: ApplicationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsService", function() { return ApplicationsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class ApplicationsService {
    constructor(service) {
        this.service = service;
        this.allApplications = [];
        this.applications = [];
    }
    retriveAppByTenant() {
        const url = 'adm/apps/appsbytenant';
        return this.service.get(url);
    }
    retriveAppByTenantAndOrgId(id) {
        const url = `adm/apps/retriveAppByTenantAndOrgId/${id}`;
        return this.service.get(url);
    }
    clearData() {
        this.allApplications = [];
        this.applications = [];
    }
}
ApplicationsService.ɵfac = function ApplicationsService_Factory(t) { return new (t || ApplicationsService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
ApplicationsService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ApplicationsService, factory: ApplicationsService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "4nGI":
/*!******************************************************!*\
  !*** ./src/app/services/root/usermanager.service.ts ***!
  \******************************************************/
/*! exports provided: UsermanagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsermanagerService", function() { return UsermanagerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class UsermanagerService {
    constructor(service) {
        this.service = service;
    }
    retriveProfile(id) {
        const url = 'adm/profile/' + id;
        return this.service.get(url);
    }
    putPassword(payload) {
        const url = 'adm/profile/changepassword';
        return this.service.put(url, payload);
    }
    insertByAdmin(payload) {
        const url = 'adm/umanager/insertbyadmin';
        return this.service.post(url, payload);
    }
    insertBySuper(payload) {
        const url = 'adm/umanager/insertbysuper';
        return this.service.post(url, payload);
    }
    updatebySuper(payload) {
        const url = 'adm/umanager/updatebysupperuser';
        return this.service.post(url, payload);
    }
    updatebyAdmin(payload) {
        const url = 'adm/umanager/updatebyadmin';
        return this.service.post(url, payload);
    }
    updatebyAdminActive(payload) {
        const url = 'adm/umanager/updatebyadminactive';
        return this.service.post(url, payload);
    }
    // retriveUsers() {
    //   const url = 'adm/umanager/retriveusers';
    //   return this.service.get(url);
    // }
    retriveUsers() {
        const url = 'adm/umanager/retriveusersadmin';
        return this.service.get(url);
    }
    retriveUsersById(id) {
        const url = 'adm/umanager/retriveusersbyid/' + id;
        return this.service.get(url);
    }
    deleteUser(payload) {
        // console.log("HAPUS "+JSON.stringify(payload));
        const url = `adm/umanager/deletebysuper/${payload.user.id} `;
        return this.service.get(url);
    }
    deleteUserByAdmin(payload) {
        // console.log("HAPUS "+JSON.stringify(payload));
        const url = `adm/umanager/deletebyadmin/${payload.user.id} `;
        return this.service.get(url);
    }
}
UsermanagerService.ɵfac = function UsermanagerService_Factory(t) { return new (t || UsermanagerService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
UsermanagerService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: UsermanagerService, factory: UsermanagerService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "6t8E":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/bpmp/company/companydetail/companydetail.component.ts ***!
  \*****************************************************************************/
/*! exports provided: CompanydetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanydetailComponent", function() { return CompanydetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/root/usermanager.service */ "4nGI");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/forgotpassword/forgotpassword.service */ "Jdzd");
/* harmony import */ var src_app_services_servbpmp_company_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/servbpmp/company.service */ "pjdP");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/button */ "jIHw");

















function CompanydetailComponent_form_3_div_10_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Company ID is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function CompanydetailComponent_form_3_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, CompanydetailComponent_form_3_div_10_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r1.f.companyId.errors.required);
} }
function CompanydetailComponent_form_3_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Company Code is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function CompanydetailComponent_form_3_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, CompanydetailComponent_form_3_div_16_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r2.f.companyCode.errors.required);
} }
function CompanydetailComponent_form_3_div_22_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Company Name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function CompanydetailComponent_form_3_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, CompanydetailComponent_form_3_div_22_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.f.companyName.errors.required);
} }
function CompanydetailComponent_form_3_div_29_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Created Date is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function CompanydetailComponent_form_3_div_29_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, CompanydetailComponent_form_3_div_29_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r4.f.createDate.errors.required);
} }
function CompanydetailComponent_form_3_div_35_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Host Code is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function CompanydetailComponent_form_3_div_35_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, CompanydetailComponent_form_3_div_35_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r5.f.hostCode.errors.required);
} }
function CompanydetailComponent_form_3_div_41_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Created Who is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function CompanydetailComponent_form_3_div_41_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, CompanydetailComponent_form_3_div_41_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r6.f.createWho.errors.required);
} }
function CompanydetailComponent_form_3_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "form", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngSubmit", function CompanydetailComponent_form_3_Template_form_ngSubmit_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r13.onSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "p-card", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "label", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, "Company ID * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "input", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](10, CompanydetailComponent_form_3_div_10_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](11, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "label", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](14, "Company Code * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](15, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](16, CompanydetailComponent_form_3_div_16_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](17, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "label", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](20, "Company Name * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](21, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](22, CompanydetailComponent_form_3_div_22_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](23, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](24, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](25, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](26, "label", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](27, "Created Date * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](28, "input", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](29, CompanydetailComponent_form_3_div_29_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](30, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](31, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](32, "label", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](33, "Host Code * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](34, "input", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](35, CompanydetailComponent_form_3_div_35_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](36, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](37, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](38, "label", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](39, "Created Who * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](40, "input", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](41, CompanydetailComponent_form_3_div_41_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](42, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](43, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](44, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](45, "button", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("formGroup", ctx_r0.groupForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.companyId.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.companyCode.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.companyName.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.createDate.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.hostCode.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.createWho.errors);
} }
class CompanydetailComponent {
    constructor(router, activatedRoute, formBuilder, umService, authservice, filterService, messageService, location, verifikasiService, companyService) {
        var _a;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.umService = umService;
        this.authservice = authservice;
        this.filterService = filterService;
        this.messageService = messageService;
        this.location = location;
        this.verifikasiService = verifikasiService;
        this.companyService = companyService;
        this.extraInfo = {};
        this.isEdit = false;
        this.userId = null;
        this.stateOptions = [];
        this.stateOptionsEdit = [];
        this.leveltenant = 0;
        this.userInfo = {};
        this.selectedApps = [];
        this.tokenID = "";
        this.submitted = false;
        this.orgsData = [];
        this.appInfoActive = {};
        this.orgSuggest = {};
        this.user = {};
        this.formatedOrg = [];
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
        console.log(">>>>>>>>>>> " + this.extraInfo);
        console.log(checkurl);
        if (checkurl)
            this.isEdit = true;
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [
            {
                label: "Company Management",
                command: (event) => {
                    this.location.back();
                },
                url: "",
            },
            { label: this.isEdit ? "Edit data" : "Add data" },
        ];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.leveltenant = this.userInfo.leveltenant;
            this.groupForm = this.formBuilder.group({
                companyId: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                companyCode: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                companyName: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                createWho: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                createDate: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                hostCode: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            });
            if (this.isEdit) {
                if (this.activatedRoute.snapshot.paramMap.get("id")) {
                    this.userId = this.activatedRoute.snapshot.paramMap.get("id");
                    this.companyService
                        .getCompany(this.userId)
                        .subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        console.log("Data edit user " + JSON.stringify(result.data));
                        let cDate = result.data.CREATE_DATE;
                        var createDate = moment__WEBPACK_IMPORTED_MODULE_2__(cDate).utc().format("YYYY-MM-DD");
                        this.user.companyId = result.data.COMPANY_ID;
                        this.user.companyCode = result.data.COMPANY_CODE;
                        this.user.companyName = result.data.COMPANY_NAME;
                        this.user.createWho = result.data.CREATE_WHO;
                        this.user.createDate = createDate;
                        this.user.hostCode = result.data.HOST_CODE;
                        this.groupForm.patchValue({
                            companyId: this.user.companyId,
                            companyCode: this.user.companyCode,
                            companyName: this.user.companyName,
                            createWho: this.user.createWho,
                            createDate: this.user.createDate,
                            hostCode: this.user.hostCode,
                        });
                        this.groupForm.controls["companyId"].disable();
                        console.log(this.user);
                    }));
                }
            }
        });
    }
    get f() {
        return this.groupForm.controls;
    }
    onSubmit() {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
        this.submitted = true;
        // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
        if (this.groupForm.valid) {
            var groupacl = (_a = this.groupForm.get("orgobj")) === null || _a === void 0 ? void 0 : _a.value;
            let payload = {};
            if (!this.isEdit) {
                payload = {
                    companyId: (_b = this.groupForm.get("companyId")) === null || _b === void 0 ? void 0 : _b.value,
                    companyCode: (_c = this.groupForm.get("companyCode")) === null || _c === void 0 ? void 0 : _c.value,
                    companyName: (_d = this.groupForm.get("companyName")) === null || _d === void 0 ? void 0 : _d.value,
                    createWho: (_e = this.groupForm.get("createWho")) === null || _e === void 0 ? void 0 : _e.value,
                    createDate: (_f = this.groupForm.get("createDate")) === null || _f === void 0 ? void 0 : _f.value,
                    hostCode: (_g = this.groupForm.get("hostCode")) === null || _g === void 0 ? void 0 : _g.value,
                };
                console.log(">>>>>>>> payload " + JSON.stringify(payload));
                this.companyService.insertCompany(payload).subscribe((result) => {
                    if (result.status === 200) {
                        this.location.back();
                    }
                }, (err) => {
                    console.log(err);
                    this.showTopCenterErr(err.error.data);
                });
            }
            else {
                payload = {
                    id: this.userId,
                    companyId: (_h = this.groupForm.get("companyId")) === null || _h === void 0 ? void 0 : _h.value,
                    companyCode: (_j = this.groupForm.get("companyCode")) === null || _j === void 0 ? void 0 : _j.value,
                    companyName: (_k = this.groupForm.get("companyName")) === null || _k === void 0 ? void 0 : _k.value,
                    createWho: (_l = this.groupForm.get("createWho")) === null || _l === void 0 ? void 0 : _l.value,
                    createDate: (_m = this.groupForm.get("createDate")) === null || _m === void 0 ? void 0 : _m.value,
                    hostCode: (_o = this.groupForm.get("hostCode")) === null || _o === void 0 ? void 0 : _o.value,
                };
                console.log(">>>>>>>> payload " + JSON.stringify(payload));
                this.companyService
                    .updateCompany(payload)
                    .subscribe((result) => {
                    // console.log(">>>>>>>> return "+JSON.stringify(result));
                    if (result.status === 200) {
                        this.location.back();
                    }
                });
            }
        }
        console.log(this.groupForm.valid);
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: "error",
            summary: "Error",
            detail: message,
        });
    }
}
CompanydetailComponent.ɵfac = function CompanydetailComponent_Factory(t) { return new (t || CompanydetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_5__["UsermanagerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["FilterService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_9__["ForgotpasswordService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_servbpmp_company_service__WEBPACK_IMPORTED_MODULE_10__["CompanyService"])); };
CompanydetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: CompanydetailComponent, selectors: [["app-companydetail"]], decls: 4, vars: 3, consts: [[3, "model", "home"], [1, "wrapperinside"], ["style", "padding: 2px;", 3, "formGroup", "ngSubmit", 4, "ngIf"], [2, "padding", "2px", 3, "formGroup", "ngSubmit"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "companyId", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "companyId", "formControlName", "companyId", "id", "companyId", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "p-field", 4, "ngIf"], [2, "height", "15px"], ["for", "companyCode", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "companyCode", "formControlName", "companyCode", "id", "companyCode", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "companyName", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "companyName", "formControlName", "companyName", "id", "companyName", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "createDate", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "createDate", "formControlName", "createDate", "id", "createDate", "type", "date", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "hostCode", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "hostCode", "formControlName", "hostCode", "id", "hostCode", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "createWho", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "createWho", "formControlName", "createWho", "id", "createWho", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success"], [1, "p-field"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"]], template: function CompanydetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, CompanydetailComponent_form_3_Template, 46, 7, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("model", ctx.breadcrumbs)("home", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.groupForm);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_11__["Breadcrumb"], primeng_messages__WEBPACK_IMPORTED_MODULE_12__["Messages"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_13__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_14__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], primeng_button__WEBPACK_IMPORTED_MODULE_15__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb21wYW55ZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "7LVI":
/*!***********************************************************************!*\
  !*** ./src/app/pages/bpmp/rules/rulesdetail/rulesdetail.component.ts ***!
  \***********************************************************************/
/*! exports provided: RulesdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RulesdetailComponent", function() { return RulesdetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class RulesdetailComponent {
    constructor() { }
    ngOnInit() { }
}
RulesdetailComponent.ɵfac = function RulesdetailComponent_Factory(t) { return new (t || RulesdetailComponent)(); };
RulesdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: RulesdetailComponent, selectors: [["app-rulesdetail"]], decls: 2, vars: 0, template: function RulesdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "rulesdetail works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJydWxlc2RldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "7YUa":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/root/applicationgroup/applicationdetail/applicationdetail.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: ApplicationdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationdetailComponent", function() { return ApplicationdetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_root_applications_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/root/applications.service */ "4ei0");
/* harmony import */ var src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/root/group-service.service */ "XfbB");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/checkbox */ "Ji6n");
/* harmony import */ var primeng_picklist__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/picklist */ "iHf9");
/* harmony import */ var primeng_orderlist__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/orderlist */ "cQJI");

















function ApplicationdetailComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Group Name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ApplicationdetailComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Choose All");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-checkbox", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function ApplicationdetailComponent_div_16_Template_p_checkbox_onChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r6.checkAll($event, ctx_r6.listmenubymodules, "create"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-checkbox", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function ApplicationdetailComponent_div_16_Template_p_checkbox_onChange_8_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r8.checkAll($event, ctx_r8.listmenubymodules, "read"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p-checkbox", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function ApplicationdetailComponent_div_16_Template_p_checkbox_onChange_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r9.checkAll($event, ctx_r9.listmenubymodules, "update"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "p-checkbox", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function ApplicationdetailComponent_div_16_Template_p_checkbox_onChange_12_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r10.checkAll($event, ctx_r10.listmenubymodules, "delete"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "p-checkbox", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function ApplicationdetailComponent_div_16_Template_p_checkbox_onChange_14_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r11.checkAll($event, ctx_r11.listmenubymodules, "view"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "p-checkbox", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onChange", function ApplicationdetailComponent_div_16_Template_p_checkbox_onChange_16_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r12.checkAll($event, ctx_r12.listmenubymodules, "approval"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("binary", true);
} }
function ApplicationdetailComponent_p_pickList_17_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "i", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const product_r14 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](" " + product_r14.modulename);
} }
const _c0 = function () { return { "height": "5rem" }; };
function ApplicationdetailComponent_p_pickList_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-pickList", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ApplicationdetailComponent_p_pickList_17_ng_template_1_Template, 5, 1, "ng-template", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("source", ctx_r2.listmodulessource)("target", ctx_r2.listmodulesdest)("dragdrop", true)("responsive", true)("sourceStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](6, _c0))("targetStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](7, _c0));
} }
function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "h5", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "i", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p-checkbox", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_ngModelChange_10_listener($event) { const menu_r16 = ctx.$implicit; return menu_r16.acl.create = $event; })("onChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_onChange_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const menu_r16 = ctx.$implicit; const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r18.check($event, menu_r16, "create", menu_r16); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "p-checkbox", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_ngModelChange_12_listener($event) { const menu_r16 = ctx.$implicit; return menu_r16.acl.read = $event; })("onChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_onChange_12_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const menu_r16 = ctx.$implicit; const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r21.check($event, menu_r16, "read", menu_r16); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "p-checkbox", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_ngModelChange_14_listener($event) { const menu_r16 = ctx.$implicit; return menu_r16.acl.update = $event; })("onChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_onChange_14_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const menu_r16 = ctx.$implicit; const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r23.check($event, menu_r16, "update", menu_r16); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "p-checkbox", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_ngModelChange_16_listener($event) { const menu_r16 = ctx.$implicit; return menu_r16.acl.delete = $event; })("onChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_onChange_16_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const menu_r16 = ctx.$implicit; const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r25.check($event, menu_r16, "delete", menu_r16); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "p-checkbox", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_ngModelChange_18_listener($event) { const menu_r16 = ctx.$implicit; return menu_r16.acl.view = $event; })("onChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_onChange_18_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const menu_r16 = ctx.$implicit; const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r27.check($event, menu_r16, "view", menu_r16); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, ",\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "p-checkbox", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_ngModelChange_20_listener($event) { const menu_r16 = ctx.$implicit; return menu_r16.acl.approval = $event; })("onChange", function ApplicationdetailComponent_p_orderList_19_ng_template_1_Template_p_checkbox_onChange_20_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const menu_r16 = ctx.$implicit; const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r29.check($event, menu_r16, "approval", menu_r16); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const menu_r16 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](menu_r16.moduleName);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](" " + menu_r16.menuName);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", menu_r16.acl.create)("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", menu_r16.acl.read)("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", menu_r16.acl.update)("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", menu_r16.acl.delete)("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", menu_r16.acl.view)("binary", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", menu_r16.acl.approval)("binary", true);
} }
function ApplicationdetailComponent_p_orderList_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-orderList", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ApplicationdetailComponent_p_orderList_19_ng_template_1_Template, 21, 14, "ng-template", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", ctx_r3.listmenubymodules);
} }
function ApplicationdetailComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Modules are required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ApplicationdetailComponent_button_24_Template(rf, ctx) { if (rf & 1) {
    const _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ApplicationdetailComponent_button_24_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r31); const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r30.onCancel(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
class ApplicationdetailComponent {
    constructor(authservice, dialogService, messageService, router, location, formBuilder, applicationsService, activatedRoute, groupService) {
        var _a;
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.router = router;
        this.location = location;
        this.formBuilder = formBuilder;
        this.applicationsService = applicationsService;
        this.activatedRoute = activatedRoute;
        this.groupService = groupService;
        this.extraInfo = {};
        this.inputform = {};
        this.activedetail = 1;
        this.titleActive = "Please select module needed on this group ACL!";
        this.activebutton = "Next";
        this.isEdit = false;
        this.userInfo = {};
        this.selectedModules = [];
        this.tokenID = "";
        // orgForm!: FormGroup;
        this.groupName = "";
        this.groupDesc = "";
        this.submitted = false;
        this.groupId = "";
        this.apps = [];
        this.aclData = {
            read: 0,
            create: 0,
            update: 0,
            delete: 0,
            view: 0,
            approval: 0,
        };
        this.listmodulessource = [];
        this.listmodulessourcetmp = [];
        this.listmodulesdest = [];
        this.listmenubymodules = [];
        this.appNotSelected = false;
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
        // console.log(">>>EXTRA>>>>> "+this.extraInfo);
        // console.log(checkurl);
        if (checkurl)
            this.isEdit = true;
        // console.dir(this.router);
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [
            {
                label: "Applications Group",
                command: (event) => {
                    this.location.back();
                },
                url: "",
            },
            { label: this.isEdit ? "Edit data" : "Add data" },
        ];
        // this.orgForm = this.formBuilder.group({
        //   organizationname: ['', Validators.required],
        //   description: [''],
        // });
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            // console.log(">>>> Userinfo : "+JSON.stringify(this.userInfo));
            if (!this.isEdit) {
                this.groupService.menus = [];
                this.apps = this.userInfo.apps;
                this.groupService
                    .getAllModules(this.apps[0].idapp)
                    .subscribe((modulesresult) => {
                    // console.log(">> MODULES >>"+JSON.stringify(modulesresult));
                    this.listmodulessource = modulesresult.data;
                    this.listmodulessourcetmp = [];
                });
            }
            else {
                console.log(">>>>>>>>>> ON ADD DATA  ");
                this.apps = this.userInfo.apps;
                // console.log(">>>>>>>>>> Route active "+this.activatedRoute.snapshot.paramMap.get('id'));
                if (this.activatedRoute.snapshot.paramMap.get("id")) {
                    let grpid = this.activatedRoute.snapshot.paramMap.get("id");
                    this.groupId = grpid;
                    // console.log(">>>>>>>>>> Route active "+grpid);
                    this.groupId = grpid || "";
                    this.groupService
                        .getGroupDetail(this.groupId)
                        .subscribe((result) => {
                        console.log(">>>> Data JSON EDIT : " + JSON.stringify(result));
                        const groupObj = result.data.result;
                        this.groupName = groupObj.group.groupname;
                        let destTempList = groupObj.modules;
                        // this.listmodulesdest = groupObj.modules;
                        this.groupService
                            .getAllModules(this.apps[0].idapp)
                            .subscribe((modulesresult) => {
                            // console.log(">> MODULES >>"+JSON.stringify(modulesresult));
                            this.listmodulessource = modulesresult.data;
                            // console.log(
                            //   ">> MODULES listmodulessource >>" +
                            //     JSON.stringify(this.listmodulessource)
                            // );
                            // console.log(
                            //   ">> MODULES Dapat >>" + JSON.stringify(destTempList)
                            // );
                            destTempList.forEach((elementdst, idx) => {
                                var ItemIndex = this.listmodulessource.findIndex((b) => b.id[0] === elementdst.id[0]);
                                console.log(">>>> " + ItemIndex);
                                if (ItemIndex > -1) {
                                    this.listmodulesdest.push(this.listmodulessource[ItemIndex]);
                                    this.listmodulessource.splice(ItemIndex, 1);
                                }
                            });
                            //  });
                            // this.listmodulessourcetmp.map((element, index) => {
                            //   destTempList.map((elementdst, idx) =>{
                            //       // console.log("id dest "+JSON.stringify(elementdst));
                            //       console.log(" TEST : "+" >>> "+element.id +"," + elementdst.id)
                            //       if(element.id === elementdst.id) {
                            //         this.listmodulesdest.push(element);
                            //       } else {
                            //         this.listmodulessource.push(element);
                            //       }
                            //   });
                            // })
                        });
                    });
                }
            }
        });
    }
    onRowSelect(event) {
        this.applicationsService.applications = [];
        this.appNotSelected = false;
        let index = this.applicationsService.allApplications.findIndex((application) => {
            return application.id === event.data.id;
        });
        this.applicationsService.allApplications[index].selected = true;
        this.applicationsService.applications.push(event.data);
    }
    onSubmit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.submitted = true;
            // console.log('VALID ' + this.orgForm.valid + " "+ this.activedetail);
            // if (this.orgForm.valid) {
            if (this.groupName === "" || this.groupName === null) {
                this.inputform.errors = "InputError";
                return true;
            }
            switch (this.activedetail) {
                case 1:
                    this.titleActive = "Please check Access Level for the menus!";
                    this.activedetail = 2;
                    this.activebutton = "Save";
                    this.selectedModules = this.listmodulesdest;
                    this.listmenubymodules = yield this.getAllmenuByModules(this.listmodulesdest, this.groupService, this.isEdit, this.groupId, this.userInfo);
                    // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> ABIS I KIRM "+JSON.stringify(this.listmenubymodules));
                    break;
                case 2:
                    this.titleActive = "Please select module needed on this group ACL!";
                    let reqData = {};
                    yield this.setupModuleReq();
                    // reqData.
                    // let group = {"groupName": this.orgForm.get('organizationname').value, "groupType":1};
                    let group = { groupName: this.groupName, groupType: 1 };
                    this.groupService.groupSetupData.group = group;
                    reqData.group = this.groupService.groupSetupData.group;
                    reqData.modules = this.selectedModules;
                    reqData.menus = this.groupService.menus;
                    this.groupService.reqGroup = reqData;
                    this.groupService.haveModuleAndMenus = true;
                    // console.log("Data to input "+JSON.stringify(reqData))
                    if (!this.isEdit) {
                        // console.log("Add to input "+JSON.stringify(this.groupService.reqGroup))
                        this.groupService
                            .regisGroup(this.groupService.reqGroup)
                            .subscribe((result) => {
                            if (result.status === 200) {
                                this.groupService.modules = [];
                                this.groupService.reqGroup = [];
                                this.location.back();
                            }
                        });
                    }
                    else {
                        this.groupService.reqGroup.group.groupId = this.groupId;
                        // console.log("Edit to input "+JSON.stringify(this.groupService.reqGroup))
                        // this.groupService.reqGroup.group.groupId = this.groupId;
                        this.groupService
                            .editGroup(this.groupService.reqGroup)
                            .subscribe((result) => {
                            if (result.status === 200) {
                                this.groupService.modules = [];
                                this.groupService.reqGroup = [];
                                this.location.back();
                            }
                        });
                    }
                    break;
                default:
                    break;
            }
        });
    }
    // get f() {
    //   return this.orgForm.controls;
    // }
    onCancel() {
        this.activedetail = this.activedetail - 1;
        this.activebutton = "Next";
    }
    getAllmenuByModules(payload, gs, isEdit, groupid, userInfo) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let jml = 1;
            const promise = new Promise(function (resolve, reject) {
                let menusTmp = [];
                let menus = [];
                payload.map((module) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    if (isEdit) {
                        // let modulesTmp: any[] = [];
                        let modules = [];
                        gs.getAllMenuByModuleIdAndGroupId(module.id, groupid).subscribe((value) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            console.log("Menu Ambil dari Edit " + JSON.stringify(value.data));
                            menusTmp = [];
                            // console.log("Menu Edit "+JSON.stringify(userInfo));
                            // let menusobj = value.data.menus;
                            //   menusobj.map((jsobj) => {
                            //   menus.push(jsobj);
                            // })
                            // gs.menus = [];
                            // let objmodule = {"id":value.data.id,"modulename":value.data.modulename,"created_byid":userInfo.id,"status":1,"idapplication":"4","modulecode":"BIFAST003","applabel":"KOMI","view":1}
                            // modules.push(objmodule);
                            // menus.push(value.data.menus);
                            // modules.map(async (module: any) => {
                            //   menus = module.menus;
                            // });
                            let menusobj = value.data.menus;
                            yield menusobj.map((jsobj) => {
                                menusTmp.push(jsobj);
                            });
                            yield menusTmp.map((menut) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                                let data = {};
                                let acl = {};
                                acl = {
                                    read: menut.fread,
                                    create: menut.fcreate,
                                    update: menut.fupdate,
                                    delete: menut.fdelete,
                                    approval: menut.fapproval,
                                };
                                data.menuId = menut.id;
                                data.menuName = menut.title;
                                data.moduleId = value.data.id;
                                // data.moduleName = menu.modulename;value.data
                                data.moduleName = value.data.modulename;
                                data.acl = acl;
                                if (!gs.haveModuleAndMenus) {
                                    menusTmp = [];
                                    menus.push(data);
                                    gs.menus.push(data);
                                }
                            }));
                            if (jml == payload.length) {
                                // gs.menus = menus;
                                console.log(">> MENU : " + JSON.stringify(menus));
                                resolve(menus);
                            }
                            jml++;
                        }));
                    }
                    else {
                        gs.getAllMenuByModuleId(module.id).subscribe((value) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            // console.log("Menu Ambil dari module "+JSON.stringify(value.data));
                            // console.log("PAYLOAD "+payload.length+", jml "+jml);
                            // let menusobj = value.data.menus;
                            //   menusobj.map((jsobj) => {
                            //   menus.push(jsobj);
                            // })
                            let menusobj = value.data.menus;
                            yield menusobj.map((jsobj) => {
                                menusTmp.push(jsobj);
                            });
                            yield menusTmp.map((menut) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                                let data = {};
                                let acl = {};
                                acl = {
                                    read: menut.fread,
                                    create: menut.fcreate,
                                    update: menut.fupdate,
                                    delete: menut.fdelete,
                                };
                                data.menuId = menut.id;
                                data.menuName = menut.title;
                                // data.moduleId = menut.idmodule;value.data
                                data.moduleId = value.data.id;
                                // data.moduleName = menu.modulename;value.data
                                data.moduleName = value.data.modulename;
                                data.acl = acl;
                                if (!gs.haveModuleAndMenus) {
                                    menusTmp = [];
                                    // console.log("Isi menu  "+JSON.stringify(data));
                                    menus.push(data);
                                    gs.menus.push(data);
                                }
                            }));
                            if (jml == payload.length) {
                                menusTmp = [];
                                // gs.menus = menus;
                                console.log(">> MENU : " + JSON.stringify(menus));
                                resolve(menus);
                            }
                            jml++;
                        }));
                    }
                    // console.log(JSON.stringify(module))
                }));
                // resolve('Promise returns after 1.5 second!');
            });
            return promise.then(function (value) {
                // console.log("Menu yang di masukin :"+JSON.stringify(value));
                return value;
                // Promise returns after 1.5 second!
            });
        });
    }
    check(event, datas, name, module) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // console.log("MASUK CECH")
            yield this.setupAclData(name, event);
            // console.log("Modules on check "+JSON.stringify(this.aclData));
            // console.log("Modules on check "+JSON.stringify(datas));
            if (this.groupService.menus.length < 1) {
                // console.log("MENU NGGA ADA");
                let menuReq = {
                    // moduleId: module.id,
                    moduleId: module.idmodule,
                    moduleName: datas.modulename,
                    menuId: datas.id,
                    menuName: datas.title,
                    acl: {},
                };
                menuReq.acl = this.aclData;
                // console.log(JSON.stringify(menuReq));
                this.groupService.menus.push(menuReq);
            }
            else {
                // console.log("MENU BANYAK");
                let isExist = yield this.groupService.menus.findIndex((menu) => {
                    // console.log(">>> MENU SERVICE "+JSON.stringify(menu))
                    // console.log(">>> MENU DATA "+JSON.stringify(datas))
                    return parseInt(menu.menuId) === parseInt(datas.menuId);
                    // return parseInt(menu.id) === parseInt(datas.id);
                });
                // console.log('isExist ' + isExist);
                if (isExist < 0) {
                    let menuReq = {
                        // moduleId: module.id,
                        moduleId: module.moduleId,
                        moduleName: datas.modulename,
                        menuId: datas.id,
                        menuName: datas.title,
                        acl: {},
                    };
                    menuReq.acl = this.aclData;
                    this.groupService.menus.push(menuReq);
                    //console.log(this.groupService.menus);
                }
                else {
                    // console.log("isExist > anem : ", name+" "+ isExist);
                    // console.log("OBJECT "+JSON.stringify(this.groupService.menus));
                    // console.log('Module ' + JSON.stringify(module));
                    this.groupService.menus[isExist].idmodule = module.moduleId;
                    this.groupService.menus[isExist].moduleId = module.moduleId;
                    // this.groupService.menus[isExist].moduleId = module.id;
                    if (name == "approval") {
                        this.groupService.menus[isExist].acl[name] = event.checked ? 1 : 0;
                        this.groupService.menus[isExist].acl["create"] = 0;
                        yield this.setupAclData("create", { event: { checked: 0 } });
                        // this.groupService.menus[isExist].acl['read'] = 0;
                        this.groupService.menus[isExist].acl["update"] = 0;
                        yield this.setupAclData("update", { event: { checked: 0 } });
                        this.groupService.menus[isExist].acl["delete"] = 0;
                        yield this.setupAclData("delete", { event: { checked: 0 } });
                        this.groupService.menus[isExist].acl["view"] = 0;
                        yield this.setupAclData("view", { event: { checked: 0 } });
                    }
                    else {
                        this.groupService.menus[isExist].acl[name] = event.checked ? 1 : 0;
                        this.groupService.menus[isExist].acl["approval"] = 0;
                    }
                    // this.groupService.menus[isExist].acl['approval'] = event.checked ? 1 : 0;
                    // this.groupService.menus[isExist]["f"+name] = event.checked ? 1 : 0;
                    // console.log(JSON.stringify(this.groupService.menus));
                }
            }
        });
    }
    checkAll(event, datas, name) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // console.log(JSON.stringify(event));
            console.log(JSON.stringify(datas));
            let idx = 0;
            for (let i in datas) {
                // resultingArr.push(i + 1)
                if (name == "approval") {
                    yield this.setupAclData(name, event);
                    this.groupService.menus[idx].acl[name] = event.checked ? 1 : 0;
                    this.groupService.menus[idx].acl["create"] = 0;
                    yield this.setupAclData("create", { event: { checked: 0 } });
                    this.groupService.menus[idx].acl["read"] = 1;
                    yield this.setupAclData("read", { event: { checked: 1 } });
                    this.groupService.menus[idx].acl["update"] = 0;
                    yield this.setupAclData("update", { event: { checked: 0 } });
                    this.groupService.menus[idx].acl["delete"] = 0;
                    yield this.setupAclData("delete", { event: { checked: 0 } });
                    this.groupService.menus[idx].acl["view"] = 1;
                    yield this.setupAclData("view", { event: { checked: 1 } });
                }
                else {
                    yield this.setupAclData(name, event);
                    // console.log(JSON.stringify(event));
                    // console.log(JSON.stringify(name));
                    this.groupService.menus[idx].acl[name] = event.checked ? 1 : 0;
                    this.groupService.menus[idx].acl["approval"] = 0;
                }
                // this.groupService.menus[idx].acl[name] = event.checked ? 1 : 0;
                idx++;
            }
        });
    }
    setupAclData(name, event) {
        this.aclData = {
            read: 0,
            create: 0,
            update: 0,
            delete: 0,
            view: 0,
        };
        // console.log(JSON.stringify(event));
        switch (name) {
            case "read": {
                this.aclData.read = event.checked ? 1 : 0;
                break;
            }
            case "create": {
                this.aclData.create = event.checked ? 1 : 0;
                break;
            }
            case "update": {
                this.aclData.update = event.checked ? 1 : 0;
                break;
            }
            case "delete": {
                this.aclData.delete = event.checked ? 1 : 0;
                break;
            }
            case "view": {
                this.aclData.view = event.checked ? 1 : 0;
                break;
            }
            case "approval": {
                this.aclData.view = event.checked ? 1 : 0;
                break;
            }
            default: {
                //statements;
                break;
            }
        }
    }
    setupModuleReq() {
        this.groupService.modules.map((data) => {
            let moduleModel = {
                moduleId: "",
                acl: {
                    view: 1,
                },
            };
            moduleModel.moduleId = data.id;
            this.selectedModules.push(moduleModel);
        });
    }
}
ApplicationdetailComponent.ɵfac = function ApplicationdetailComponent_Factory(t) { return new (t || ApplicationdetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_4__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_root_applications_service__WEBPACK_IMPORTED_MODULE_8__["ApplicationsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_9__["GroupServiceService"])); };
ApplicationdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ApplicationdetailComponent, selectors: [["app-applicationdetail"]], decls: 27, vars: 11, consts: [[3, "home", "model"], [1, "wrapperinside"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-3"], [1, "box"], ["for", "organizationname"], ["name", "organizationname", "id", "organizationname", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2", 3, "ngModel", "ngModelChange"], ["class", "p-field", 4, "ngIf"], [1, "p-col-9"], ["for", "mnlist"], [2, "height", "10px"], ["class", "p-grid", 4, "ngIf"], ["sourceHeader", "Modules availables", "targetHeader", "Selected", 3, "source", "target", "dragdrop", "responsive", "sourceStyle", "targetStyle", 4, "ngIf"], ["id", "mnlist", 3, "value", 4, "ngIf"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "button", "label", "Back", "class", "p-button-warning p-mr-2", "icon", "pi pi-angle-left", "iconPos", "left", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success", 3, "label", "click"], [1, "p-field"], [2, "color", "red"], [1, "p-col-fixed", 2, "width", "345px", "font-weight", "bold"], [1, "p-text-right"], [1, "p-col"], ["name", "create", "label", "Create", 3, "binary", "onChange"], ["name", "read", "label", "Read", 3, "binary", "onChange"], ["name", "update", "label", "Update", 3, "binary", "onChange"], ["name", "delete", "label", "Delete", 3, "binary", "onChange"], ["name", "view", "label", "Preview", 3, "binary", "onChange"], ["name", "approval", "label", "Approval", 3, "binary", "onChange"], ["sourceHeader", "Modules availables", "targetHeader", "Selected", 3, "source", "target", "dragdrop", "responsive", "sourceStyle", "targetStyle"], ["pTemplate", "item"], [1, "product-item"], [1, "product-list-detail"], [1, "pi", "pi-tag", "product-category-icon"], [1, "product-category"], ["id", "mnlist", 3, "value"], [1, "p-col-fixed", 2, "width", "250px"], [1, "p-mb-2", "p-mt-2"], [1, "menu-category"], ["name", "create", "label", "Create", 3, "ngModel", "binary", "ngModelChange", "onChange"], ["name", "read", "label", "Read", 3, "ngModel", "binary", "ngModelChange", "onChange"], ["name", "update", "label", "Update", 3, "ngModel", "binary", "ngModelChange", "onChange"], ["name", "delete", "label", "Delete", 3, "ngModel", "binary", "ngModelChange", "onChange"], ["name", "view", "label", "Preview", 3, "ngModel", "binary", "ngModelChange", "onChange"], ["name", "approval", "label", "Approval", 3, "ngModel", "binary", "ngModelChange", "onChange"], [1, "p-ml-6", "p-pl-4", 2, "color", "red", "padding-top", "10px"], ["pButton", "", "pRipple", "", "type", "button", "label", "Back", "icon", "pi pi-angle-left", "iconPos", "left", 1, "p-button-warning", "p-mr-2", 3, "click"]], template: function ApplicationdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Group Name * :");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ApplicationdetailComponent_Template_input_ngModelChange_10_listener($event) { return ctx.groupName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, ApplicationdetailComponent_div_11_Template, 3, 0, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, ApplicationdetailComponent_div_16_Template, 17, 6, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, ApplicationdetailComponent_p_pickList_17_Template, 2, 8, "p-pickList", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, ApplicationdetailComponent_p_orderList_19_Template, 2, 1, "p-orderList", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, ApplicationdetailComponent_div_20_Template, 3, 0, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, ApplicationdetailComponent_button_24_Template, 1, 0, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ApplicationdetailComponent_Template_button_click_25_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](26, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.groupName);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.inputform.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.titleActive);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.activedetail == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.activedetail == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.activedetail == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.listmodulesdest.length < 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.activedetail > 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("label", ctx.activebutton);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["Breadcrumb"], primeng_card__WEBPACK_IMPORTED_MODULE_11__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["DefaultValueAccessor"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["ButtonDirective"], primeng_checkbox__WEBPACK_IMPORTED_MODULE_14__["Checkbox"], primeng_picklist__WEBPACK_IMPORTED_MODULE_15__["PickList"], primeng_api__WEBPACK_IMPORTED_MODULE_4__["PrimeTemplate"], primeng_orderlist__WEBPACK_IMPORTED_MODULE_16__["OrderList"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBsaWNhdGlvbmRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "AUuv":
/*!****************************************************************!*\
  !*** ./src/app/pages/bpmp/bpmphome/bpmpintercept.component.ts ***!
  \****************************************************************/
/*! exports provided: BpmpinterceptComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BpmpinterceptComponent", function() { return BpmpinterceptComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");




class BpmpinterceptComponent {
    constructor(route, router, authservice, sessionStorage) {
        this.route = route;
        this.router = router;
        this.authservice = authservice;
        this.sessionStorage = sessionStorage;
        this.userInfo = {};
        //  app:any = {};
        //  applicationAvailable:number = 0;
        this.tokenID = "";
    }
    ngOnInit() {
        this.param = this.route.snapshot.params["config"];
        // console.log('>ITCEPT!@@!@!@!@!@>'+this.param);
        this.tokenID = this.param;
        // this.authservice.whoAmi().subscribe((value) =>{
        //   // console.log(">>> User Info : "+JSON.stringify(value));
        //     this.userInfo = value.data;
        //     console.log(">>> User Info : "+JSON.stringify(this.userInfo));
        //     this.tokenID = value.tokenId;
        // });
        this.authservice.whoAmiInitialize(this.tokenID).subscribe((value) => {
            // this.sessionStorage.set("accesstoken", authToken);
            // this.authservice.setAuthStatus(true);
            // this.authservice.setToken(authToken);
            console.log(">>> Token session : " + JSON.stringify(value));
            if ((value.status = 200)) {
                this.sessionStorage.set("accesstoken", value.data);
                this.authservice.setAuthStatus(true);
                this.authservice.setToken(value.data);
                this.router.navigate(["mgm/home"]);
            }
            else {
                this.router.navigate(["auth/login"]);
            }
        });
    }
}
BpmpinterceptComponent.ɵfac = function BpmpinterceptComponent_Factory(t) { return new (t || BpmpinterceptComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"])); };
BpmpinterceptComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BpmpinterceptComponent, selectors: [["app-bpmphome"]], decls: 2, vars: 0, template: function BpmpinterceptComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Hallo");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, encapsulation: 2 });


/***/ }),

/***/ "AYsR":
/*!*****************************************************************!*\
  !*** ./src/app/pages/root/systemparam/systemparam.component.ts ***!
  \*****************************************************************/
/*! exports provided: SystemparamComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemparamComponent", function() { return SystemparamComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_servbpmp_sysparam_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/servbpmp/sysparam.service */ "dI1x");
/* harmony import */ var src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils/aclmenuchecker.service */ "42A2");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");














function SystemparamComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 10);
} }
function SystemparamComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function SystemparamComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.deleteConfirmation($event); })("dataapprover", function SystemparamComponent_div_3_Template_app_tablehelper_dataapprover_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.approvalData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.userlist)("header", ctx_r1.usrheader)("wsearch", true)("actionbtn", ctx_r1.usractionbtn)("colnames", ctx_r1.usrcolname)("colwidth", ctx_r1.usrcolwidth)("colclasshalign", ctx_r1.usrcolhalign)("addbtnlink", ctx_r1.usraddbtn)("nopaging", false)("scrollheight", ctx_r1.scrollheight)("colmark", 4);
} }
function SystemparamComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SystemparamComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SystemparamComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.deleteUser(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function SystemparamComponent_ng_template_13_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SystemparamComponent_ng_template_13_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.viewApprove = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SystemparamComponent_ng_template_13_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r12.approvalSubmit(4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p-button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SystemparamComponent_ng_template_13_Template_p_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r13.approvalSubmit(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
const _c1 = function () { return { width: "50vw" }; };
class SystemparamComponent {
    constructor(authservice, dialogService, messageService, sysService, aclMenuService, router) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.sysService = sysService;
        this.aclMenuService = aclMenuService;
        this.router = router;
        this.display = false;
        this.viewApprove = false;
        this.selectedUser = {};
        this.isGroup = false;
        this.userlist = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.usrheader = [
            { label: "No", sort: "id" },
            { label: "Parameter", sort: "param_name" },
            { label: "Value", sort: "param_value" },
            { label: "Category", sort: "category" },
            { label: "Status", sort: "active" },
            { label: "Description", sort: "description" },
        ];
        this.usrcolname = [
            "id",
            "param_name",
            "param_value",
            "category",
            "active",
            "description",
        ];
        this.usrcolhalign = [
            "p-text-center",
            "",
            "p-text-center",
            "",
            "p-text-center",
            "",
        ];
        this.usrcolwidth = [
            { width: "80px" },
            "",
            "",
            { width: "180px" },
            { width: "105px" },
            "",
        ];
        // orgcollinghref:any = {'url':'#','label':'Application'}
        this.usractionbtn = [0, 1, 1, 0, 0, 1];
        this.usraddbtn = { route: "detail", label: "Add Data" };
    }
    // private sysparamService:sysparamService
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [{ label: "Security parameters" }];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
                console.log("MENU ALL ACL Set");
                this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
                    if (JSON.stringify(dataacl.acl) === "{}") {
                        console.log("No ACL Founded");
                    }
                    else {
                        console.log("ACL Founded");
                        console.log(dataacl.acl);
                        this.usractionbtn[0] = dataacl.acl.create;
                        this.usractionbtn[1] = dataacl.acl.read;
                        this.usractionbtn[2] = dataacl.acl.update;
                        // this.usractionbtn[3] = dataacl.acl.delete;
                        this.usractionbtn[4] = dataacl.acl.view;
                        this.usractionbtn[5] = dataacl.acl.approval;
                    }
                });
            });
        });
        this.refreshingUser();
    }
    refreshingUser() {
        this.isFetching = true;
        this.authservice.whoAmi().subscribe((data) => {
            // console.log(">>>>>>> "+JSON.stringify(data));
            if ((data.status = 200)) {
                this.sysService
                    .getAllSysParam()
                    .subscribe((orgall) => {
                    // console.log('>>>>>>> ' + JSON.stringify(orgall));
                    this.userlist = orgall.data;
                    if (this.userlist.length < 1) {
                        let objtmp = {
                            id: "No records",
                            param_name: "No records",
                            param_value: "No records",
                            category: "No records",
                            description: "No records",
                        };
                        this.userlist = [];
                        this.userlist.push(objtmp);
                    }
                    this.isFetching = false;
                });
            }
        });
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedUser = data;
    }
    approvalData(data) {
        console.log(data);
        this.viewApprove = true;
        this.selectedUser = data;
    }
    approvalSubmit(status) {
        console.log(this.selectedUser);
        console.log(status);
        let payload = {
            id: this.selectedUser.id,
            oldactive: this.selectedUser.active,
            isactive: status,
            idapproval: this.selectedUser.idapproval,
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.sysService
            .updateSysParamActive(payload)
            .subscribe((result) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status === 200) {
                this.refreshingUser();
                this.viewApprove = false;
            }
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
SystemparamComponent.ɵfac = function SystemparamComponent_Factory(t) { return new (t || SystemparamComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_servbpmp_sysparam_service__WEBPACK_IMPORTED_MODULE_4__["SysparamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__["AclmenucheckerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"])); };
SystemparamComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SystemparamComponent, selectors: [["app-systemparam"]], decls: 14, vars: 18, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete User", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "Approval", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", 2, "font-size", "18px"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "colclasshalign", "addbtnlink", "nopaging", "scrollheight", "colmark", "datadeleted", "dataapprover"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"], ["label", "Cancel", "styleClass", "p-button-text", 3, "click"], ["label", "Reject", "styleClass", "p-button-text", 3, "click"], ["label", "Approve", "styleClass", "p-button-text", 3, "click"]], template: function SystemparamComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SystemparamComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, SystemparamComponent_div_3_Template, 2, 11, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function SystemparamComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Users?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, SystemparamComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p-dialog", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function SystemparamComponent_Template_p_dialog_visibleChange_8_listener($event) { return ctx.viewApprove = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "This records need approval from you?, please choose wisely");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, SystemparamComponent_ng_template_13_Template, 3, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userlist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userlist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](16, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](17, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewApprove)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_9__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_10__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzeXN0ZW1wYXJhbS5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "D8EZ":
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");















const _c0 = function () { return { width: "200px!important" }; };
function LoginComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](2, _c0));
} }
function LoginComponent_div_15_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "User is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function LoginComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, LoginComponent_div_15_span_1_Template, 2, 0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.userid.errors.required);
} }
function LoginComponent_div_21_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function LoginComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, LoginComponent_div_21_span_1_Template, 2, 0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.secret.errors.required);
} }
function LoginComponent_ng_template_26_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c1 = function () { return { width: "360px" }; };
class LoginComponent {
    constructor(messageService, sessionStorage, route, formBuilder, backend, authservice) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.blockedDocument = false;
        //  userid = "";
        this.captchaSiteKey = "6LcvgrQcAAAAAB_tLF7BUtS1p2xY2sI8tvNxJW94";
        this.secret = "";
        this.errorMsg = "";
        this.isProcess = false;
        this.submitted = false;
    }
    ngOnInit() {
        this.userForm = this.formBuilder.group({
            userid: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            secret: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        });
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.userForm.controls;
    }
    get userFormControl() {
        return this.userForm.controls;
    }
    resolved(captchaResponse) {
        console.log(`Resolved captcha with response: ${captchaResponse}`);
    }
    onError(errorDetails) {
        console.log(`reCAPTCHA error encountered; details:`, errorDetails);
    }
    onSubmit() {
        this.submitted = true;
        if (this.userForm.valid) {
            this.isProcess = true;
            this.backend
                .post("adm/auth/signviaadmin", {
                credential: this.userForm.value["userid"],
                secret: this.userForm.value["secret"],
                appname: "bpmp",
            }, false)
                .subscribe((data) => {
                // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
                if (data.status === 200) {
                    const authToken = data.data;
                    this.sessionStorage.set("accesstoken", authToken);
                    this.authservice.setAuthStatus(true);
                    this.authservice.setToken(authToken);
                    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
                    //   console.log('>>>>>>> ' + JSON.stringify(data));
                    //   if ((data.status = 200)) {
                    //     let objecInfo = data.data;
                    //     let lvlTn = parseInt(objecInfo.leveltenant);
                    //     console.log('>> LVL TENANT >> ' + lvlTn);
                    //     //   if(objecInfo.leveltenant < 1) {
                    // this.route.navigate(['/' + tokenID]);
                    //     //   } else {
                    // this.route.navigate(['/mgm/admin']);
                    //     //   }
                    //   }
                    // });
                    this.route.navigate(["mgm/home"]);
                    this.isProcess = false;
                }
                else {
                    this.sessionStorage.clear();
                    this.showTopCenterErr(`${data.data ? data.data : "Invalid username or password"}`);
                    this.authservice.setAuthStatus(false);
                    this.isProcess = false;
                }
            }, (error) => {
                this.showTopCenterErr("Invalid user and password");
                this.isProcess = false;
                this.authservice.setAuthStatus(false);
            });
        }
        //  this.blockDocument();
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: "error",
            summary: "Error",
            detail: message,
        });
    }
    blockDocument() {
        this.blockedDocument = true;
        //  setTimeout(() => {
        //      this.blockedDocument = false;
        //      this.showTopCenterErr("Invalid user and password!")
        //  }, 3000);
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"])); };
LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], decls: 31, vars: 10, consts: [[3, "target", "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], [1, "form", 3, "formGroup", "ngSubmit"], ["header", "", "styleClass", "p-card-shadow p-header-w50"], ["pTemplate", "header"], [1, "p-fluid"], [2, "margin", "0", "padding", "0"], [1, "p-field"], ["for", "userid", 1, "labelpb"], ["id", "userid", "name", "userid", "type", "text", "required", "", "formControlName", "userid", "pInputText", ""], ["class", "p-field", 4, "ngIf"], [2, "height", "0.3rem"], ["for", "secret", 1, "labelpb"], ["id", "secret", "type", "password", "formControlName", "secret", "pInputText", ""], ["href", "#/auth/forgotpassword"], ["pTemplate", "footer"], [2, "height", "0.5rem"], [1, "p-text-center"], ["alt", "commonwealth", "src", "assets/logos/commonwealthbank.png"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], [1, "p-text-right"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Login", 1, "p-primary-btn"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-blockUI", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function LoginComponent_Template_form_ngSubmit_5_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, LoginComponent_ng_template_7_Template, 2, 3, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "h4", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "PTBC Portal");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "User Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, LoginComponent_div_15_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, LoginComponent_div_21_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](22, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "Forgot Password?");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](26, LoginComponent_ng_template_26_Template, 2, 0, "ng-template", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, " Layanan yang ada hanya diperuntukkan oleh yang berwenang. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](30, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.userForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](8, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.userid.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.secret.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](9, _c1));
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_7__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_8__["ProgressSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_9__["Card"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__["InputText"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_12__["Messages"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyJ9 */", "body[_ngcontent-%COMP%] {\n        background: #007dc5\n    }\n\n    form[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%)\n    }\n\n    .p-messages[_ngcontent-%COMP%] {\n        max-width: 360px;\n    }"] });


/***/ }),

/***/ "Es3d":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/bpmp/transactionmonitoring/transactionmonitoring.component.ts ***!
  \*************************************************************************************/
/*! exports provided: TransactionmonitoringComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionmonitoringComponent", function() { return TransactionmonitoringComponent; });
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! xlsx */ "EUZL");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jspdf */ "i680");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jspdf-autotable */ "DaQG");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_services_servbpmp_trxmonitor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/servbpmp/trxmonitor.service */ "wite");
/* harmony import */ var _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../layout/mainmenulayout/mainmenulayout.component */ "SuKp");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/calendar */ "eO1q");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! primeng/table */ "rEr+");





















function TransactionmonitoringComponent_span_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"]("Max End Date : ", ctx_r0.birthday, "");
} }
function TransactionmonitoringComponent_span_39_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" Max End Date : ", ctx_r1.birthday, "");
} }
function TransactionmonitoringComponent_p_progressSpinner_56_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "p-progressSpinner", 34);
} }
function TransactionmonitoringComponent_p_table_57_ng_template_2_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "button", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function TransactionmonitoringComponent_p_table_57_ng_template_2_button_2_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](3); return ctx_r11.openPDF(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function TransactionmonitoringComponent_p_table_57_ng_template_2_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "button", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function TransactionmonitoringComponent_p_table_57_ng_template_2_button_4_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](3); return ctx_r13.exportexcel(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function TransactionmonitoringComponent_p_table_57_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, TransactionmonitoringComponent_p_table_57_ng_template_2_button_2_Template, 1, 0, "button", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " \u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, TransactionmonitoringComponent_p_table_57_ng_template_2_button_4_Template, 1, 0, "button", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "span", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "i", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "input", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("input", function TransactionmonitoringComponent_p_table_57_ng_template_2_Template_input_input_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r16); _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](1); return _r5.filterGlobal($event.target.value, "contains"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r6.isdataexist);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r6.isdataexist);
} }
function TransactionmonitoringComponent_p_table_57_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "th", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "LOG UBP TRX ID ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "p-sortIcon", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "th", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, "LOG BILL ID ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "p-sortIcon", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "th", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, "EVENT ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "p-sortIcon", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "th", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, "TRX TIMESTAMP ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](12, "p-sortIcon", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "th", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](14, "BILL KEY1 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](15, "p-sortIcon", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](16, "th", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](17, "BILL KEY2 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](18, "p-sortIcon", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "th", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](20, "BILL KEY3 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](21, "p-sortIcon", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](22, "th", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](23, "RESPONSE CODE ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](24, "p-sortIcon", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function TransactionmonitoringComponent_p_table_57_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](11, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "td", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const logdata_r17 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.LOG_UBP_TRX_ID, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.LOG_BILL_ID, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.EVENT, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.TRX_TIMESTAMP, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.BILL_KEY1, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.BILL_KEY2, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.BILL_KEY3, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", logdata_r17.RESPONSE_CODE, " ");
} }
const _c0 = function () { return ["LOG_UBP_TRX_ID"]; };
const _c1 = function () { return [5, 10, 25, 50]; };
function TransactionmonitoringComponent_p_table_57_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "p-table", 35, 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, TransactionmonitoringComponent_p_table_57_ng_template_2_Template, 8, 2, "ng-template", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, TransactionmonitoringComponent_p_table_57_ng_template_3_Template, 25, 0, "ng-template", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, TransactionmonitoringComponent_p_table_57_ng_template_4_Template, 17, 8, "ng-template", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("value", ctx_r3.logData)("scrollable", true)("globalFilterFields", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction0"](7, _c0))("rows", 5)("showCurrentPageReport", true)("rowsPerPageOptions", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction0"](8, _c1))("paginator", true);
} }
function TransactionmonitoringComponent_ng_template_61_Template(rf, ctx) { if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "p-button", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function TransactionmonitoringComponent_ng_template_61_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r19); const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r18.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "p-button", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function TransactionmonitoringComponent_ng_template_61_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r19); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r20.deleteGroup(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
const _c2 = function () { return { width: "20vw" }; };
class TransactionmonitoringComponent {
    // no: number = 1;
    constructor(authservice, dialogService, messageService, datepipe, trxmonitorService, 
    // private sysparamService: SysparamService,
    ServUsername) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.datepipe = datepipe;
        this.trxmonitorService = trxmonitorService;
        this.ServUsername = ServUsername;
        this.isdataexist = false;
        this.isactives = [];
        this.isactivesSelected = {};
        this.channels = [];
        this.channelSelected = {};
        this.transactiontype = [];
        this.transactiontypeSelected = {};
        this.fileName = "ExcelSheet.xlsx";
        this.startDate = new Date();
        this.display = false;
        this.displayPrv = false;
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.logData = [];
        this.isLoggedIn = false;
        this.sysParamData = [];
        this.range = 14;
        this.jsonUserInfo = {};
        this.username = "Unknown";
        this.div = "   ";
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [{ label: "Transaction Monitoring" }];
        // mantap
        this.isactives = [
            { label: "Sukses", value: "S" },
            { label: "Error", value: "E" },
            { label: "Timeout", value: "T" },
        ];
        this.transactiontype = [
            { label: "Incoming Transaction", value: "I" },
            { label: "Outgoing Transaction", value: "O" },
        ];
        this.logData = [];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.initialData();
    }
    refreshData() {
        this.isdataexist = false;
        this.logData = [];
        this.isFetching = true;
        let payload = {};
        // if (this.rangeDates != undefined) {
        //   start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
        //   end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
        // }
        // if (start_date) payload.start_date = start_date;
        // if (end_date) payload.end_date = end_date;
        if (this.startDate) {
            payload.startDate = this.datepipe.transform(this.startDate, "yyyy-MM-dd");
        }
        this.dateTime = new Date(this.startDate.getFullYear(), this.startDate.getMonth(), this.startDate.getDate() + Number(this.range));
        if (this.endDate > this.dateTime) {
            this.isLoggedIn = true;
            this.endDate = this.dateTime;
            payload.endDate = this.datepipe.transform(this.dateTime, "yyyy-MM-dd");
        }
        else if (this.endDate == null) {
            this.isLoggedIn = false;
            this.endDate = this.dateTime;
            payload.endDate = this.datepipe.transform(this.dateTime, "yyyy-MM-dd");
        }
        else {
            this.isLoggedIn = false;
            payload.endDate = this.datepipe.transform(this.endDate, "yyyy-MM-dd");
        }
        this.isLoggedIn = false;
        if (this.isactivesSelected)
            payload.status = this.isactivesSelected.value;
        if (this.channelSelected)
            payload.channel = this.channelSelected.channelcode;
        if (this.logUbpTrxId)
            payload.logUbpTrxId = this.logUbpTrxId;
        if (this.transactiontypeSelected)
            payload.trxtype = this.transactiontypeSelected.value;
        // console.log('JSON>>>', JSON.stringify(payload));
        this.trxmonitorService
            .postParam(payload)
            .subscribe((result) => {
            console.log(JSON.stringify(result));
            console.log(result);
            if (result.status == 200) {
                this.isdataexist = true;
                this.logData = result.data;
            }
            else {
                this.isdataexist = false;
                this.logData = [];
                let objtmp = {
                    id: "No record",
                    LOG_UBP_TRX_ID: "No record",
                    LOG_BILL_ID: "No record",
                    EVENT: "No record",
                    TRX_TIMESTAMP: "No record",
                    BILL_KEY1: "No record",
                    BILL_KEY2: "No record",
                    BILL_KEY3: "No record",
                    RESPONSE_CODE: "No record",
                };
                this.logData.push(objtmp);
            }
            this.isFetching = false;
        });
    }
    initialData() {
        this.isFetching = true;
        this.trxmonitorService
            .getAllTrxMon()
            .subscribe((result) => {
            // console.log(">>>>>>> " + JSON.stringify(result));
            if (result.status === 200) {
                // 202
                let objtmp = {
                    id: "No record",
                    LOG_UBP_TRX_ID: "No record",
                    LOG_BILL_ID: "No record",
                    EVENT: "No record",
                    TRX_TIMESTAMP: "No record",
                    BILL_KEY1: "No record",
                    BILL_KEY2: "No record",
                    BILL_KEY3: "No record",
                    RESPONSE_CODE: "No record",
                };
                this.logData.push(objtmp);
            }
            else {
                this.isdataexist = true;
                this.logData = result.data.results;
            }
            this.isFetching = false;
        });
    }
    maxDate() {
        const now = this.startDate;
        this.birthday = new Date(now.getFullYear(), now.getMonth(), now.getDate() + Number(this.range));
        var date = this.birthday, mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
        this.birthday = [day, mnth, date.getFullYear()].join("-");
        this.isLoggedIn = true;
        return this.birthday;
    }
    functionAmount() {
        var total = 0;
        for (var i = 0; i < this.logData.length; i++) {
            var product = this.logData[i].trx_amount;
            total += Number(product);
        }
        return total;
    }
    functionVolume() {
        return this.logData.length;
    }
    exportexcel() {
        let myDate = new Date();
        let datenow = this.datepipe.transform(myDate, "yyyyMMddHHmmss");
        // this.fileName = 'xls' + datenow + '.xlsx';
        this.fileName = "csv" + datenow + ".csv";
        /* table id is passed over here */
        let element = document.getElementById("excel-table");
        const ws = xlsx__WEBPACK_IMPORTED_MODULE_0__["utils"].table_to_sheet(element);
        /* generate workbook and add the worksheet */
        const wb = xlsx__WEBPACK_IMPORTED_MODULE_0__["utils"].book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_0__["utils"].book_append_sheet(wb, ws, "Sheet1");
        /* save to file */
        xlsx__WEBPACK_IMPORTED_MODULE_0__["writeFile"](wb, this.fileName);
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
    openPDF() {
        var number = 1;
        var doc = new jspdf__WEBPACK_IMPORTED_MODULE_1__["default"]("l", "mm", [297, 210]);
        var col = [
            [
                "NO",
                "UBP TRX ID",
                "BILL ID",
                "EVENT",
                "TRX TIMESTAMP",
                "BILL KEY1",
                "BILL KEY1",
                "BILL KEY1",
                "RESPONSE CODE",
            ],
        ];
        var rows = [];
        let myDate = new Date();
        let datenow = this.datepipe.transform(myDate, "yyyyMMddHHmmss");
        this.fileName = "pdf" + datenow + ".pdf";
        this.logData.forEach((element) => {
            let tid = "";
            // TRX_TIMESTAMP
            tid = this.datepipe.transform(element.TRX_TIMESTAMP, "yyyy-MM-dd HH:mm:ss");
            var temp = [
                number++,
                element.LOG_UBP_TRX_ID,
                element.LOG_BILL_ID,
                element.EVENT,
                tid,
                element.BILL_KEY1,
                element.BILL_KEY2,
                element.BILL_KEY3,
                element.RESPONSE_CODE,
            ];
            rows.push(temp);
        });
        // doc.autoTable(col, rows);
        // doc.text('Left aligned text', 15, 10);
        //header right
        doc.setFontSize(8);
        let dateGenerated = this.datepipe.transform(myDate, "dd/MM/yyyy");
        let timeGenerated = this.datepipe.transform(myDate, "HH:mm:ss");
        doc.text("DATE GENERATED :\t" + dateGenerated, doc.internal.pageSize.getWidth() - 60, 20, {
            align: "left",
        });
        doc.text("TIME GENERATED : \t" + timeGenerated, doc.internal.pageSize.getWidth() - 60, 25, {
            align: "left",
        });
        doc.text("GENERATED BY     :  \t" + this.ServUsername.getUsername(), doc.internal.pageSize.getWidth() - 60, 30, {
            align: "left",
        });
        // header page
        doc.setFontSize(12);
        doc.text("TRANSFER TRANSACTION DETAIL REPORT", doc.internal.pageSize.getWidth() / 2, 40, {
            align: "center",
        });
        // range date
        doc.setFontSize(12);
        let stardate = this.datepipe.transform(this.startDate, "yyyy/MM/dd");
        let endate = this.datepipe.transform(this.endDate, "yyyy/MM/dd"); // note
        doc.text("Date : " + stardate + " - " + endate, doc.internal.pageSize.getWidth() / 2, 45, {
            align: "center",
        });
        jspdf_autotable__WEBPACK_IMPORTED_MODULE_2___default()(doc, {
            head: col,
            body: rows,
            didDrawCell: (rows) => { },
            headStyles: { fillColor: [128, 128, 128] },
            styles: { fontSize: 8 },
            // columnStyles: {
            //   1: { cellWidth: 25 },
            //   3: { cellWidth: 18 },
            //   5: { cellWidth: 18 },
            //   8: { cellWidth: 18 },
            //   9: { cellWidth: 19 },
            // },
            margin: { top: 70, right: 14, bottom: 10, left: 14 },
            didDrawPage: function (data) {
                data.settings.margin.top = 25;
            },
        });
        const pageCount = doc.internal.pages.length - 1;
        // console.log('pageCount', pageCount);
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i);
            doc.text("Page " + String(i) + " from " + String(pageCount), doc.internal.pageSize.getWidth() - 60, 10);
        }
        doc.save(this.fileName);
    }
    removeFilter() {
        // this.startDate = null;
        this.startDate = new Date();
        // this.endDate = null;
        this.logUbpTrxId = "";
        this.endDate = null;
        this.isactivesSelected = [0];
        this.channelSelected = [0];
        this.transactiontypeSelected = [0];
        // this.refreshData();
        this.isLoggedIn = false;
    }
}
TransactionmonitoringComponent.ɵfac = function TransactionmonitoringComponent_Factory(t) { return new (t || TransactionmonitoringComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_5__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_servbpmp_trxmonitor_service__WEBPACK_IMPORTED_MODULE_8__["TrxmonitorService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_9__["MainmenulayoutComponent"])); };
TransactionmonitoringComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: TransactionmonitoringComponent, selectors: [["app-transactionmonitoring"]], decls: 62, vars: 25, consts: [[3, "home", "model"], [1, "wrapperinside"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "rangesDates", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], [2, "height", "8px"], ["dateFormat", "dd-mm-yy", "inputId", "startDate", 3, "ngModel", "showIcon", "ngModelChange", "click"], ["dateFormat", "dd-mm-yy", "inputId", "endDate", 3, "showIcon", "ngModel", "ngModelChange", "click"], ["style", "color: red;", 4, "ngIf"], ["for", "channels", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["id", "channels", "optionLabel", "channeltype", "placeholder", "Select Channel", 3, "options", "ngModel", "ngModelChange"], ["for", "logUbpTrxId", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["type", "text", "placeholder", "Transaction ID", "pInputText", "", 1, "p-inputtext-sm", 3, "ngModel", "ngModelChange"], ["for", "branch", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["id", "branch", "optionLabel", "label", "placeholder", "Select Status", 3, "options", "ngModel", "ngModelChange"], ["style", "color: transparent;", 4, "ngIf"], [2, "height", "15px"], ["for", "proxyaddrestype", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["id", "proxyaddrestype", "optionLabel", "label", "placeholder", "Select Transaction Type", 3, "ngModel", "options", "ngModelChange"], [2, "height", "30px"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", "label", "Search", 1, "p-button-info", 3, "routerLink", "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-refresh", "label", "Clear Filter", 1, "p-button-warning", 3, "click"], [1, "card"], ["class", "p-text-center", 4, "ngIf"], ["id", "excel-table", "scrollHeight", "300px", "scrollDirection", "both", "dataKey", "id", "styleClass", "p-datatable-customs p-datatable-gridlines", "currentPageReportTemplate", "Showing {first} to {last} of {totalRecords} entries", 3, "value", "scrollable", "globalFilterFields", "rows", "showCurrentPageReport", "rowsPerPageOptions", "paginator", 4, "ngIf"], ["header", "Delete Group", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [2, "color", "red"], [2, "color", "transparent"], [1, "p-text-center"], ["id", "excel-table", "scrollHeight", "300px", "scrollDirection", "both", "dataKey", "id", "styleClass", "p-datatable-customs p-datatable-gridlines", "currentPageReportTemplate", "Showing {first} to {last} of {totalRecords} entries", 3, "value", "scrollable", "globalFilterFields", "rows", "showCurrentPageReport", "rowsPerPageOptions", "paginator"], ["dt", ""], ["pTemplate", "caption"], ["pTemplate", "header"], ["pTemplate", "body"], [1, "p-d-flex"], [1, "p-input-icon-right", "p-ml-auto"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-file-excel", "label", "Export PDF", "class", "p-button-danger", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-file-pdf", "label", "Export CSV", "class", "p-button-success", "style", "margin-right: 10px;", 3, "click", 4, "ngIf"], [1, "p-input-icon-right"], [1, "pi", "pi-search"], ["pInputText", "", "type", "text", "placeholder", "Search Keyword", 3, "input"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-file-excel", "label", "Export PDF", 1, "p-button-danger", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-file-pdf", "label", "Export CSV", 1, "p-button-success", 2, "margin-right", "10px", 3, "click"], ["pSortableColumn", "LOG_UBP_TRX_ID", 1, "p-text-center", 2, "width", "200px"], ["field", "LOG_UBP_TRX_ID"], ["pSortableColumn", "LOG_BILL_ID", 1, "p-text-center", 2, "width", "200px"], ["field", "LOG_BILL_ID"], ["pSortableColumn", "EVENT", 1, "p-text-center", 2, "width", "200px"], ["field", "EVENT"], ["pSortableColumn", "TRX_TIMESTAMP", 1, "p-text-center", 2, "width", "200px"], ["field", "TRX_TIMESTAMP"], ["pSortableColumn", "BILL_KEY1", 1, "p-text-center", 2, "width", "200px"], ["field", "BILL_KEY1"], ["pSortableColumn", "BILL_KEY2", 1, "p-text-center", 2, "width", "200px"], ["field", "BILL_KEY2"], ["pSortableColumn", "BILL_KEY3", 1, "p-text-center", 2, "width", "200px"], ["field", "BILL_KEY3"], ["pSortableColumn", "RESPONSE_CODE", 1, "p-text-center", 2, "width", "200px"], ["field", "RESPONSE_CODE"], [1, "p-text-center", 2, "width", "200px"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function TransactionmonitoringComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, "Start Date :");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](12, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "p-calendar", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function TransactionmonitoringComponent_Template_p_calendar_ngModelChange_13_listener($event) { return ctx.startDate = $event; })("click", function TransactionmonitoringComponent_Template_p_calendar_click_13_listener() { return ctx.maxDate(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](16, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](17, "End Date :");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](18, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "p-calendar", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function TransactionmonitoringComponent_Template_p_calendar_ngModelChange_19_listener($event) { return ctx.endDate = $event; })("click", function TransactionmonitoringComponent_Template_p_calendar_click_19_listener() { return ctx.maxDate(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](20, TransactionmonitoringComponent_span_20_Template, 2, 1, "span", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](22, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](23, "label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](24, "Channel :");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](25, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](26, "p-dropdown", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function TransactionmonitoringComponent_Template_p_dropdown_ngModelChange_26_listener($event) { return ctx.channelSelected = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](27, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](28, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](29, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](30, "Transaction ID :");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](31, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](32, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function TransactionmonitoringComponent_Template_input_ngModelChange_32_listener($event) { return ctx.logUbpTrxId = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](33, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](34, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](35, "label", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](36, "Status :");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](37, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](38, "p-dropdown", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function TransactionmonitoringComponent_Template_p_dropdown_ngModelChange_38_listener($event) { return ctx.isactivesSelected = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](39, TransactionmonitoringComponent_span_39_Template, 2, 1, "span", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](40, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](41, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](42, "label", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](43, "Transaction Type :");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](44, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](45, "p-dropdown", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function TransactionmonitoringComponent_Template_p_dropdown_ngModelChange_45_listener($event) { return ctx.transactiontypeSelected = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](46, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](47, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](48, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](49, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](50, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function TransactionmonitoringComponent_Template_button_click_50_listener() { return ctx.refreshData(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](51, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](52, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](53, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function TransactionmonitoringComponent_Template_button_click_53_listener() { return ctx.removeFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](54, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](55, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](56, TransactionmonitoringComponent_p_progressSpinner_56_Template, 1, 0, "p-progressSpinner", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](57, TransactionmonitoringComponent_p_table_57_Template, 5, 9, "p-table", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](58, "p-dialog", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("visibleChange", function TransactionmonitoringComponent_Template_p_dialog_visibleChange_58_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](59, "p", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](60, "Are you sure want to delete this Organization?");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](61, TransactionmonitoringComponent_ng_template_61_Template, 2, 0, "ng-template", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngModel", ctx.startDate)("showIcon", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("showIcon", true)("ngModel", ctx.endDate);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoggedIn);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("options", ctx.channels)("ngModel", ctx.channelSelected);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngModel", ctx.logUbpTrxId);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("options", ctx.isactives)("ngModel", ctx.isactivesSelected);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoggedIn);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngModel", ctx.transactiontypeSelected)("options", ctx.transactiontype);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("routerLink", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.logData.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.logData.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction0"](24, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["Breadcrumb"], primeng_card__WEBPACK_IMPORTED_MODULE_11__["Card"], primeng_calendar__WEBPACK_IMPORTED_MODULE_12__["Calendar"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_14__["Dropdown"], _angular_forms__WEBPACK_IMPORTED_MODULE_13__["DefaultValueAccessor"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_15__["InputText"], primeng_button__WEBPACK_IMPORTED_MODULE_16__["ButtonDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_17__["RouterLink"], primeng_dialog__WEBPACK_IMPORTED_MODULE_18__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_6__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_19__["ProgressSpinner"], primeng_table__WEBPACK_IMPORTED_MODULE_20__["Table"], primeng_table__WEBPACK_IMPORTED_MODULE_20__["SortableColumn"], primeng_table__WEBPACK_IMPORTED_MODULE_20__["SortIcon"], primeng_button__WEBPACK_IMPORTED_MODULE_16__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0cmFuc2FjdGlvbm1vbml0b3JpbmcuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "FGci":
/*!*****************************************************!*\
  !*** ./src/app/pages/bpmp/rules/rules.component.ts ***!
  \*****************************************************/
/*! exports provided: RulesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RulesComponent", function() { return RulesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_servbpmp_rules_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/servbpmp/rules.service */ "tjdq");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "jIHw");













function RulesComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 12);
} }
function RulesComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function RulesComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); })("datapreview", function RulesComponent_div_3_Template_app_tablehelper_datapreview_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.viewData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.ruleslist)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("collinkaction", ctx_r1.orgcollinghref)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn)("colldate", 6);
} }
function RulesComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RulesComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RulesComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.deleteRules(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
const _c1 = function () { return { width: "50vw" }; };
class RulesComponent {
    constructor(authservice, dialogService, messageService, backend, rulesService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.backend = backend;
        this.rulesService = rulesService;
        this.viewDisplay = false;
        this.display = false;
        this.selectedRules = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [
            { label: "Rule ID", sort: "ruleId" },
            { label: "Channel ID", sort: "channelId" },
            { label: "Company ID", sort: "companyId" },
            { label: "Rule No", sort: "ruleNo" },
            { label: "Rule Type", sort: "ruleType" },
            { label: "Create Who", sort: "createdWho" },
            { label: "Create Date", sort: "createdDate" },
        ];
        this.orgcolname = [
            "RULE_ID",
            "CHANNEL_ID",
            "COMPANY_ID",
            "RULE_NO",
            "RULE_TYPE",
            "CREATE_WHO",
            "CREATE_DATE",
        ];
        this.orgcolhalign = [
            "p-text-center",
            "",
            "",
            "p-text-center",
            "p-text-center",
        ];
        this.orgcolwidth = [{ width: "110px" }, "", "", { width: "120px" }, ""];
        this.orgactionbtn = [1, 1, 1, 1, 1];
        this.orgaddbtn = { route: "detail", label: "Add Data" };
        this.ruleslist = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Rules" }];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.rulesService.getAllRules().subscribe((result) => {
            if (result.status === 202) {
                this.ruleslist = [];
                let objtmp = {
                    RULE_ID: "No records",
                    CHANNEL_ID: "No records",
                    COMPANY_ID: "No records",
                    RULE_NO: "No records",
                    CREATE_WHO: "No records",
                    CREATE_DATE: "No records",
                };
                this.ruleslist.push(objtmp);
            }
            else {
                this.ruleslist = result.data;
            }
            this.isFetching = false;
        });
    }
    viewData(data) {
        console.log("View" + JSON.stringify(data));
        this.viewDisplay = true;
        this.selectedRules = data;
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedRules = data;
    }
    deleteRules() {
        console.log(this.selectedRules);
        let rules = this.selectedRules;
        const payload = { rules };
        this.rulesService
            .deleteRules(payload.rules.id)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
RulesComponent.ɵfac = function RulesComponent_Factory(t) { return new (t || RulesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_servbpmp_rules_service__WEBPACK_IMPORTED_MODULE_5__["RulesService"])); };
RulesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: RulesComponent, selectors: [["app-rules"]], decls: 52, vars: 24, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete Rules", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "View Company", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", "p-md-3"], [1, "p-col-12", "p-md-1"], [1, "p-col-12", "p-md-8"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "collinkaction", "colclasshalign", "addbtnlink", "colldate", "datadeleted", "datapreview"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function RulesComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RulesComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, RulesComponent_div_3_Template, 2, 10, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function RulesComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Rules?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, RulesComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p-dialog", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function RulesComponent_Template_p_dialog_visibleChange_8_listener($event) { return ctx.viewDisplay = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Company ID");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Company Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Company Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Host Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Created Who");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Created Date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.ruleslist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.ruleslist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](22, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](23, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewDisplay)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedRules.RULE_ID);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedRules.CHANNEL_ID);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedRules.RULE_NO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedRules.RULE_TYPE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedRules.CREATE_WHO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedRules.CREATE_DATE);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_8__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_9__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_12__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJydWxlcy5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "GK+Y":
/*!********************************************************!*\
  !*** ./src/app/pages/homeadmin/homeadmin.component.ts ***!
  \********************************************************/
/*! exports provided: HomeadminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeadminComponent", function() { return HomeadminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_divider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/divider */ "lUkA");




class HomeadminComponent {
    constructor(authservice) {
        this.authservice = authservice;
        this.userInfo = {};
        this.app = {};
        this.applicationAvailable = 0;
        this.tokenID = "";
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.typeStrings = [{ id: "1", label: "In a Month" }, { id: "2", label: "In a Year" }];
        this.selectedType = { id: "1", label: "In a Month" };
        this.authservice.whoAmi().subscribe((value) => {
            console.log(">>> User Info : " + JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.applicationAvailable = this.userInfo.appscount - 1;
            this.app = this.userInfo.apps[0];
        });
        //   this.basicData = {
        //     labels: [5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17,18,19,20,21,22,23],
        //     datasets: [
        //         {
        //             label: 'Logged In Users',
        //             data: [5, 2, 1, 0, 9, 10, 1,2,10,0,1,7,7,8,1,0,2,2,3],
        //             fill: false,
        //             borderColor: '#42A5F5',
        //             tension: .4
        //         },
        //     ]
        // };
    }
}
HomeadminComponent.ɵfac = function HomeadminComponent_Factory(t) { return new (t || HomeadminComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); };
HomeadminComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeadminComponent, selectors: [["app-homeadmin"]], decls: 45, vars: 3, consts: [[2, "height", "20px"], [1, "wrapper"], [1, "p-grid"], [1, "p-col-12"], [1, "box"], ["header", "Welcome", "styleClass", "p-card-shadow p-cst-primary"], [1, "p-grid", 2, "padding-left", "20px"], [1, "p-col-4"], [1, "p-col-2", "p-rowcust-1"], [1, "p-col-1", "p-rowcust-1"], [1, "p-col-9", "p-rowcust-1"], [1, "p-col-1"], ["layout", "vertical"], [1, "p-col-7"], [1, "pi", "pi-sliders-h", "p-pt-10", 2, "font-size", "3.5rem!important"], [1, "p-col-10", "p-rowcust-1"], [2, "font-size", "3.5rem!important", "padding-top", "15px"]], template: function HomeadminComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p-card", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "User Id");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Fullname");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Application");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "p-divider", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "span", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "KOMI MANAGER");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.userInfo.userid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.userInfo.fullname);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.app.applabel);
    } }, directives: [primeng_card__WEBPACK_IMPORTED_MODULE_2__["Card"], primeng_divider__WEBPACK_IMPORTED_MODULE_3__["Divider"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lYWRtaW4uY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "HisP":
/*!*************************************************************************!*\
  !*** ./src/app/pages/root/change-password/change-password.component.ts ***!
  \*************************************************************************/
/*! exports provided: ChangePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function() { return ChangePasswordComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/usermanager.service */ "4nGI");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/button */ "jIHw");












function ChangePasswordComponent_div_7_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Old Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ChangePasswordComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ChangePasswordComponent_div_7_span_1_Template, 2, 0, "span", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r0.f.oldpassword.errors.required);
} }
function ChangePasswordComponent_div_13_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "New Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ChangePasswordComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ChangePasswordComponent_div_13_span_1_Template, 2, 0, "span", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.newpassword.errors.required);
} }
function ChangePasswordComponent_div_19_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Confirm Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ChangePasswordComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ChangePasswordComponent_div_19_span_1_Template, 2, 0, "span", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.confirmpassword.errors.required);
} }
function ChangePasswordComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Confirm Password must same with New Password");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ChangePasswordComponent_span_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r4.err.msg, " ");
} }
const _c0 = function () { return { width: "25vw" }; };
class ChangePasswordComponent {
    constructor(authservice, ref, usermanager, sessionStorage, config, dialogService, route, messageService, formBuilder) {
        this.authservice = authservice;
        this.ref = ref;
        this.usermanager = usermanager;
        this.sessionStorage = sessionStorage;
        this.config = config;
        this.dialogService = dialogService;
        this.route = route;
        this.messageService = messageService;
        this.formBuilder = formBuilder;
        this.changepass = {};
        this.submitted = false;
        this.confirmed = false;
        this.err = {};
    }
    ngOnInit() {
        this.inputForm = this.formBuilder.group({
            oldpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            newpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            confirmpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        });
    }
    get f() {
        return this.inputForm.controls;
    }
    get userFormControl() {
        return this.inputForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        if (this.inputForm.valid) {
            if (this.inputForm.value.newpassword == this.inputForm.value.confirmpassword) {
                this.authservice.whoAmi().subscribe((data) => {
                    if ((data.status = 200)) {
                        console.log(data.data);
                        this.changepass.id = data.data.id;
                        this.changepass.oldpassword = this.inputForm.value['oldpassword'];
                        this.changepass.newpassword = this.inputForm.value['newpassword'];
                        console.log(this.changepass);
                        this.usermanager.putPassword(this.changepass).subscribe((data) => {
                            if ((data.status = 200)) {
                                this.ref.close(data.status);
                            }
                            else {
                                this.ref.close(data.status);
                            }
                        }, (error) => {
                            this.err.msg = error.error.data;
                        });
                    }
                });
            }
            else {
                this.confirmed = true;
            }
        }
    }
    onCancel() {
        this.ref.destroy();
    }
}
ChangePasswordComponent.ɵfac = function ChangePasswordComponent_Factory(t) { return new (t || ChangePasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__["DynamicDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__["UsermanagerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_5__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__["DynamicDialogConfig"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"])); };
ChangePasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ChangePasswordComponent, selectors: [["app-change-password"]], decls: 30, vars: 9, consts: [[1, "form", 2, "padding", "10px 0 10px 0", 3, "formGroup", "ngSubmit"], [1, "p-fluid", "p-formgrid", "p-grid"], [1, "p-field", "p-col"], ["for", "oldpassword"], ["name", "oldpassword", "id", "oldpassword", "type", "password", "formControlName", "oldpassword", "pInputText", ""], ["class", "p-field", 4, "ngIf"], ["for", "newpassword"], ["name", "newpassword", "id", "newpassword", "type", "password", "formControlName", "newpassword", "pInputText", ""], ["for", "confirmpassword"], ["name", "confirmpassword", "id", "confirmpassword", "type", "password", "formControlName", "confirmpassword", "pInputText", ""], ["style", "color: red;", 4, "ngIf"], [1, "p-fluid", "p-formgrid", "p-grid", 2, "padding-top", "10px"], [1, "p-field", "p-col-8"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", 1, ""], ["pButton", "", "pRipple", "", "type", "button", "label", "Cancel", 1, "", 3, "click"], [1, "p-field"], [2, "color", "red"]], template: function ChangePasswordComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "form", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function ChangePasswordComponent_Template_form_ngSubmit_0_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "label", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Old Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "input", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ChangePasswordComponent_div_7_Template, 2, 1, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "New Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, ChangePasswordComponent_div_13_Template, 2, 1, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Confirm Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, ChangePasswordComponent_div_19_Template, 2, 1, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, ChangePasswordComponent_div_20_Template, 3, 0, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, ChangePasswordComponent_span_21_Template, 2, 1, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ChangePasswordComponent_Template_button_click_29_listener() { return ctx.onCancel(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.inputForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](8, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.oldpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.newpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.confirmpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.confirmed);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.err.msg);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_8__["InputText"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], primeng_button__WEBPACK_IMPORTED_MODULE_10__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGFuZ2UtcGFzc3dvcmQuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "INgj":
/*!***********************************************************!*\
  !*** ./src/app/pages/bpmp/rulesreq/rulesreq.component.ts ***!
  \***********************************************************/
/*! exports provided: RulesreqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RulesreqComponent", function() { return RulesreqComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/root/organization.service */ "u0Pv");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "jIHw");













function RulesreqComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 6);
} }
function RulesreqComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function RulesreqComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.rulesreqlist)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("collinkaction", ctx_r1.orgcollinghref)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn);
} }
function RulesreqComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RulesreqComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RulesreqComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.deleteOrganization(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
class RulesreqComponent {
    constructor(authservice, dialogService, messageService, backend, organizationService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.backend = backend;
        this.organizationService = organizationService;
        this.display = false;
        this.selectedOrganization = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [
            { label: "Rule ID", sort: "ruleid" },
            { label: "Channel ID", sort: "channelid" },
            { label: "Company ID", sort: "companyid" },
            { label: "Rule No", sort: "ruleno" },
            { label: "Rule Type", sort: "ruletype" },
            { label: "Create Who", sort: "createdwho" },
            { label: "Create Date", sort: "createddate" },
        ];
        this.orgcolname = [
            "RULE_ID",
            "CHANNEL_ID",
            "COMPANY_ID",
            "RULE_NO",
            "RULE_TYPE",
            "CREATE_WHO",
            "CREATE_DATE",
        ];
        this.orgcolhalign = [
            "p-text-center",
            "",
            "",
            "p-text-center",
            "p-text-center",
        ];
        this.orgcolwidth = [{ width: "110px" }, "", "", { width: "120px" }, ""];
        this.orgactionbtn = [1, 1, 1, 1, 1];
        this.orgaddbtn = { route: "detail", label: "Add Data" };
        this.rulesreqlist = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Pending Request Rules" }];
        // this.authservice.whoAmi().subscribe((value) =>{
        //   // console.log(">>> User Info : "+JSON.stringify(value));
        //     this.userInfo = value.data;
        //     this.tokenID = value.tokenId;
        // });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.backend
            .basePost(
        // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
        "api/rulesreq/getRulesTemp", {}, false)
            .subscribe((data) => {
            //console.log("Company length>>>>>>> " + JSON.stringify(data));
            // if ((data.status = 200)) {
            //   this.organizationService
            //     .retriveOrgByTenant()
            //     .subscribe((orgall: BackendResponse) => {
            //console.log('>>>>>>> ' + data.data.data.length);
            this.rulesreqlist = data.data.data;
            if (this.rulesreqlist.length < 1) {
                let objtmp = {
                    orgcode: "No records",
                    orgname: "No records",
                    orgdescription: "No records",
                    application: "No records",
                    created_by: "No records",
                };
                this.rulesreqlist = [];
                this.rulesreqlist.push(objtmp);
            }
            this.isFetching = false;
            //     });
            // }
        });
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedOrganization = data;
    }
    deleteOrganization() {
        console.log(this.selectedOrganization);
        let organization = this.selectedOrganization;
        const payload = { organization };
        this.organizationService
            .deleteOrg(payload)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
RulesreqComponent.ɵfac = function RulesreqComponent_Factory(t) { return new (t || RulesreqComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__["OrganizationService"])); };
RulesreqComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: RulesreqComponent, selectors: [["app-rulesreq"]], decls: 8, vars: 11, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete Organization", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "collinkaction", "colclasshalign", "addbtnlink", "datadeleted"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function RulesreqComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RulesreqComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, RulesreqComponent_div_3_Template, 2, 9, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function RulesreqComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Organization?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, RulesreqComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.rulesreqlist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.rulesreqlist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_8__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_9__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_12__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJydWxlc3JlcS5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "J4FQ":
/*!*************************************************************!*\
  !*** ./src/app/pages/root/eventlogs/eventlogs.component.ts ***!
  \*************************************************************/
/*! exports provided: EventlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventlogsComponent", function() { return EventlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class EventlogsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Eventlogs Monitoring' }
        ];
    }
}
EventlogsComponent.ɵfac = function EventlogsComponent_Factory(t) { return new (t || EventlogsComponent)(); };
EventlogsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: EventlogsComponent, selectors: [["app-eventlogs"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function EventlogsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJldmVudGxvZ3MuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "Jdzd":
/*!*******************************************************************!*\
  !*** ./src/app/services/forgotpassword/forgotpassword.service.ts ***!
  \*******************************************************************/
/*! exports provided: ForgotpasswordService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordService", function() { return ForgotpasswordService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class ForgotpasswordService {
    constructor(service) {
        this.service = service;
    }
    forgot(payload) {
        const url = 'adm/send/forgotpassword';
        return this.service.post(url, payload);
    }
    reset(payload) {
        const url = `adm/send/resetpassword/${payload.id}`;
        return this.service.post(url, payload);
    }
    verifikasi(payload) {
        const url = 'adm/send/verifikasi';
        return this.service.post(url, payload);
    }
    verifikasiemail(payload) {
        const url = `adm/send/verifikasiemail/${payload.id}`;
        return this.service.post(url, payload);
    }
}
ForgotpasswordService.ɵfac = function ForgotpasswordService_Factory(t) { return new (t || ForgotpasswordService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
ForgotpasswordService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ForgotpasswordService, factory: ForgotpasswordService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "JxjP":
/*!*********************************************************!*\
  !*** ./src/app/pages/bpmp/company/company.component.ts ***!
  \*********************************************************/
/*! exports provided: CompanyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyComponent", function() { return CompanyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/root/organization.service */ "u0Pv");
/* harmony import */ var src_app_services_servbpmp_company_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/servbpmp/company.service */ "pjdP");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");














function CompanyComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 12);
} }
function CompanyComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function CompanyComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); })("datapreview", function CompanyComponent_div_3_Template_app_tablehelper_datapreview_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.viewData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.companylist)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("collinkaction", ctx_r1.orgcollinghref)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn)("colldate", 5);
} }
function CompanyComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CompanyComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CompanyComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.deleteCompany(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
const _c1 = function () { return { width: "50vw" }; };
class CompanyComponent {
    constructor(authservice, dialogService, messageService, backend, organizationService, companyService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.backend = backend;
        this.organizationService = organizationService;
        this.companyService = companyService;
        this.viewDisplay = false;
        this.display = false;
        this.selectedCompany = [];
        this.isFetching = false;
        this.companyInfo = {};
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [
            { label: "Company ID", sort: "COMPANY_ID" },
            { label: "Company Code", sort: "COMPANY_CODE" },
            { label: "Company Name", sort: "COMPANY_NAME" },
            { label: "Host Code", sort: "HOST_CODE" },
            { label: "Created Who", sort: "CREATE_WHO" },
            { label: "Created Date", sort: "CREATE_DATE" },
        ];
        this.orgcolname = [
            "COMPANY_ID",
            "COMPANY_CODE",
            "COMPANY_NAME",
            "HOST_CODE",
            "CREATE_WHO",
            "CREATE_DATE",
        ];
        this.orgcolhalign = [
            "p-text-center",
            "p-text-center",
            "p-text-center",
            "p-text-center",
            "p-text-center",
        ];
        this.orgcolwidth = [{ width: "110px" }, "", "", { width: "120px" }, ""];
        this.orgactionbtn = [1, 1, 1, 1, 1];
        this.orgaddbtn = { route: "detail", label: "Add Data" };
        this.companylist = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Company List" }];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.companyService.getAllCompany().subscribe((result) => {
            if (result.status === 202) {
                this.companylist = [];
                let objtmp = {
                    COMPANY_ID: "No records",
                    COMPANY_CODE: "No records",
                    COMPANY_NAME: "No records",
                    CREATE_WHO: "No records",
                    CREATE_DATE: "No records",
                    HOST_CODE: "No records",
                };
                this.companylist.push(objtmp);
            }
            else {
                this.companylist = result.data;
            }
            this.isFetching = false;
        });
    }
    viewData(data) {
        console.log("View" + JSON.stringify(data));
        this.viewDisplay = true;
        this.selectedCompany = data;
    }
    deleteConfirmation(data) {
        console.log("Delete Confrim " + JSON.stringify(data));
        this.display = true;
        this.selectedCompany = data;
    }
    deleteCompany() {
        console.log(this.selectedCompany);
        let company = this.selectedCompany;
        const payload = { company };
        this.companyService
            .deleteCompany(payload.company.id)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
CompanyComponent.ɵfac = function CompanyComponent_Factory(t) { return new (t || CompanyComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__["OrganizationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_servbpmp_company_service__WEBPACK_IMPORTED_MODULE_6__["CompanyService"])); };
CompanyComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CompanyComponent, selectors: [["app-company"]], decls: 52, vars: 24, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", " Delete Company", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "View Company", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", "p-md-3"], [1, "p-col-12", "p-md-1"], [1, "p-col-12", "p-md-8"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "collinkaction", "colclasshalign", "addbtnlink", "colldate", "datadeleted", "datapreview"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function CompanyComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CompanyComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, CompanyComponent_div_3_Template, 2, 10, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function CompanyComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Company?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, CompanyComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p-dialog", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function CompanyComponent_Template_p_dialog_visibleChange_8_listener($event) { return ctx.viewDisplay = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Company ID");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Company Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Company Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Host Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Created Who");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Created Date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.companylist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.companylist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](22, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](23, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewDisplay)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedCompany.COMPANY_ID);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedCompany.COMPANY_CODE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedCompany.COMPANY_NAME);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedCompany.HOST_CODE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedCompany.CREATE_WHO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedCompany.CREATE_DATE);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_9__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_10__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb21wYW55LmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "MUFR":
/*!*******************************************************************!*\
  !*** ./src/app/layout/fullmenulayout/fullmenulayout.component.ts ***!
  \*******************************************************************/
/*! exports provided: FullmenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullmenulayoutComponent", function() { return FullmenulayoutComponent; });
/* harmony import */ var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/_files/appadmin.json */ "xlh6");
var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! src/app/_files/appadmin.json */ "xlh6", 1);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/menubar */ "b1Ni");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");














function FullmenulayoutComponent_ng_template_29_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FullmenulayoutComponent_ng_template_29_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r1.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FullmenulayoutComponent_ng_template_29_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r3.signOut(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
class FullmenulayoutComponent {
    constructor(messageService, sessionStorage, route, formBuilder, backend, authservice, router) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.router = router;
        this.username = "Unknown";
        this.companytitle = "Unknown";
        this.display = false;
        this.isProcess = false;
        // authToken = "";
        this.jsonUserInfo = {};
        this.title = "krakatoa";
    }
    ngOnInit() {
        console.log("###########APP COMPONENT Admin #############");
        this.isProcess = true;
        this.authservice.whoAmi().subscribe((value) => {
            this.jsonUserInfo = value;
            this.username = this.jsonUserInfo.data.fullname;
            this.companytitle = this.jsonUserInfo.data.tnname;
            this.items = [
                {
                    label: this.username,
                    icon: "pi pi-fw pi-user",
                    items: [
                        {
                            label: "My Account",
                            icon: "pi pi-user-edit",
                        },
                        {
                            label: "",
                            separator: true,
                        },
                        {
                            label: "Sign out",
                            icon: "pi pi-fw pi-sign-out",
                            command: () => {
                                // this.delete();
                                this.showDialog();
                            },
                        },
                    ],
                },
                {
                    label: "Help",
                    icon: "pi pi-fw pi-info-circle",
                },
            ];
            let lvlTn = parseInt(this.jsonUserInfo.data.leveltenant);
            console.log(">> Level tenant " + this.jsonUserInfo.data.leveltenant);
            console.log(this.jsonUserInfo);
            if (lvlTn == 0) {
                this.sidemenus = src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__;
            }
            this.isProcess = false;
        });
    }
    showDialog() {
        this.display = true;
    }
    signOut() {
        this.display = false;
        this.isProcess = true;
        console.log("Ini harusnya keluar");
        this.backend
            .post("adm/auth/signout", { appid: 4, appname: "bpmp" }, false)
            .subscribe((data) => {
            // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
            this.isProcess = false;
            if (data.status === 200) {
                this.authservice.loggedOut();
                this.router.navigate(["/auth/login"]);
            }
            else {
            }
        }, (error) => { });
        // this.isProcess= true
    }
}
FullmenulayoutComponent.ɵfac = function FullmenulayoutComponent_Factory(t) { return new (t || FullmenulayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
FullmenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: FullmenulayoutComponent, selectors: [["app-fullmenulayout"]], decls: 30, vars: 10, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], [3, "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], ["id", "wrapper"], ["id", "contentliquid2"], ["id", "contentfull"], [1, "p-toolbar-group-left", 2, "padding-left", "10px"], [1, "p-pt-15", "p-pb-15", "forcecenter"], ["src", "assets/logos/krakatoalogo.png", "height", "17.5", 2, "padding-left", "40px", "padding-right", "40px"], [2, "margin", "0"], [2, "color", "#08736f"], [1, "p-toolbar-group-right"], [3, "model"], ["header", "Sign Out", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function FullmenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-blockUI", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "h1", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "h3", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "One Solution of ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](24, "p-menubar", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "p-dialog", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function FullmenulayoutComponent_Template_p_dialog_visibleChange_26_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "p", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "Sign out from application?, if yes you should log in again from the beginning");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, FullmenulayoutComponent_ng_template_29_Template, 2, 0, "ng-template", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.companytitle, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx.items);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](9, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_8__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__["ProgressSpinner"], primeng_toolbar__WEBPACK_IMPORTED_MODULE_10__["Toolbar"], primeng_menubar__WEBPACK_IMPORTED_MODULE_11__["Menubar"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"], primeng_dialog__WEBPACK_IMPORTED_MODULE_12__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmdWxsbWVudWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "QdSJ":
/*!******************************************************************!*\
  !*** ./src/app/pages/forgotpassword/forgotpassword.component.ts ***!
  \******************************************************************/
/*! exports provided: ForgotpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordComponent", function() { return ForgotpasswordComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/forgotpassword/forgotpassword.service */ "Jdzd");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/button */ "jIHw");
















function ForgotpasswordComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ForgotpasswordComponent_div_13_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Email is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ForgotpasswordComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ForgotpasswordComponent_div_13_span_1_Template, 2, 0, "span", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.emailid.errors.required);
} }
function ForgotpasswordComponent_ng_template_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "360px" }; };
class ForgotpasswordComponent {
    constructor(messageService, sessionStorage, route, formBuilder, backend, authservice, forgotpasswordService, location) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.forgotpasswordService = forgotpasswordService;
        this.location = location;
        this.blockedDocument = false;
        //  userid = "";
        this.secret = '';
        this.errorMsg = '';
        this.isProcess = false;
        this.submitted = false;
    }
    ngOnInit() {
        this.userForm = this.formBuilder.group({
            emailid: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        });
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.userForm.controls;
    }
    get userFormControl() {
        return this.userForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        const payload = {
            email: this.userForm.value['emailid'],
        };
        console.log('payload', payload);
        this.forgotpasswordService
            .forgot(payload)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopCenterInfo('The mail has beed send');
                setTimeout(() => {
                    this.route.navigate(['/auth/login']);
                }, 4000);
            }
            else if (resp.status === 201) {
                this.showTopCenterErr('Email is not found');
                setTimeout(() => {
                    this.route.navigate(['/auth/login']);
                }, 4000);
            }
        });
    }
    showTopCenterInfo(message) {
        this.messageService.add({
            severity: 'info',
            summary: 'Confirmed',
            detail: message,
        });
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: message,
        });
    }
    blockDocument() {
        this.blockedDocument = true;
        //  setTimeout(() => {
        //      this.blockedDocument = false;
        //      this.showTopCenterErr("Invalid user and password!")
        //  }, 3000);
    }
}
ForgotpasswordComponent.ɵfac = function ForgotpasswordComponent_Factory(t) { return new (t || ForgotpasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_7__["ForgotpasswordService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"])); };
ForgotpasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ForgotpasswordComponent, selectors: [["app-forgotpassword"]], decls: 17, vars: 6, consts: [[3, "target", "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], [1, "form", 3, "formGroup", "ngSubmit"], ["header", "Forgot Password", "styleClass", "p-card-shadow p-header-w50"], ["pTemplate", "header"], [1, "p-fluid"], [1, "p-field"], ["for", "emailid", 1, "labelpb"], ["id", "emailid", "name", "emailid", "type", "text", "required", "", "formControlName", "emailid", "pInputText", ""], ["class", "p-field", 4, "ngIf"], [2, "height", "0.3rem"], ["pTemplate", "footer"], [1, "p-text-center"], ["alt", "tai", "src", "assets/logos/komiportal.png"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], [1, "p-text-right"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Submit", 1, "p-primary-btn"]], template: function ForgotpasswordComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-blockUI", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function ForgotpasswordComponent_Template_form_ngSubmit_5_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ForgotpasswordComponent_ng_template_7_Template, 2, 0, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, ForgotpasswordComponent_div_13_Template, 2, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, ForgotpasswordComponent_ng_template_15_Template, 2, 0, "ng-template", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.userForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](5, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.emailid.errors);
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_9__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_11__["Card"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__["InputText"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_13__["Messages"], primeng_button__WEBPACK_IMPORTED_MODULE_14__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb3Jnb3RwYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */", "body[_ngcontent-%COMP%] {\n        background: #007dc5\n    }\n\n    section[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%)\n    }"] });


/***/ }),

/***/ "RnhZ":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "K/tc",
	"./af.js": "K/tc",
	"./ar": "jnO4",
	"./ar-dz": "o1bE",
	"./ar-dz.js": "o1bE",
	"./ar-kw": "Qj4J",
	"./ar-kw.js": "Qj4J",
	"./ar-ly": "HP3h",
	"./ar-ly.js": "HP3h",
	"./ar-ma": "CoRJ",
	"./ar-ma.js": "CoRJ",
	"./ar-sa": "gjCT",
	"./ar-sa.js": "gjCT",
	"./ar-tn": "bYM6",
	"./ar-tn.js": "bYM6",
	"./ar.js": "jnO4",
	"./az": "SFxW",
	"./az.js": "SFxW",
	"./be": "H8ED",
	"./be.js": "H8ED",
	"./bg": "hKrs",
	"./bg.js": "hKrs",
	"./bm": "p/rL",
	"./bm.js": "p/rL",
	"./bn": "kEOa",
	"./bn-bd": "loYQ",
	"./bn-bd.js": "loYQ",
	"./bn.js": "kEOa",
	"./bo": "0mo+",
	"./bo.js": "0mo+",
	"./br": "aIdf",
	"./br.js": "aIdf",
	"./bs": "JVSJ",
	"./bs.js": "JVSJ",
	"./ca": "1xZ4",
	"./ca.js": "1xZ4",
	"./cs": "PA2r",
	"./cs.js": "PA2r",
	"./cv": "A+xa",
	"./cv.js": "A+xa",
	"./cy": "l5ep",
	"./cy.js": "l5ep",
	"./da": "DxQv",
	"./da.js": "DxQv",
	"./de": "tGlX",
	"./de-at": "s+uk",
	"./de-at.js": "s+uk",
	"./de-ch": "u3GI",
	"./de-ch.js": "u3GI",
	"./de.js": "tGlX",
	"./dv": "WYrj",
	"./dv.js": "WYrj",
	"./el": "jUeY",
	"./el.js": "jUeY",
	"./en-au": "Dmvi",
	"./en-au.js": "Dmvi",
	"./en-ca": "OIYi",
	"./en-ca.js": "OIYi",
	"./en-gb": "Oaa7",
	"./en-gb.js": "Oaa7",
	"./en-ie": "4dOw",
	"./en-ie.js": "4dOw",
	"./en-il": "czMo",
	"./en-il.js": "czMo",
	"./en-in": "7C5Q",
	"./en-in.js": "7C5Q",
	"./en-nz": "b1Dy",
	"./en-nz.js": "b1Dy",
	"./en-sg": "t+mt",
	"./en-sg.js": "t+mt",
	"./eo": "Zduo",
	"./eo.js": "Zduo",
	"./es": "iYuL",
	"./es-do": "CjzT",
	"./es-do.js": "CjzT",
	"./es-mx": "tbfe",
	"./es-mx.js": "tbfe",
	"./es-us": "Vclq",
	"./es-us.js": "Vclq",
	"./es.js": "iYuL",
	"./et": "7BjC",
	"./et.js": "7BjC",
	"./eu": "D/JM",
	"./eu.js": "D/JM",
	"./fa": "jfSC",
	"./fa.js": "jfSC",
	"./fi": "gekB",
	"./fi.js": "gekB",
	"./fil": "1ppg",
	"./fil.js": "1ppg",
	"./fo": "ByF4",
	"./fo.js": "ByF4",
	"./fr": "nyYc",
	"./fr-ca": "2fjn",
	"./fr-ca.js": "2fjn",
	"./fr-ch": "Dkky",
	"./fr-ch.js": "Dkky",
	"./fr.js": "nyYc",
	"./fy": "cRix",
	"./fy.js": "cRix",
	"./ga": "USCx",
	"./ga.js": "USCx",
	"./gd": "9rRi",
	"./gd.js": "9rRi",
	"./gl": "iEDd",
	"./gl.js": "iEDd",
	"./gom-deva": "qvJo",
	"./gom-deva.js": "qvJo",
	"./gom-latn": "DKr+",
	"./gom-latn.js": "DKr+",
	"./gu": "4MV3",
	"./gu.js": "4MV3",
	"./he": "x6pH",
	"./he.js": "x6pH",
	"./hi": "3E1r",
	"./hi.js": "3E1r",
	"./hr": "S6ln",
	"./hr.js": "S6ln",
	"./hu": "WxRl",
	"./hu.js": "WxRl",
	"./hy-am": "1rYy",
	"./hy-am.js": "1rYy",
	"./id": "UDhR",
	"./id.js": "UDhR",
	"./is": "BVg3",
	"./is.js": "BVg3",
	"./it": "bpih",
	"./it-ch": "bxKX",
	"./it-ch.js": "bxKX",
	"./it.js": "bpih",
	"./ja": "B55N",
	"./ja.js": "B55N",
	"./jv": "tUCv",
	"./jv.js": "tUCv",
	"./ka": "IBtZ",
	"./ka.js": "IBtZ",
	"./kk": "bXm7",
	"./kk.js": "bXm7",
	"./km": "6B0Y",
	"./km.js": "6B0Y",
	"./kn": "PpIw",
	"./kn.js": "PpIw",
	"./ko": "Ivi+",
	"./ko.js": "Ivi+",
	"./ku": "JCF/",
	"./ku.js": "JCF/",
	"./ky": "lgnt",
	"./ky.js": "lgnt",
	"./lb": "RAwQ",
	"./lb.js": "RAwQ",
	"./lo": "sp3z",
	"./lo.js": "sp3z",
	"./lt": "JvlW",
	"./lt.js": "JvlW",
	"./lv": "uXwI",
	"./lv.js": "uXwI",
	"./me": "KTz0",
	"./me.js": "KTz0",
	"./mi": "aIsn",
	"./mi.js": "aIsn",
	"./mk": "aQkU",
	"./mk.js": "aQkU",
	"./ml": "AvvY",
	"./ml.js": "AvvY",
	"./mn": "lYtQ",
	"./mn.js": "lYtQ",
	"./mr": "Ob0Z",
	"./mr.js": "Ob0Z",
	"./ms": "6+QB",
	"./ms-my": "ZAMP",
	"./ms-my.js": "ZAMP",
	"./ms.js": "6+QB",
	"./mt": "G0Uy",
	"./mt.js": "G0Uy",
	"./my": "honF",
	"./my.js": "honF",
	"./nb": "bOMt",
	"./nb.js": "bOMt",
	"./ne": "OjkT",
	"./ne.js": "OjkT",
	"./nl": "+s0g",
	"./nl-be": "2ykv",
	"./nl-be.js": "2ykv",
	"./nl.js": "+s0g",
	"./nn": "uEye",
	"./nn.js": "uEye",
	"./oc-lnc": "Fnuy",
	"./oc-lnc.js": "Fnuy",
	"./pa-in": "8/+R",
	"./pa-in.js": "8/+R",
	"./pl": "jVdC",
	"./pl.js": "jVdC",
	"./pt": "8mBD",
	"./pt-br": "0tRk",
	"./pt-br.js": "0tRk",
	"./pt.js": "8mBD",
	"./ro": "lyxo",
	"./ro.js": "lyxo",
	"./ru": "lXzo",
	"./ru.js": "lXzo",
	"./sd": "Z4QM",
	"./sd.js": "Z4QM",
	"./se": "//9w",
	"./se.js": "//9w",
	"./si": "7aV9",
	"./si.js": "7aV9",
	"./sk": "e+ae",
	"./sk.js": "e+ae",
	"./sl": "gVVK",
	"./sl.js": "gVVK",
	"./sq": "yPMs",
	"./sq.js": "yPMs",
	"./sr": "zx6S",
	"./sr-cyrl": "E+lV",
	"./sr-cyrl.js": "E+lV",
	"./sr.js": "zx6S",
	"./ss": "Ur1D",
	"./ss.js": "Ur1D",
	"./sv": "X709",
	"./sv.js": "X709",
	"./sw": "dNwA",
	"./sw.js": "dNwA",
	"./ta": "PeUW",
	"./ta.js": "PeUW",
	"./te": "XLvN",
	"./te.js": "XLvN",
	"./tet": "V2x9",
	"./tet.js": "V2x9",
	"./tg": "Oxv6",
	"./tg.js": "Oxv6",
	"./th": "EOgW",
	"./th.js": "EOgW",
	"./tk": "Wv91",
	"./tk.js": "Wv91",
	"./tl-ph": "Dzi0",
	"./tl-ph.js": "Dzi0",
	"./tlh": "z3Vd",
	"./tlh.js": "z3Vd",
	"./tr": "DoHr",
	"./tr.js": "DoHr",
	"./tzl": "z1FC",
	"./tzl.js": "z1FC",
	"./tzm": "wQk9",
	"./tzm-latn": "tT3J",
	"./tzm-latn.js": "tT3J",
	"./tzm.js": "wQk9",
	"./ug-cn": "YRex",
	"./ug-cn.js": "YRex",
	"./uk": "raLr",
	"./uk.js": "raLr",
	"./ur": "UpQW",
	"./ur.js": "UpQW",
	"./uz": "Loxo",
	"./uz-latn": "AQ68",
	"./uz-latn.js": "AQ68",
	"./uz.js": "Loxo",
	"./vi": "KSF8",
	"./vi.js": "KSF8",
	"./x-pseudo": "/X5v",
	"./x-pseudo.js": "/X5v",
	"./yo": "fzPg",
	"./yo.js": "fzPg",
	"./zh-cn": "XDpg",
	"./zh-cn.js": "XDpg",
	"./zh-hk": "SatO",
	"./zh-hk.js": "SatO",
	"./zh-mo": "OmwH",
	"./zh-mo.js": "OmwH",
	"./zh-tw": "kOpN",
	"./zh-tw.js": "kOpN"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "RnhZ";

/***/ }),

/***/ "SuKp":
/*!*******************************************************************!*\
  !*** ./src/app/layout/mainmenulayout/mainmenulayout.component.ts ***!
  \*******************************************************************/
/*! exports provided: MainmenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainmenulayoutComponent", function() { return MainmenulayoutComponent; });
/* harmony import */ var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/_files/appadmin.json */ "xlh6");
var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! src/app/_files/appadmin.json */ "xlh6", 1);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/menubar */ "b1Ni");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "jIHw");













function MainmenulayoutComponent_ng_template_27_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MainmenulayoutComponent_ng_template_27_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r2.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MainmenulayoutComponent_ng_template_27_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r4.signOut(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function MainmenulayoutComponent_ng_template_31_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MainmenulayoutComponent_ng_template_31_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r5.changePasswordDisplay = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MainmenulayoutComponent_ng_template_31_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); ctx_r7.toProfile(); return ctx_r7.changePasswordDisplay = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { "width": "199px" }; };
const _c1 = function () { return { width: "20vw" }; };
const _c2 = function () { return { width: "50vw" }; };
class MainmenulayoutComponent {
    constructor(messageService, sessionStorage, routeA, route, formBuilder, backend, authservice, router) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.routeA = routeA;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.router = router;
        this.title = "Unknown";
        this.username = "Unknown";
        this.companytitle = "Unknown";
        this.applabel = "Unknown";
        this.display = false;
        this.isProcess = false;
        this.changePasswordDisplay = false;
        this.passwordMessage = "";
        // authToken = "";
        this.jsonUserInfo = {};
    }
    ngOnInit() {
        console.log("###########APP COMPONENT#############");
        this.title = this.routeA.snapshot.data["title"];
        this.isProcess = true;
        this.authservice.whoAmi().subscribe((value) => {
            var _a, _b, _c, _d;
            this.jsonUserInfo = value;
            // console.log(">> INSIDE Main Menu "+JSON.stringify(this.jsonUserInfo));
            // INSIDE Main Menu {"status":200,"data":{"id":"1","fullname":"KKTCIMB","userid":"kktcimb@cimb.co.id","idtenant":"1","idsubtenant":"0","leveltenant":"0","tnname":"PT Bank CIMB","tnstatus":1,"tnflag":1,"tnparentid":0,"cdtenant":"9624","bioemailactive":"kktcimb@cimb.co.id","biophoneactive":"5554444","bioaddress":"Kantor Pusat","bionik":"3400342324342","bionpwp":null,"bioidtipenik":1,"bioidcorel":3,"orgid":0,"appscount":3,"apps":[{"id":"1","expiredate":"2022-05-23","id_application":"1","paidstatus":"1","active":1,"defaultactive":null,"appname":"default","applabel":"Default","description":"Krakatoa Administer"},{"id":"2","expiredate":"2022-05-24","id_application":"2","paidstatus":"1","active":1,"defaultactive":null,"appname":"vam","applabel":"Virtual account manager","description":"This application for maintained and monitoring Virtual Account Management purposed. And this is general operation form Banking Needed "},{"id":"3","expiredate":"2021-07-13","id_application":"3","paidstatus":"1","active":1,"defaultactive":null,"appname":"crm","applabel":"Bank CRM","description":"This contain all modules for Customer Relationship Management purpose on front end banking, and it can be customed as the users needed"}],"sidemenus":[],"iat":1629743103}}
            this.username = this.jsonUserInfo.data.fullname;
            this.companytitle = this.jsonUserInfo.data.tnname;
            // console.log("object :>> ", this.jsonUserInfo.data);
            this.applabel = this.jsonUserInfo.data.apps[0].applabel;
            let lastLogin;
            lastLogin = new Date(this.jsonUserInfo.data.last_login);
            lastLogin = lastLogin.toLocaleString();
            this.items = [
                {
                    label: this.username,
                    icon: "pi pi-fw pi-user",
                    items: [
                        {
                            label: "My Account",
                            icon: "pi pi-user-edit",
                            command: () => {
                                this.toProfile();
                            },
                        },
                        {
                            label: "",
                            separator: true,
                        },
                        {
                            label: "Sign out",
                            icon: "pi pi-fw pi-sign-out",
                            command: () => {
                                // this.delete();
                                this.showDialog();
                            },
                        },
                    ],
                },
                {
                    label: `Last Login : ${lastLogin}`,
                },
            ];
            let lvlTn = parseInt(this.jsonUserInfo.data.leveltenant);
            // console.log(this.jsonUserInfo);
            if ((_b = (_a = this.jsonUserInfo) === null || _a === void 0 ? void 0 : _a.data.notif) === null || _b === void 0 ? void 0 : _b.changePassword) {
                this.passwordMessage = (_d = (_c = this.jsonUserInfo) === null || _c === void 0 ? void 0 : _c.data.notif) === null || _d === void 0 ? void 0 : _d.changePassword;
                this.changePasswordDisplay = true;
            }
            if (lvlTn == 0) {
                var home = {
                    label: "Dashboard",
                    icon: "pi pi-fw pi-home",
                    routerLink: "/mgm/home",
                };
                this.sidemenus.push(home);
                this.sidemenus.push(src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__);
            }
            else {
                let home = {
                    label: "Dashboard",
                    icon: "pi pi-fw pi-home",
                    routerLink: "/mgm/home",
                };
                this.sidemenus = this.jsonUserInfo.data.sidemenus;
                this.sidemenus.unshift(home);
            }
            this.isProcess = false;
        });
    }
    toProfile() {
        console.log("profile");
        this.router.navigate(["/mgm/profile"]);
    }
    getUsername() {
        this.username = this.jsonUserInfo.data.fullname;
        return this.username;
    }
    showDialog() {
        this.display = true;
    }
    signOut() {
        this.display = false;
        this.isProcess = true;
        let payload = { appid: 2, appname: "bpmp" };
        console.log("Ini harusnya keluar >> " + JSON.stringify(payload));
        this.backend.post("adm/auth/signout", payload, false).subscribe((data) => {
            // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
            this.isProcess = false;
            if (data.status === 200) {
                this.authservice.loggedOut();
                this.router.navigate(["/auth/login"]);
            }
            else {
            }
        }, (error) => { });
        // this.isProcess= true
    }
}
MainmenulayoutComponent.ɵfac = function MainmenulayoutComponent_Factory(t) { return new (t || MainmenulayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
MainmenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: MainmenulayoutComponent, selectors: [["app-mainmenulayout"]], decls: 32, vars: 22, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], ["id", "wrapper"], ["id", "contentliquid"], ["id", "content"], ["styleClass", "p-toolbar-bb"], [1, "p-toolbar-group-left", 2, "padding-left", "10px"], [2, "margin", "0px", "padding", "5px 0 5px 0", "font-weight", "600"], [1, "p-toolbar-group-right", 2, "padding-top", "5px"], [3, "model"], ["id", "leftcolumn", 1, "full-height"], [1, "p-pt-15", "p-pb-15", "forcecenter"], ["src", "assets/logos/commonwealthbank.png", "height", "46.5", 2, "padding-left", "5px"], ["header", "Sign Out", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "Password will be expired", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"], ["label", "Later", "styleClass", "p-button-text", 3, "click"], ["label", "Now", "styleClass", "p-button-text", 3, "click"]], template: function MainmenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "p-toolbar", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "h2", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "p-menubar", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "p-panelMenu", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "p-dialog", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function MainmenulayoutComponent_Template_p_dialog_visibleChange_24_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "Sign out from application?, if yes you should log in again from the beginning");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](27, MainmenulayoutComponent_ng_template_27_Template, 2, 0, "ng-template", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "p-dialog", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function MainmenulayoutComponent_Template_p_dialog_visibleChange_28_listener($event) { return ctx.changePasswordDisplay = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, MainmenulayoutComponent_ng_template_31_Template, 2, 0, "ng-template", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.applabel, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx.items);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](19, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx.sidemenus);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](20, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](21, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.changePasswordDisplay)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.passwordMessage);
    } }, directives: [primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__["Toolbar"], primeng_menubar__WEBPACK_IMPORTED_MODULE_9__["Menubar"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"], primeng_panelmenu__WEBPACK_IMPORTED_MODULE_10__["PanelMenu"], primeng_dialog__WEBPACK_IMPORTED_MODULE_11__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_12__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtYWlubWVudWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");


class AppComponent {
    constructor(route) {
        this.route = route;
    }
    ngOnInit() {
        this.config = this.route.snapshot.params['config'];
        // console.log("###########APP COMPONENT############# "+this.config)
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "UZDb":
/*!*********************************************************!*\
  !*** ./src/app/pages/bpmp/channel/channel.component.ts ***!
  \*********************************************************/
/*! exports provided: ChannelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChannelComponent", function() { return ChannelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_servbpmp_channel_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/servbpmp/channel.service */ "xjfA");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "jIHw");













function ChannelComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 12);
} }
function ChannelComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function ChannelComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); })("datapreview", function ChannelComponent_div_3_Template_app_tablehelper_datapreview_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.viewData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.channellist)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("collinkaction", ctx_r1.orgcollinghref)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn)("colldate", 5);
} }
function ChannelComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ChannelComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ChannelComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.deleteChannel(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
const _c1 = function () { return { width: "50vw" }; };
class ChannelComponent {
    constructor(authservice, dialogService, messageService, backend, channelService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.backend = backend;
        this.channelService = channelService;
        this.viewApprove = false;
        this.viewDisplay = false;
        this.display = false;
        this.selectedChannel = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [
            { label: "Channel Id", sort: "CHANNEL_ID" },
            { label: "Channel Name", sort: "CHANNEL_NAME" },
            { label: "Channel Code", sort: "CHANNEL_CODE" },
            { label: "Merchant Type", sort: "MERCHANT_TYPE" },
            { label: "Create Who", sort: "CREATE_WHO" },
            { label: "Create Date", sort: "CREATE_DATE" },
            { label: "Change Who", sort: "CHANGE_WHO" },
            { label: "Change Date", sort: "CHANGE_DATE" },
        ];
        this.orgcolname = [
            "CHANNEL_ID",
            "CHANNEL_NAME",
            "CHANNEL_CODE",
            "MERCHANT_TYPE",
            "CREATE_WHO",
            "CREATE_DATE",
            "CHANGE_WHO",
            "CHANGE_DATE",
        ];
        this.orgcolhalign = ["", "", "", "", "", ""];
        this.orgcolwidth = [{ width: "110px" }, "", "", { width: "120px" }, ""];
        this.orgactionbtn = [1, 1, 1, 1, 1, 1, 1, 1];
        this.orgaddbtn = { route: "detail", label: "Add Data" };
        this.channellist = [];
        this.channellist2 = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Channel" }];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.channelService.getAllChannel().subscribe((result) => {
            this.channellist2 = [];
            if (result.status === 202) {
                this.channellist = [];
                let objtmp = {
                    CHANNEL_ID: "No records",
                    CHANNEL_NAME: "No records",
                    CHANNEL_CODE: "No records",
                    MERCHANT_TYPE: "No records",
                    CREATE_WHO: "No records",
                    CREATE_DATE: "No records",
                    CHANGE_WHO: "No records",
                    CHANGE_DATE: "No records",
                };
                this.channellist.push(objtmp);
            }
            else {
                this.channellist = result.data;
            }
            this.isFetching = false;
        });
    }
    viewData(data) {
        console.log(data);
        this.viewDisplay = true;
        this.selectedChannel = data;
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedChannel = data;
    }
    deleteChannel() {
        let channel = this.selectedChannel;
        const payload = { channel };
        this.channelService
            .deleteChannel(payload.channel)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
ChannelComponent.ɵfac = function ChannelComponent_Factory(t) { return new (t || ChannelComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_servbpmp_channel_service__WEBPACK_IMPORTED_MODULE_5__["ChannelService"])); };
ChannelComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ChannelComponent, selectors: [["app-channel"]], decls: 66, vars: 26, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete Channel", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "View Channel", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", "p-md-3"], [1, "p-col-12", "p-md-1"], [1, "p-col-12", "p-md-8"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "collinkaction", "colclasshalign", "addbtnlink", "colldate", "datadeleted", "datapreview"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function ChannelComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ChannelComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ChannelComponent_div_3_Template, 2, 10, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ChannelComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Channel?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ChannelComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p-dialog", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ChannelComponent_Template_p_dialog_visibleChange_8_listener($event) { return ctx.viewDisplay = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Channel ID");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Merchant Type");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Channel Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Create Who");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Created Date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Change Who");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Change Date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Channel Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channellist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channellist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](24, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](25, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewDisplay)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CHANNEL_ID);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.MERCHANT_TYPE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CHANNEL_NAME);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CREATE_WHO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CREATE_DATE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CHANGE_WHO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CHANGE_DATE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannel.CHANNEL_CODE);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_8__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_9__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_12__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGFubmVsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "WQ6N":
/*!***************************************************************!*\
  !*** ./src/app/layout/nomenulayout/nomenulayout.component.ts ***!
  \***************************************************************/
/*! exports provided: NomenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NomenulayoutComponent", function() { return NomenulayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");


class NomenulayoutComponent {
    constructor(routeA) {
        this.routeA = routeA;
        this.title = "Unknown";
    }
    ngOnInit() {
        this.title = this.routeA.snapshot.data['title'];
        console;
    }
}
NomenulayoutComponent.ɵfac = function NomenulayoutComponent_Factory(t) { return new (t || NomenulayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"])); };
NomenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NomenulayoutComponent, selectors: [["app-nomenulayout"]], decls: 10, vars: 1, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], ["id", "wrapper"]], template: function NomenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.title);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJub21lbnVsYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "XfbB":
/*!********************************************************!*\
  !*** ./src/app/services/root/group-service.service.ts ***!
  \********************************************************/
/*! exports provided: GroupServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupServiceService", function() { return GroupServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class GroupServiceService {
    constructor(service) {
        this.service = service;
        this.reqGroup = {};
        this.groupSetupData = {};
        this.allModules = [];
        this.modules = [];
        this.menus = [];
        this.groupEditData = {};
        this.haveModuleAndMenus = false;
    }
    getAllModules(id) {
        const url = `adm/accmod/${id}`;
        return this.service.get(url);
    }
    getAllMenuByModuleId(id) {
        console.log(">>>>>>>>>>>>>>>>>", id);
        const url = `adm/accmod/menusmodule/${id}`;
        return this.service.get(url);
    }
    getAllMenuByModuleIdEdit(id, grpid) {
        const url = `adm/accmod/menusmodule/${id}`;
        return this.service.get(url);
    }
    getAllMenuByModuleIdAndGroupId(id, groupid) {
        const url = `adm/accmod/menusmodule/${id}/${groupid}`;
        return this.service.get(url);
    }
    getModuleWithMenu() {
        const url = 'adm/group/getallmodulewithmenu';
        return this.service.get(url);
    }
    regisGroup(payload) {
        console.log(JSON.stringify(payload));
        const url = 'adm/group/regisGroup';
        return this.service.post(url, payload);
    }
    editGroup(payload) {
        const url = 'adm/group/editGroup';
        return this.service.post(url, payload);
    }
    editGroupActive(payload) {
        const url = 'adm/group/editGroupActive';
        return this.service.post(url, payload);
    }
    getAllGroup() {
        const url = 'adm/group/getAllGroup';
        return this.service.get(url);
    }
    getAllGroupForData() {
        const url = 'adm/group/getAllGroupForData';
        return this.service.get(url);
    }
    getExternalGroup() {
        const url = 'adm/group/getExternalGroup';
        return this.service.get(url);
    }
    getGroupDetail(id) {
        const url = `adm/group/getDetailsGroup/${id}`;
        return this.service.get(url);
    }
    deleteGroup(payload) {
        const url = 'adm/group/deleteGroup';
        return this.service.post(url, payload);
    }
}
GroupServiceService.ɵfac = function GroupServiceService_Factory(t) { return new (t || GroupServiceService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
GroupServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: GroupServiceService, factory: GroupServiceService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "YBn2":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/bpmp/channel/channeldetail/channeldetail.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ChanneldetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChanneldetailComponent", function() { return ChanneldetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/root/group-service.service */ "XfbB");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_services_servbpmp_channel_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/servbpmp/channel.service */ "xjfA");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/button */ "jIHw");
















function ChanneldetailComponent_form_3_div_10_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Channel ID is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_10_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r1.f.channelId.errors.required);
} }
function ChanneldetailComponent_form_3_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Channel Name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_16_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r2.f.channelName.errors.required);
} }
function ChanneldetailComponent_form_3_div_22_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Channel Code is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_22_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.f.channelCode.errors.required);
} }
function ChanneldetailComponent_form_3_div_28_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Merchant Type is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_28_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_28_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r4.f.merchantType.errors.required);
} }
function ChanneldetailComponent_form_3_div_35_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Create Who is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_35_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_35_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r5.f.createWho.errors.required);
} }
function ChanneldetailComponent_form_3_div_41_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Create Date is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_41_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_41_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r6.f.createDate.errors.required);
} }
function ChanneldetailComponent_form_3_div_47_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Change Who is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_47_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_47_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r7.f.changeWho.errors.required);
} }
function ChanneldetailComponent_form_3_div_53_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Change Date is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ChanneldetailComponent_form_3_div_53_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ChanneldetailComponent_form_3_div_53_span_1_Template, 2, 0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r8.f.changeDate.errors.required);
} }
function ChanneldetailComponent_form_3_Template(rf, ctx) { if (rf & 1) {
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "form", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngSubmit", function ChanneldetailComponent_form_3_Template_form_ngSubmit_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r18); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r17.onSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "p-card", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "label", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, "Channel ID * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "input", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](10, ChanneldetailComponent_form_3_div_10_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](11, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "label", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](14, "Channel Name * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](15, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](16, ChanneldetailComponent_form_3_div_16_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](17, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "label", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](20, "Channel Code * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](21, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](22, ChanneldetailComponent_form_3_div_22_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](23, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](24, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](25, "label", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](26, "Merchant Type * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](27, "input", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](28, ChanneldetailComponent_form_3_div_28_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](29, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](30, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](31, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](32, "label", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](33, "Create Who * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](34, "input", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](35, ChanneldetailComponent_form_3_div_35_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](36, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](37, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](38, "label", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](39, "Create Date * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](40, "input", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](41, ChanneldetailComponent_form_3_div_41_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](42, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](43, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](44, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](45, "Change Who * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](46, "input", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](47, ChanneldetailComponent_form_3_div_47_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](48, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](49, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](50, "label", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](51, "Change Date * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](52, "input", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](53, ChanneldetailComponent_form_3_div_53_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](54, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](55, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](56, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](57, "button", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("formGroup", ctx_r0.groupForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.channelId.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.channelName.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.channelCode.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.merchantType.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.createWho.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.createDate.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.changeWho.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.changeDate.errors);
} }
class ChanneldetailComponent {
    constructor(router, activatedRoute, formBuilder, authservice, groupService, messageService, location, channelService) {
        var _a;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.authservice = authservice;
        this.groupService = groupService;
        this.messageService = messageService;
        this.location = location;
        this.channelService = channelService;
        this.extraInfo = {};
        this.isEdit = false;
        this.userId = null;
        this.stateOptions = [];
        this.stateOptionsEdit = [];
        this.leveltenant = 0;
        this.userInfo = {};
        this.selectedApps = [];
        this.tokenID = "";
        this.submitted = false;
        this.orgsData = [];
        this.appInfoActive = {};
        this.orgSuggest = {};
        this.user = {};
        this.formatedOrg = [];
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
        // console.log(">>>>>>>>>>> " + this.extraInfo);
        if (checkurl)
            this.isEdit = true;
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [
            {
                label: "Channel Management",
                command: (event) => {
                    this.location.back();
                },
                url: "",
            },
            { label: this.isEdit ? "Edit data" : "Add data" },
        ];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.leveltenant = this.userInfo.leveltenant;
            this.groupForm = this.formBuilder.group({
                channelId: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                merchantType: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                channelName: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                createWho: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                createDate: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                changeWho: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                changeDate: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                channelCode: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            });
            if (this.isEdit) {
                if (this.activatedRoute.snapshot.paramMap.get("id")) {
                    this.userId = this.activatedRoute.snapshot.paramMap.get("id");
                    this.channelService
                        .getChannel(this.userId)
                        .subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        // console.log("Data edit user " + JSON.stringify(result.data));
                        let cDate = result.data.CREATE_DATE;
                        var createDate = moment__WEBPACK_IMPORTED_MODULE_2__(cDate).utc().format("YYYY-MM-DD");
                        let chDate = result.data.CHANGE_DATE;
                        var changeDate = moment__WEBPACK_IMPORTED_MODULE_2__(chDate).utc().format("YYYY-MM-DD");
                        this.user.channelId = result.data.CHANNEL_ID;
                        this.user.merchantType = result.data.MERCHANT_TYPE;
                        this.user.channelName = result.data.CHANNEL_NAME;
                        this.user.createWho = result.data.CREATE_WHO;
                        this.user.createDate = createDate;
                        this.user.changeWho = result.data.CHANGE_WHO;
                        this.user.changeDate = changeDate;
                        this.user.channelCode = result.data.CHANNEL_CODE;
                        this.groupForm.patchValue({
                            channelId: this.user.channelId,
                            merchantType: this.user.merchantType,
                            channelName: this.user.channelName,
                            createWho: this.user.createWho,
                            createDate: this.user.createDate,
                            changeWho: this.user.changeWho,
                            changeDate: this.user.changeDate,
                            channelCode: this.user.channelCode,
                        });
                        this.groupForm.controls["channelId"].disable();
                        console.log(this.user);
                    }));
                }
            }
        });
    }
    get f() {
        return this.groupForm.controls;
    }
    // formatOrgData(data: any) {
    //   data.map((dt: any) => {
    //     let formated: any = {};
    //     formated.name = dt.groupname;
    //     formated.id = dt.id;
    //     this.formatedOrg.push(formated);
    //   });
    // }
    // filterOrg(event: any) {
    //   let filtered: any[] = [];
    //   let query = event.query;
    //   for (let i = 0; i < this.formatedOrg.length; i++) {
    //     let country = this.formatedOrg[i];
    //     if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
    //       filtered.push(country);
    //     }
    //   }
    //   this.orgSuggest = filtered;
    // }
    onSubmit() {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s;
        this.submitted = true;
        // console.log(">>>>>>>>IS NULL >>>> " + this.leveltenant);
        // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
        if (this.groupForm.valid) {
            // event?.preventDefault;
            var groupacl = (_a = this.groupForm.get("orgobj")) === null || _a === void 0 ? void 0 : _a.value;
            let payload = {};
            if (!this.isEdit) {
                payload = {
                    CHANNEL_ID: (_b = this.groupForm.get("channelId")) === null || _b === void 0 ? void 0 : _b.value,
                    MERCHANT_TYPE: (_c = this.groupForm.get("merchantType")) === null || _c === void 0 ? void 0 : _c.value,
                    CHANNEL_NAME: (_d = this.groupForm.get("channelName")) === null || _d === void 0 ? void 0 : _d.value,
                    CREATE_WHO: (_e = this.groupForm.get("createWho")) === null || _e === void 0 ? void 0 : _e.value,
                    CREATE_DATE: (_f = this.groupForm.get("createDate")) === null || _f === void 0 ? void 0 : _f.value,
                    CHANGE_WHO: (_g = this.groupForm.get("changeWho")) === null || _g === void 0 ? void 0 : _g.value,
                    CHANGE_DATE: (_h = this.groupForm.get("changeDate")) === null || _h === void 0 ? void 0 : _h.value,
                    CHANNEL_CODE: (_j = this.groupForm.get("channelCode")) === null || _j === void 0 ? void 0 : _j.value,
                };
                console.log(">>>>>>>> payload " + JSON.stringify(payload));
                this.channelService.insertChannel(payload).subscribe((result) => {
                    if (result.status === 200) {
                        this.location.back();
                    }
                }, (err) => {
                    console.log(err);
                    this.showTopCenterErr(err.error.data);
                });
            }
            else {
                payload = {
                    id: this.userId,
                    CHANNEL_ID: (_k = this.groupForm.get("channelId")) === null || _k === void 0 ? void 0 : _k.value,
                    MERCHANT_TYPE: (_l = this.groupForm.get("merchantType")) === null || _l === void 0 ? void 0 : _l.value,
                    CHANNEL_NAME: (_m = this.groupForm.get("channelName")) === null || _m === void 0 ? void 0 : _m.value,
                    CREATE_WHO: (_o = this.groupForm.get("createWho")) === null || _o === void 0 ? void 0 : _o.value,
                    CREATE_DATE: (_p = this.groupForm.get("createDate")) === null || _p === void 0 ? void 0 : _p.value,
                    CHANGE_WHO: (_q = this.groupForm.get("changeWho")) === null || _q === void 0 ? void 0 : _q.value,
                    CHANGE_DATE: (_r = this.groupForm.get("changeDate")) === null || _r === void 0 ? void 0 : _r.value,
                    CHANNEL_CODE: (_s = this.groupForm.get("channelCode")) === null || _s === void 0 ? void 0 : _s.value,
                };
                console.log(">>>>>>>> payload " + JSON.stringify(payload));
                this.channelService
                    .updateChannel(payload)
                    .subscribe((result) => {
                    if (result.status === 200) {
                        this.location.back();
                    }
                });
            }
        }
        // console.log(this.groupForm.valid);
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: "error",
            summary: "Error",
            detail: message,
        });
    }
}
ChanneldetailComponent.ɵfac = function ChanneldetailComponent_Factory(t) { return new (t || ChanneldetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_6__["GroupServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](src_app_services_servbpmp_channel_service__WEBPACK_IMPORTED_MODULE_9__["ChannelService"])); };
ChanneldetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: ChanneldetailComponent, selectors: [["app-channeldetail"]], decls: 4, vars: 3, consts: [[3, "model", "home"], [1, "wrapperinside"], ["style", "padding: 2px;", 3, "formGroup", "ngSubmit", 4, "ngIf"], [2, "padding", "2px", 3, "formGroup", "ngSubmit"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "channelId", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "channelId", "formControlName", "channelId", "id", "channelId", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "p-field", 4, "ngIf"], [2, "height", "15px"], ["for", "channelName", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "channelName", "formControlName", "channelName", "id", "channelName", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "channelCode", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "channelCode", "formControlName", "channelCode", "id", "channelCode", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "merchantType", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "merchantType", "formControlName", "merchantType", "id", "merchantType", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "createWho", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "createWho", "formControlName", "createWho", "id", "createWho", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "createDate", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "createDate", "formControlName", "createDate", "id", "createDate", "type", "date", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "changeWho", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "changeWho", "formControlName", "changeWho", "id", "changeWho", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "changeDate", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "changeDate", "formControlName", "changeDate", "id", "changeDate", "type", "date", "pInputText", "", "required", "", 1, "p-mt-2"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success"], [1, "p-field"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"]], template: function ChanneldetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ChanneldetailComponent_form_3_Template, 58, 9, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("model", ctx.breadcrumbs)("home", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.groupForm);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["Breadcrumb"], primeng_messages__WEBPACK_IMPORTED_MODULE_11__["Messages"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_12__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], primeng_button__WEBPACK_IMPORTED_MODULE_14__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGFubmVsZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/splitbutton */ "Wq6t");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/menubar */ "b1Ni");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/panel */ "7CaW");
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/checkbox */ "Ji6n");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_divider__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! primeng/divider */ "lUkA");
/* harmony import */ var _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./layout/mainmenulayout/mainmenulayout.component */ "SuKp");
/* harmony import */ var _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./layout/nomenulayout/nomenulayout.component */ "WQ6N");
/* harmony import */ var _pages_homeadmin_homeadmin_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./pages/homeadmin/homeadmin.component */ "GK+Y");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./pages/home/home.component */ "1LmZ");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! primeng/selectbutton */ "5o1E");
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! primeng/steps */ "KcHJ");
/* harmony import */ var primeng_inputnumber__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/inputnumber */ "Ks7X");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! primeng/autocomplete */ "V5BG");
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! primeng/chart */ "I5S5");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_picklist__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! primeng/picklist */ "iHf9");
/* harmony import */ var primeng_tooltip__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! primeng/tooltip */ "xlun");
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! primeng/listbox */ "+07z");
/* harmony import */ var primeng_orderlist__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! primeng/orderlist */ "cQJI");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! primeng/calendar */ "eO1q");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! primeng/message */ "FMpt");
/* harmony import */ var ngx_gauge__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ngx-gauge */ "Mumg");
/* harmony import */ var primeng_chip__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! primeng/chip */ "wxlm");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! primeng/toast */ "Gxio");
/* harmony import */ var _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationgroup.component */ "sBLG");
/* harmony import */ var _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./pages/root/users/users.component */ "brkH");
/* harmony import */ var _pages_root_applications_applications_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./pages/root/applications/applications.component */ "hwT4");
/* harmony import */ var _pages_root_smtpaccounts_smtpaccounts_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./pages/root/smtpaccounts/smtpaccounts.component */ "qUOm");
/* harmony import */ var _pages_root_oauthsettings_oauthsettings_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./pages/root/oauthsettings/oauthsettings.component */ "brE3");
/* harmony import */ var _pages_root_eventlogs_eventlogs_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./pages/root/eventlogs/eventlogs.component */ "J4FQ");
/* harmony import */ var _pages_root_resourceusage_resourceusage_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./pages/root/resourceusage/resourceusage.component */ "q50L");
/* harmony import */ var _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./pages/errorpage/errorpage.component */ "bzlq");
/* harmony import */ var _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./layout/backmenulayout/backmenulayout.component */ "nMyi");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./pages/login/login.component */ "D8EZ");
/* harmony import */ var _pages_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./pages/forgotpassword/forgotpassword.component */ "QdSJ");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _interceptors_interceptor_http_service__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./interceptors/interceptor-http.service */ "mnfZ");
/* harmony import */ var _layout_fullmenulayout_fullmenulayout_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./layout/fullmenulayout/fullmenulayout.component */ "MUFR");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationdetail/applicationdetail.component */ "7YUa");
/* harmony import */ var _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./pages/root/users/userdetail/userdetail.component */ "z+Ab");
/* harmony import */ var _env_env_service_provider__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./env/env.service.provider */ "mPzt");
/* harmony import */ var _pages_bpmp_bpmphome_bpmpintercept_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./pages/bpmp/bpmphome/bpmpintercept.component */ "AUuv");
/* harmony import */ var _pages_root_systemparam_systemparam_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./pages/root/systemparam/systemparam.component */ "AYsR");
/* harmony import */ var _pages_root_systemparam_systemparamdetail_systemparamdetail_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./pages/root/systemparam/systemparamdetail/systemparamdetail.component */ "e3kg");
/* harmony import */ var ng_recaptcha__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ng-recaptcha */ "jCJ1");
/* harmony import */ var _pages_root_profile_profile_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./pages/root/profile/profile.component */ "pZjL");
/* harmony import */ var _pages_root_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./pages/root/change-password/change-password.component */ "HisP");
/* harmony import */ var _pages_forgotpassword_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./pages/forgotpassword/resetpassword/resetpassword.component */ "+G24");
/* harmony import */ var _pages_forgotpassword_verifikasiemail_verifikasiemail_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./pages/forgotpassword/verifikasiemail/verifikasiemail.component */ "pSR5");
/* harmony import */ var _pages_bpmp_bpmphome_bpmphome_component__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./pages/bpmp/bpmphome/bpmphome.component */ "lhZ7");
/* harmony import */ var _pages_bpmp_company_company_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./pages/bpmp/company/company.component */ "JxjP");
/* harmony import */ var _pages_bpmp_company_companydetail_companydetail_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./pages/bpmp/company/companydetail/companydetail.component */ "6t8E");
/* harmony import */ var _pages_bpmp_channel_channel_component__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! ./pages/bpmp/channel/channel.component */ "UZDb");
/* harmony import */ var _pages_bpmp_channel_channeldetail_channeldetail_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./pages/bpmp/channel/channeldetail/channeldetail.component */ "YBn2");
/* harmony import */ var _pages_bpmp_companyreq_companyreq_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./pages/bpmp/companyreq/companyreq.component */ "dJaT");
/* harmony import */ var _pages_bpmp_companyreq_companyreqdetail_companyreqdetail_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./pages/bpmp/companyreq/companyreqdetail/companyreqdetail.component */ "ZgGJ");
/* harmony import */ var _pages_bpmp_channelreq_channelreq_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./pages/bpmp/channelreq/channelreq.component */ "eUMV");
/* harmony import */ var _pages_bpmp_rulesreq_rulesreq_component__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./pages/bpmp/rulesreq/rulesreq.component */ "INgj");
/* harmony import */ var _pages_bpmp_rulesreq_rulesreqdetail_rulesreqdetail_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./pages/bpmp/rulesreq/rulesreqdetail/rulesreqdetail.component */ "fRat");
/* harmony import */ var _pages_bpmp_rules_rules_component__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./pages/bpmp/rules/rules.component */ "FGci");
/* harmony import */ var _pages_bpmp_rules_rulesdetail_rulesdetail_component__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./pages/bpmp/rules/rulesdetail/rulesdetail.component */ "7LVI");
/* harmony import */ var _pages_bpmp_transactionmonitoring_transactionmonitoring_component__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./pages/bpmp/transactionmonitoring/transactionmonitoring.component */ "Es3d");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! @angular/core */ "fXoL");




















































































class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_82__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_82__["ɵɵdefineInjector"]({ providers: [
        _env_env_service_provider__WEBPACK_IMPORTED_MODULE_60__["EnvServiceProvider"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_17__["DynamicDialogRef"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_17__["DynamicDialogConfig"],
        primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_54__["HTTP_INTERCEPTORS"],
            useClass: _interceptors_interceptor_http_service__WEBPACK_IMPORTED_MODULE_55__["InterceptorHttpService"],
            multi: true,
        },
        { provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_4__["HashLocationStrategy"] },
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_17__["DialogService"],
        _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"],
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_54__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            primeng_card__WEBPACK_IMPORTED_MODULE_7__["CardModule"],
            primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__["ToolbarModule"],
            primeng_button__WEBPACK_IMPORTED_MODULE_10__["ButtonModule"],
            primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__["InputTextModule"],
            primeng_splitbutton__WEBPACK_IMPORTED_MODULE_11__["SplitButtonModule"],
            primeng_menubar__WEBPACK_IMPORTED_MODULE_12__["MenubarModule"],
            primeng_panelmenu__WEBPACK_IMPORTED_MODULE_13__["PanelMenuModule"],
            primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_23__["BreadcrumbModule"],
            primeng_dropdown__WEBPACK_IMPORTED_MODULE_24__["DropdownModule"],
            primeng_chart__WEBPACK_IMPORTED_MODULE_30__["ChartModule"],
            primeng_blockui__WEBPACK_IMPORTED_MODULE_25__["BlockUIModule"],
            primeng_divider__WEBPACK_IMPORTED_MODULE_18__["DividerModule"],
            primeng_progressspinner__WEBPACK_IMPORTED_MODULE_41__["ProgressSpinnerModule"],
            primeng_toast__WEBPACK_IMPORTED_MODULE_42__["ToastModule"],
            primeng_messages__WEBPACK_IMPORTED_MODULE_37__["MessagesModule"],
            primeng_message__WEBPACK_IMPORTED_MODULE_38__["MessageModule"],
            primeng_dialog__WEBPACK_IMPORTED_MODULE_14__["DialogModule"],
            primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_17__["DynamicDialogModule"],
            primeng_panel__WEBPACK_IMPORTED_MODULE_15__["PanelModule"],
            primeng_table__WEBPACK_IMPORTED_MODULE_31__["TableModule"],
            primeng_selectbutton__WEBPACK_IMPORTED_MODULE_26__["SelectButtonModule"],
            primeng_steps__WEBPACK_IMPORTED_MODULE_27__["StepsModule"],
            primeng_inputnumber__WEBPACK_IMPORTED_MODULE_28__["InputNumberModule"],
            primeng_autocomplete__WEBPACK_IMPORTED_MODULE_29__["AutoCompleteModule"],
            primeng_picklist__WEBPACK_IMPORTED_MODULE_32__["PickListModule"],
            primeng_listbox__WEBPACK_IMPORTED_MODULE_34__["ListboxModule"],
            primeng_orderlist__WEBPACK_IMPORTED_MODULE_35__["OrderListModule"],
            primeng_checkbox__WEBPACK_IMPORTED_MODULE_16__["CheckboxModule"],
            primeng_calendar__WEBPACK_IMPORTED_MODULE_36__["CalendarModule"],
            primeng_chip__WEBPACK_IMPORTED_MODULE_40__["ChipModule"],
            ngx_gauge__WEBPACK_IMPORTED_MODULE_39__["NgxGaugeModule"],
            ng_recaptcha__WEBPACK_IMPORTED_MODULE_64__["RecaptchaModule"],
            ng_recaptcha__WEBPACK_IMPORTED_MODULE_64__["RecaptchaFormsModule"],
            primeng_tooltip__WEBPACK_IMPORTED_MODULE_33__["TooltipModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_82__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
        _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_19__["MainmenulayoutComponent"],
        _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_20__["NomenulayoutComponent"],
        _pages_homeadmin_homeadmin_component__WEBPACK_IMPORTED_MODULE_21__["HomeadminComponent"],
        _pages_home_home_component__WEBPACK_IMPORTED_MODULE_22__["HomeComponent"],
        _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_43__["ApplicationgroupComponent"],
        _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_44__["UsersComponent"],
        _pages_root_applications_applications_component__WEBPACK_IMPORTED_MODULE_45__["ApplicationsComponent"],
        _pages_root_smtpaccounts_smtpaccounts_component__WEBPACK_IMPORTED_MODULE_46__["SmtpaccountsComponent"],
        _pages_root_oauthsettings_oauthsettings_component__WEBPACK_IMPORTED_MODULE_47__["OauthsettingsComponent"],
        _pages_root_eventlogs_eventlogs_component__WEBPACK_IMPORTED_MODULE_48__["EventlogsComponent"],
        _pages_root_resourceusage_resourceusage_component__WEBPACK_IMPORTED_MODULE_49__["ResourceusageComponent"],
        _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_50__["ErrorpageComponent"],
        _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_51__["BackmenulayoutComponent"],
        _pages_login_login_component__WEBPACK_IMPORTED_MODULE_52__["LoginComponent"],
        _pages_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_53__["ForgotpasswordComponent"],
        _layout_fullmenulayout_fullmenulayout_component__WEBPACK_IMPORTED_MODULE_56__["FullmenulayoutComponent"],
        _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_57__["TablehelperComponent"],
        _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_58__["ApplicationdetailComponent"],
        _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_59__["UserdetailComponent"],
        _pages_bpmp_bpmphome_bpmpintercept_component__WEBPACK_IMPORTED_MODULE_61__["BpmpinterceptComponent"],
        _pages_root_systemparam_systemparam_component__WEBPACK_IMPORTED_MODULE_62__["SystemparamComponent"],
        _pages_root_systemparam_systemparamdetail_systemparamdetail_component__WEBPACK_IMPORTED_MODULE_63__["SystemparamdetailComponent"],
        _pages_root_profile_profile_component__WEBPACK_IMPORTED_MODULE_65__["ProfileComponent"],
        _pages_root_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_66__["ChangePasswordComponent"],
        _pages_forgotpassword_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_67__["ResetpasswordComponent"],
        _pages_forgotpassword_verifikasiemail_verifikasiemail_component__WEBPACK_IMPORTED_MODULE_68__["VerifikasiemailComponent"],
        _pages_bpmp_bpmphome_bpmphome_component__WEBPACK_IMPORTED_MODULE_69__["BpmphomeComponent"],
        _pages_bpmp_company_company_component__WEBPACK_IMPORTED_MODULE_70__["CompanyComponent"],
        _pages_bpmp_company_companydetail_companydetail_component__WEBPACK_IMPORTED_MODULE_71__["CompanydetailComponent"],
        _pages_bpmp_channel_channel_component__WEBPACK_IMPORTED_MODULE_72__["ChannelComponent"],
        _pages_bpmp_channel_channeldetail_channeldetail_component__WEBPACK_IMPORTED_MODULE_73__["ChanneldetailComponent"],
        _pages_bpmp_companyreq_companyreq_component__WEBPACK_IMPORTED_MODULE_74__["CompanyreqComponent"],
        _pages_bpmp_companyreq_companyreqdetail_companyreqdetail_component__WEBPACK_IMPORTED_MODULE_75__["CompanyreqdetailComponent"],
        _pages_bpmp_channelreq_channelreq_component__WEBPACK_IMPORTED_MODULE_76__["ChannelreqComponent"],
        _pages_bpmp_rulesreq_rulesreq_component__WEBPACK_IMPORTED_MODULE_77__["RulesreqComponent"],
        _pages_bpmp_rulesreq_rulesreqdetail_rulesreqdetail_component__WEBPACK_IMPORTED_MODULE_78__["RulesreqdetailComponent"],
        _pages_bpmp_rules_rules_component__WEBPACK_IMPORTED_MODULE_79__["RulesComponent"],
        _pages_bpmp_rules_rulesdetail_rulesdetail_component__WEBPACK_IMPORTED_MODULE_80__["RulesdetailComponent"],
        _pages_bpmp_transactionmonitoring_transactionmonitoring_component__WEBPACK_IMPORTED_MODULE_81__["TransactionmonitoringComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_54__["HttpClientModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
        primeng_card__WEBPACK_IMPORTED_MODULE_7__["CardModule"],
        primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__["ToolbarModule"],
        primeng_button__WEBPACK_IMPORTED_MODULE_10__["ButtonModule"],
        primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__["InputTextModule"],
        primeng_splitbutton__WEBPACK_IMPORTED_MODULE_11__["SplitButtonModule"],
        primeng_menubar__WEBPACK_IMPORTED_MODULE_12__["MenubarModule"],
        primeng_panelmenu__WEBPACK_IMPORTED_MODULE_13__["PanelMenuModule"],
        primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_23__["BreadcrumbModule"],
        primeng_dropdown__WEBPACK_IMPORTED_MODULE_24__["DropdownModule"],
        primeng_chart__WEBPACK_IMPORTED_MODULE_30__["ChartModule"],
        primeng_blockui__WEBPACK_IMPORTED_MODULE_25__["BlockUIModule"],
        primeng_divider__WEBPACK_IMPORTED_MODULE_18__["DividerModule"],
        primeng_progressspinner__WEBPACK_IMPORTED_MODULE_41__["ProgressSpinnerModule"],
        primeng_toast__WEBPACK_IMPORTED_MODULE_42__["ToastModule"],
        primeng_messages__WEBPACK_IMPORTED_MODULE_37__["MessagesModule"],
        primeng_message__WEBPACK_IMPORTED_MODULE_38__["MessageModule"],
        primeng_dialog__WEBPACK_IMPORTED_MODULE_14__["DialogModule"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_17__["DynamicDialogModule"],
        primeng_panel__WEBPACK_IMPORTED_MODULE_15__["PanelModule"],
        primeng_table__WEBPACK_IMPORTED_MODULE_31__["TableModule"],
        primeng_selectbutton__WEBPACK_IMPORTED_MODULE_26__["SelectButtonModule"],
        primeng_steps__WEBPACK_IMPORTED_MODULE_27__["StepsModule"],
        primeng_inputnumber__WEBPACK_IMPORTED_MODULE_28__["InputNumberModule"],
        primeng_autocomplete__WEBPACK_IMPORTED_MODULE_29__["AutoCompleteModule"],
        primeng_picklist__WEBPACK_IMPORTED_MODULE_32__["PickListModule"],
        primeng_listbox__WEBPACK_IMPORTED_MODULE_34__["ListboxModule"],
        primeng_orderlist__WEBPACK_IMPORTED_MODULE_35__["OrderListModule"],
        primeng_checkbox__WEBPACK_IMPORTED_MODULE_16__["CheckboxModule"],
        primeng_calendar__WEBPACK_IMPORTED_MODULE_36__["CalendarModule"],
        primeng_chip__WEBPACK_IMPORTED_MODULE_40__["ChipModule"],
        ngx_gauge__WEBPACK_IMPORTED_MODULE_39__["NgxGaugeModule"],
        ng_recaptcha__WEBPACK_IMPORTED_MODULE_64__["RecaptchaModule"],
        ng_recaptcha__WEBPACK_IMPORTED_MODULE_64__["RecaptchaFormsModule"],
        primeng_tooltip__WEBPACK_IMPORTED_MODULE_33__["TooltipModule"]] }); })();


/***/ }),

/***/ "ZgGJ":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/bpmp/companyreq/companyreqdetail/companyreqdetail.component.ts ***!
  \**************************************************************************************/
/*! exports provided: CompanyreqdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyreqdetailComponent", function() { return CompanyreqdetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class CompanyreqdetailComponent {
    constructor() { }
    ngOnInit() { }
}
CompanyreqdetailComponent.ɵfac = function CompanyreqdetailComponent_Factory(t) { return new (t || CompanyreqdetailComponent)(); };
CompanyreqdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CompanyreqdetailComponent, selectors: [["app-companyreqdetail"]], decls: 2, vars: 0, template: function CompanyreqdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "companyreqdetail works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb21wYW55cmVxZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "brE3":
/*!*********************************************************************!*\
  !*** ./src/app/pages/root/oauthsettings/oauthsettings.component.ts ***!
  \*********************************************************************/
/*! exports provided: OauthsettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OauthsettingsComponent", function() { return OauthsettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class OauthsettingsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'LDAP Management' }
        ];
    }
}
OauthsettingsComponent.ɵfac = function OauthsettingsComponent_Factory(t) { return new (t || OauthsettingsComponent)(); };
OauthsettingsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: OauthsettingsComponent, selectors: [["app-oauthsettings"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function OauthsettingsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJvYXV0aHNldHRpbmdzLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "brkH":
/*!*****************************************************!*\
  !*** ./src/app/pages/root/users/users.component.ts ***!
  \*****************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/usermanager.service */ "4nGI");
/* harmony import */ var src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils/aclmenuchecker.service */ "42A2");
/* harmony import */ var src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/forgotpassword/forgotpassword.service */ "Jdzd");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/button */ "jIHw");















function UsersComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 14);
} }
function UsersComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function UsersComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.deleteConfirmation($event); })("datapreview", function UsersComponent_div_3_Template_app_tablehelper_datapreview_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.viewData($event); })("dataapprover", function UsersComponent_div_3_Template_app_tablehelper_dataapprover_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.approvalData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.userlist)("header", ctx_r1.usrheader)("wsearch", true)("actionbtn", ctx_r1.usractionbtn)("colnames", ctx_r1.usrcolname)("colwidth", ctx_r1.usrcolwidth)("colclasshalign", ctx_r1.usrcolhalign)("addbtnlink", ctx_r1.usraddbtn)("colmark", 2)("scrollheight", ctx_r1.scrollheight)("nopaging", false);
} }
function UsersComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.deleteUser(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function UsersComponent_ng_template_50_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_50_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.viewApprove = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_50_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r13.approvalSubmit(4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p-button", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_50_Template_p_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r14.approvalSubmit(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
const _c1 = function () { return { width: "50vw" }; };
class UsersComponent {
    constructor(authservice, dialogService, messageService, userService, aclMenuService, verifikasiService, router) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.userService = userService;
        this.aclMenuService = aclMenuService;
        this.verifikasiService = verifikasiService;
        this.router = router;
        this.display = false;
        this.scrollheight = "400px";
        this.viewDisplay = false;
        this.viewApprove = false;
        this.selectedUser = {};
        this.isGroup = false;
        this.userlist = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.usrheader = [
            { label: "Name", sort: "fullname" },
            { label: "User ID", sort: "userid" },
            { label: "Status", sort: "active" },
            { label: "Group Menu", sort: "groupname" },
            { label: "Attempt", sort: "total_attempt" },
            { label: "Created At", sort: "created_at" },
        ];
        this.usrcolname = [
            "fullname",
            "userid",
            "active",
            "groupname",
            "total_attempt",
            "created_at",
        ];
        this.usrcolhalign = [
            "",
            "",
            "p-text-center",
            "",
            "p-text-center",
            "p-text-center",
        ];
        // usrcolwidth: any = [
        //   '',
        //   '',
        //   { width: '110px' },
        //   { width: '170px' },
        //   { width: '110px' },
        //   { width: '170px' },
        // ];
        this.usrcolwidth = ["", "", "", "", { width: "170px" }, ""];
        // orgcollinghref:any = {'url':'#','label':'Application'}
        this.usractionbtn = [1, 1, 1, 1, 1, 1];
        this.usraddbtn = { route: "detail", label: "Add Data" };
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [{ label: "Users Management" }];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
                console.log("MENU ALL ACL Set");
                this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
                    if (JSON.stringify(dataacl.acl) === "{}") {
                        console.log("No ACL Founded");
                    }
                    else {
                        console.log("ACL Founded");
                        // console.log(dataacl.acl);
                        this.usractionbtn[0] = dataacl.acl.create;
                        this.usractionbtn[1] = dataacl.acl.read;
                        this.usractionbtn[2] = dataacl.acl.update;
                        this.usractionbtn[3] = dataacl.acl.delete;
                        this.usractionbtn[4] = dataacl.acl.view;
                        this.usractionbtn[5] = dataacl.acl.approval;
                    }
                });
            });
        });
        this.refreshingUser();
    }
    refreshingUser() {
        this.isFetching = true;
        this.authservice.whoAmi().subscribe((data) => {
            // console.log(">>>>>>> "+JSON.stringify(data));
            if ((data.status = 200)) {
                this.userService.retriveUsers().subscribe((orgall) => {
                    // console.log('>>>>>>> ' + JSON.stringify(orgall));
                    this.userlist = orgall.data;
                    if (this.userlist.length < 1) {
                        let objtmp = {
                            fullname: "No records",
                            userid: "No records",
                            active: "No records",
                            groupname: "No records",
                        };
                        this.userlist = [];
                        this.userlist.push(objtmp);
                    }
                    this.isFetching = false;
                });
            }
        });
    }
    deleteConfirmation(data) {
        // console.log('Di Emit nih dari child ' + JSON.stringify(data));
        this.display = true;
        this.selectedUser = data;
    }
    viewData(data) {
        // console.log(data);
        this.viewDisplay = true;
        this.selectedUser = data;
    }
    approvalData(data) {
        // console.log("MASUKK APPROVAL DATA", data);
        this.viewApprove = true;
        this.selectedUser = data;
    }
    approvalSubmit(status) {
        // console.log('id user::', this.selectedUser.id);
        // console.log(status);
        let payload = {
            id: this.selectedUser.id,
            oldactive: this.selectedUser.active,
            isactive: status,
            idapproval: this.selectedUser.idapproval,
        };
        this.userService
            .retriveUsersById(this.selectedUser.id)
            .subscribe((orgall) => {
            console.log("retriveusersbyid " + JSON.stringify(orgall));
            let statUserActive = orgall.data.active;
            let payload2 = {
                email: orgall.data.bioemailactive,
            };
            if (statUserActive === 4) {
                this.userService
                    .updatebyAdminActive(payload)
                    .subscribe((result) => {
                    // console.log(">>>>>>>> return "+JSON.stringify(result));]
                    if (result.status === 200) {
                        // console.log('kirim verifikasi email >>>');
                        this.verifikasiService
                            .verifikasi(payload2)
                            .subscribe((result) => {
                            this.refreshingUser();
                            this.viewApprove = false;
                        });
                    }
                });
            }
            else {
                this.userService
                    .updatebyAdminActive(payload)
                    .subscribe((result) => {
                    // console.log(">>>>>>>> return "+JSON.stringify(result));
                    if (result.status === 200) {
                        this.refreshingUser();
                        this.viewApprove = false;
                    }
                });
            }
        });
        // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    }
    deleteUser() {
        // console.log(this.selectedUser);
        let user = this.selectedUser;
        const payload = { user };
        this.userService
            .deleteUserByAdmin(payload)
            .subscribe((resp) => {
            // console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingUser();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
UsersComponent.ɵfac = function UsersComponent_Factory(t) { return new (t || UsersComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__["UsermanagerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__["AclmenucheckerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_6__["ForgotpasswordService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"])); };
UsersComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: UsersComponent, selectors: [["app-users"]], decls: 51, vars: 30, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete User", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "View User", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", "p-md-3"], [1, "p-col-12", "p-md-1"], [1, "p-col-12", "p-md-8"], ["header", "Approval", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-col-12", 2, "font-size", "18px"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "colclasshalign", "addbtnlink", "colmark", "scrollheight", "nopaging", "datadeleted", "datapreview", "dataapprover"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"], ["label", "Cancel", "styleClass", "p-button-text", 3, "click"], ["label", "Reject", "styleClass", "p-button-text", 3, "click"], ["label", "Approve", "styleClass", "p-button-text", 3, "click"]], template: function UsersComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, UsersComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, UsersComponent_div_3_Template, 2, 11, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function UsersComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Users?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, UsersComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p-dialog", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function UsersComponent_Template_p_dialog_visibleChange_8_listener($event) { return ctx.viewDisplay = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Fullname");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Created date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Group");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Login Attempt");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p-dialog", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function UsersComponent_Template_p_dialog_visibleChange_45_listener($event) { return ctx.viewApprove = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "This records need approval from you?, please choose wisely");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](50, UsersComponent_ng_template_50_Template, 3, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userlist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userlist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](27, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](28, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewDisplay)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedUser.fullname);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedUser.bioemailactive);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedUser.created_at);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedUser.groupname);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedUser.total_attempt);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](29, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewApprove)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_10__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_11__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_12__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_13__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_14__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2Vycy5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "bzlq":
/*!********************************************************!*\
  !*** ./src/app/pages/errorpage/errorpage.component.ts ***!
  \********************************************************/
/*! exports provided: ErrorpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorpageComponent", function() { return ErrorpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class ErrorpageComponent {
    constructor() { }
    ngOnInit() {
    }
}
ErrorpageComponent.ɵfac = function ErrorpageComponent_Factory(t) { return new (t || ErrorpageComponent)(); };
ErrorpageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ErrorpageComponent, selectors: [["app-errorpage"]], decls: 6, vars: 0, consts: [["src", "assets/logos/404.png", "height", "320"], [2, "font-size", "24px"]], template: function ErrorpageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Oops, something wrong!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " please contact your administrator.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlcnJvcnBhZ2UuY29tcG9uZW50LnNjc3MifQ== */", "body[_ngcontent-%COMP%] {\n        background: #dee2e6\n    }\n\n    section[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%);\n        text-align: center;\n    }"] });


/***/ }),

/***/ "ci6g":
/*!*********************************************************!*\
  !*** ./src/app/services/servbpmp/channelreq.service.ts ***!
  \*********************************************************/
/*! exports provided: ChannelreqService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChannelreqService", function() { return ChannelreqService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class ChannelreqService {
    constructor(service) {
        this.service = service;
    }
    getAllChannel() {
        const url = "api/channel/getChannelTemp";
        return this.service.baseGet(url);
    }
    getChannel(id) {
        const url = "api/channel/getChannelTemp/" + id;
        return this.service.baseGet(url);
    }
    approveChannel(payload) {
        const url = "api/channel/approveChannelTemp";
        return this.service.basePost(url, payload);
    }
    rejectChannel(payload) {
        const url = "api/channel/rejectChannelTemp";
        return this.service.basePost(url, payload);
    }
}
ChannelreqService.ɵfac = function ChannelreqService_Factory(t) { return new (t || ChannelreqService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
ChannelreqService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ChannelreqService, factory: ChannelreqService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "cygB":
/*!*********************************************!*\
  !*** ./src/app/services/backend.service.ts ***!
  \*********************************************/
/*! exports provided: BackendService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackendService", function() { return BackendService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _env_env_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../env/env.service */ "usjD");

// import { environment } from '@environtment';




class BackendService {
    constructor(httpClient, environment) {
        this.httpClient = httpClient;
        this.environment = environment;
    }
    post(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient
            .post(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    get(path, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    put(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient.put(url, payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    patch(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient
            .patch(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    delete(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient
            .delete(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    basePost(path, payload, authorized) {
        const url = this.environment.baseUrl + path;
        return this.httpClient
            .post(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    baseGet(path, authorized) {
        const url = this.environment.baseUrl + path;
        return this.httpClient.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    basePut(path, payload, authorized) {
        const url = this.environment.baseUrl + path;
        return this.httpClient.put(url, payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    basePatch(path, payload, authorized) {
        const url = this.environment.baseUrl + path;
        return this.httpClient
            .patch(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    baseDelete(path, payload, authorized) {
        const url = this.environment.baseUrl + path;
        return this.httpClient
            .delete(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    // tslint:disable-next-line:typedef
    handleError(error) {
        console.log('error occured ', error);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["throwError"])(error);
    }
}
BackendService.ɵfac = function BackendService_Factory(t) { return new (t || BackendService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_env_env_service__WEBPACK_IMPORTED_MODULE_4__["EnvService"])); };
BackendService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: BackendService, factory: BackendService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "dI1x":
/*!*******************************************************!*\
  !*** ./src/app/services/servbpmp/sysparam.service.ts ***!
  \*******************************************************/
/*! exports provided: SysparamService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SysparamService", function() { return SysparamService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class SysparamService {
    constructor(service) {
        this.service = service;
    }
    retriveProfile(id) {
        const url = "adm/profile/" + id;
        return this.service.get(url);
    }
    getAllSysParam() {
        const url = "adm/sysparam/getall";
        return this.service.get(url);
    }
    getSysParamById(id) {
        const url = "adm/sysparam/getById/" + id;
        return this.service.get(url);
    }
    updateSysParam(payload) {
        const url = "adm/sysparam/update";
        return this.service.post(url, payload);
    }
    updateSysParamActive(payload) {
        const url = "adm/sysparam/updatebyadminactive";
        return this.service.post(url, payload);
    }
}
SysparamService.ɵfac = function SysparamService_Factory(t) { return new (t || SysparamService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
SysparamService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SysparamService, factory: SysparamService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "dJaT":
/*!***************************************************************!*\
  !*** ./src/app/pages/bpmp/companyreq/companyreq.component.ts ***!
  \***************************************************************/
/*! exports provided: CompanyreqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyreqComponent", function() { return CompanyreqComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/root/organization.service */ "u0Pv");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "jIHw");













function CompanyreqComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 6);
} }
function CompanyreqComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function CompanyreqComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.companyreqlist)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("collinkaction", ctx_r1.orgcollinghref)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn);
} }
function CompanyreqComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CompanyreqComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CompanyreqComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.deleteOrganization(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
class CompanyreqComponent {
    constructor(authservice, dialogService, messageService, backend, organizationService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.backend = backend;
        this.organizationService = organizationService;
        this.display = false;
        this.selectedOrganization = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [
            { label: "Company ID", sort: "companyid" },
            { label: "Company Code", sort: "companycode" },
            { label: "CIF Code", sort: "companyname" },
            { label: "Company Name", sort: "hostcode" },
            { label: "Company Short Name", sort: "createdwho" },
        ];
        this.orgcolname = [
            "COMPANY_ID",
            "COMPANY_CODE",
            "CIF_CODE",
            "COMPANY_NAME",
            "COMPANY_SHORTNAME",
        ];
        this.orgcolhalign = [
            "p-text-center",
            "",
            "",
            "p-text-center",
            "p-text-center",
        ];
        this.orgcolwidth = [{ width: "110px" }, "", "", { width: "120px" }, ""];
        this.orgactionbtn = [1, 1, 1, 1, 1];
        this.orgaddbtn = { route: "detail", label: "Add Data" };
        this.companyreqlist = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Pending Company" }];
        // this.authservice.whoAmi().subscribe((value) =>{
        //   // console.log(">>> User Info : "+JSON.stringify(value));
        //     this.userInfo = value.data;
        //     this.tokenID = value.tokenId;
        // });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.backend
            .basePost(
        // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
        "api/companyreq/getCompanyTemp", {}, false)
            .subscribe((data) => {
            //console.log("Company length>>>>>>> " + JSON.stringify(data));
            // if ((data.status = 200)) {
            //   this.organizationService
            //     .retriveOrgByTenant()
            //     .subscribe((orgall: BackendResponse) => {
            //console.log('>>>>>>> ' + data.data.data.length);
            this.companyreqlist = data.data.data;
            if (this.companyreqlist.length < 1) {
                let objtmp = {
                    orgcode: "No records",
                    orgname: "No records",
                    orgdescription: "No records",
                    application: "No records",
                    created_by: "No records",
                };
                this.companyreqlist = [];
                this.companyreqlist.push(objtmp);
            }
            this.isFetching = false;
            //     });
            // }
        });
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedOrganization = data;
    }
    deleteOrganization() {
        console.log(this.selectedOrganization);
        let organization = this.selectedOrganization;
        const payload = { organization };
        this.organizationService
            .deleteOrg(payload)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
CompanyreqComponent.ɵfac = function CompanyreqComponent_Factory(t) { return new (t || CompanyreqComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_4__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__["OrganizationService"])); };
CompanyreqComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CompanyreqComponent, selectors: [["app-companyreq"]], decls: 8, vars: 11, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete Organization", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "collinkaction", "colclasshalign", "addbtnlink", "datadeleted"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function CompanyreqComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CompanyreqComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, CompanyreqComponent_div_3_Template, 2, 9, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function CompanyreqComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Organization?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, CompanyreqComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.companyreqlist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.companyreqlist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_6__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_8__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_9__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_11__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_12__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb21wYW55cmVxLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "e3kg":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/root/systemparam/systemparamdetail/systemparamdetail.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: SystemparamdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemparamdetailComponent", function() { return SystemparamdetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_services_servbpmp_sysparam_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/servbpmp/sysparam.service */ "dI1x");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/button */ "jIHw");












function SystemparamdetailComponent_form_2_div_10_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Parameters is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function SystemparamdetailComponent_form_2_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SystemparamdetailComponent_form_2_div_10_span_1_Template, 2, 0, "span", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r1.f.parameter.errors.required);
} }
function SystemparamdetailComponent_form_2_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Value is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function SystemparamdetailComponent_form_2_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, SystemparamdetailComponent_form_2_div_16_span_1_Template, 2, 0, "span", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r2.f.value.errors.required);
} }
function SystemparamdetailComponent_form_2_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "form", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngSubmit", function SystemparamdetailComponent_form_2_Template_form_ngSubmit_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r5.onSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "p-card", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "label", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, "Parameters :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "input", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, SystemparamdetailComponent_form_2_div_10_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](11, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "label", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](14, "Value * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](15, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](16, SystemparamdetailComponent_form_2_div_16_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "label", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](20, "Description :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](21, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](22, "textarea", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](23, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](25, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](26, "button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("formGroup", ctx_r0.groupForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.parameter.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.value.errors);
} }
class SystemparamdetailComponent {
    constructor(router, activatedRoute, formBuilder, authservice, location, sysService) {
        var _a;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.authservice = authservice;
        this.location = location;
        this.sysService = sysService;
        this.extraInfo = {};
        this.isEdit = false;
        this.userId = null;
        this.stateOptions = [];
        this.leveltenant = 0;
        this.userInfo = {};
        this.selectedApps = [];
        this.tokenID = "";
        this.submitted = false;
        this.orgsData = [];
        this.appInfoActive = {};
        this.orgSuggest = {};
        this.user = {};
        this.formatedOrg = [];
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
        console.log(">>>>>>>>>>> " + this.extraInfo);
        console.log(checkurl);
        if (checkurl)
            this.isEdit = true;
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [
            {
                label: "System Param",
                command: (event) => {
                    this.location.back();
                },
                url: "",
            },
            { label: this.isEdit ? "Edit data" : "Add data" },
        ];
        this.stateOptions = [
            { label: "Direct activated", value: 1 },
            { label: "Activision link", value: 0 },
        ];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.leveltenant = this.userInfo.leveltenant;
            this.groupForm = this.formBuilder.group({
                parameter: [{ value: "", disabled: true }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                value: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                description: [[], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            });
            if (this.isEdit) {
                if (this.activatedRoute.snapshot.paramMap.get("id")) {
                    this.userId = this.activatedRoute.snapshot.paramMap.get("id");
                    this.sysService
                        .getSysParamById(this.userId)
                        .subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        console.log("Data edit user " + JSON.stringify(result.data));
                        this.user.parameter = result.data.param_name;
                        this.user.value = result.data.param_value;
                        this.user.description = result.data.description;
                        this.groupForm.patchValue({
                            parameter: this.user.parameter,
                            value: this.user.value,
                            description: this.user.description,
                        });
                        console.log(this.user);
                    }));
                }
            }
        });
    }
    get f() {
        return this.groupForm.controls;
    }
    formatOrgData(data) {
        data.map((dt) => {
            let formated = {};
            formated.name = dt.groupname;
            formated.id = dt.id;
            this.formatedOrg.push(formated);
        });
    }
    filterOrg(event) {
        let filtered = [];
        let query = event.query;
        for (let i = 0; i < this.formatedOrg.length; i++) {
            let country = this.formatedOrg[i];
            if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
                filtered.push(country);
            }
        }
        this.orgSuggest = filtered;
    }
    onSubmit() {
        var _a, _b, _c;
        this.submitted = true;
        console.log(">>>>>>>>IS NULL >>>> " + this.leveltenant);
        // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
        if (this.groupForm.valid) {
            // event?.preventDefault;
            var groupacl = (_a = this.groupForm.get("orgobj")) === null || _a === void 0 ? void 0 : _a.value;
            let payload = {};
            payload = {
                id: this.userId,
                value: (_b = this.groupForm.get("value")) === null || _b === void 0 ? void 0 : _b.value,
                description: (_c = this.groupForm.get("description")) === null || _c === void 0 ? void 0 : _c.value,
            };
            console.log(">>>>>>>> payload " + JSON.stringify(payload));
            this.sysService
                .updateSysParam(payload)
                .subscribe((result) => {
                // console.log(">>>>>>>> return "+JSON.stringify(result));
                if (result.status === 200) {
                    this.location.back();
                }
            });
        }
        console.log(this.groupForm.valid);
    }
}
SystemparamdetailComponent.ɵfac = function SystemparamdetailComponent_Factory(t) { return new (t || SystemparamdetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_servbpmp_sysparam_service__WEBPACK_IMPORTED_MODULE_6__["SysparamService"])); };
SystemparamdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: SystemparamdetailComponent, selectors: [["app-systemparamdetail"]], decls: 3, vars: 3, consts: [[3, "model", "home"], [1, "wrapperinside"], ["style", "padding: 2px;", 3, "formGroup", "ngSubmit", 4, "ngIf"], [2, "padding", "2px", 3, "formGroup", "ngSubmit"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "parameter", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "parameter", "formControlName", "parameter", "id", "parameter", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "p-field", 4, "ngIf"], [2, "height", "15px"], ["for", "value", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "value", "formControlName", "value", "id", "value", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "description", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], [2, "height", "8px"], ["id", "description", "rows", "7", "cols", "109", "formControlName", "description", "pInputTextarea", "", 1, "p-inputtext"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success"], [1, "p-field"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"]], template: function SystemparamdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, SystemparamdetailComponent_form_2_Template, 27, 3, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("model", ctx.breadcrumbs)("home", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.groupForm);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_8__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], primeng_button__WEBPACK_IMPORTED_MODULE_10__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzeXN0ZW1wYXJhbWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "eUMV":
/*!***************************************************************!*\
  !*** ./src/app/pages/bpmp/channelreq/channelreq.component.ts ***!
  \***************************************************************/
/*! exports provided: ChannelreqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChannelreqComponent", function() { return ChannelreqComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_servbpmp_channelreq_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/servbpmp/channelreq.service */ "ci6g");
/* harmony import */ var src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils/aclmenuchecker.service */ "42A2");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");














function ChannelreqComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 12);
} }
function ChannelreqComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datapreview", function ChannelreqComponent_div_3_Template_app_tablehelper_datapreview_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.viewData($event); })("dataapprover", function ChannelreqComponent_div_3_Template_app_tablehelper_dataapprover_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.approvalData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.channelreqlist)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn)("scrollheight", ctx_r1.scrollheight)("nopaging", false);
} }
function ChannelreqComponent_ng_template_9_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ChannelreqComponent_ng_template_9_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.viewApprove = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ChannelreqComponent_ng_template_9_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.approvalSubmit(4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p-button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ChannelreqComponent_ng_template_9_Template_p_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.approvalSubmit(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "50vw" }; };
class ChannelreqComponent {
    constructor(authservice, dialogService, messageService, channelreqService, aclMenuService, router) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.channelreqService = channelreqService;
        this.aclMenuService = aclMenuService;
        this.router = router;
        this.viewApprove = false;
        this.viewDisplay = false;
        this.display = false;
        this.selectedChannelReq = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [
            { label: "Channel ID", sort: "id" },
            { label: "Merchant Type", sort: "channelId" },
            { label: "Channel Name", sort: "channelName" },
            { label: "Created Who", sort: "createdWho" },
            { label: "Created Date", sort: "createdDate" },
            { label: "Change Who", sort: "changedWho" },
            { label: "Channel Code", sort: "channelCode" },
            { label: "Change Date", sort: "changedDate" },
        ];
        this.orgcolname = [
            "CHANNEL_ID",
            "MERCHANT_TYPE",
            "CHANNEL_NAME",
            "CREATE_WHO",
            "CREATE_DATE",
            "CHANGE_WHO",
            "CHANNEL_CODE",
            "CHANGE_DATE",
        ];
        this.orgcolhalign = [
            "p-text-center",
            "",
            "",
            "p-text-center",
            "p-text-center",
            "",
        ];
        this.orgcolwidth = [{ width: "110px" }, "", "", { width: "120px" }, ""];
        this.orgactionbtn = [0, 1, 0, 0, 0, 1, 1]; //[1, 1, 1, 1, 1];
        this.orgaddbtn = { route: "detail", label: "Add Data" };
        this.channelreqlist = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/" };
        this.breadcrumbs = [{ label: "Pending Company" }];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
                console.log("MENU ALL ACL Set");
                this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
                    if (JSON.stringify(dataacl.acl) === "{}") {
                        console.log("No ACL Founded");
                    }
                    else {
                        console.log("ACL Founded");
                        console.log(dataacl.acl.apprioval);
                        this.orgactionbtn[0] = dataacl.acl.create;
                        this.orgactionbtn[1] = dataacl.acl.read;
                        this.orgactionbtn[2] = dataacl.acl.update;
                        this.orgactionbtn[3] = dataacl.acl.delete;
                        this.orgactionbtn[4] = dataacl.acl.view;
                        this.orgactionbtn[5] = dataacl.acl.approval;
                    }
                });
            });
        });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.channelreqService
            .getAllChannel()
            .subscribe((result) => {
            if (result.status === 202) {
                this.channelreqlist = [];
                let objtmp = {
                    CHANNEL_ID: "No records",
                    CHANNEL_NAME: "No records",
                    CHANNEL_CODE: "No records",
                    MERCHANT_TYPE: "No records",
                    CREATE_WHO: "No records",
                    CREATE_DATE: "No records",
                    CHANGE_WHO: "No records",
                    CHANGE_DATE: "No records",
                };
                this.channelreqlist.push(objtmp);
            }
            else {
                this.channelreqlist = result.data;
            }
            this.isFetching = false;
        });
    }
    viewData(data) {
        console.log(data.idapproval);
        this.channelreqService
            .getChannel(data.idapproval)
            .subscribe((result) => {
            if (result.status === 200) {
                console.log(result);
                this.viewDisplay = true;
                this.selectedChannelReq = result.data;
            }
        });
        // this.viewDisplay = true;
        // this.selectedChannelReq = data;
    }
    approvalData(data) {
        // console.log(data);
        this.viewApprove = true;
        this.selectedChannelReq = data;
    }
    approvalSubmit(status) {
        let payload = {
            id: this.selectedChannelReq.id,
            oldactive: this.selectedChannelReq.active,
            isactive: status,
            idapproval: this.selectedChannelReq.idapproval,
        };
        // console.log(">>>>>>>> payload " + JSON.stringify(payload));
        if (status == 4) {
            this.channelreqService
                .rejectChannel(payload)
                .subscribe((result) => {
                if (result.status === 200) {
                    this.refreshingApp();
                    this.viewApprove = false;
                }
            });
        }
        else {
            this.channelreqService
                .approveChannel(payload)
                .subscribe((result) => {
                if (result.status === 200) {
                    this.refreshingApp();
                    this.viewApprove = false;
                }
            });
        }
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
ChannelreqComponent.ɵfac = function ChannelreqComponent_Factory(t) { return new (t || ChannelreqComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_servbpmp_channelreq_service__WEBPACK_IMPORTED_MODULE_4__["ChannelreqService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__["AclmenucheckerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"])); };
ChannelreqComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ChannelreqComponent, selectors: [["app-channelreq"]], decls: 68, vars: 26, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Approval", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", 2, "font-size", "18px"], ["pTemplate", "footer"], ["header", "View Channel Approval", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-col-12", "p-md-3"], [1, "p-col-12", "p-md-1"], [1, "p-col-12", "p-md-8"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "colclasshalign", "addbtnlink", "scrollheight", "nopaging", "datapreview", "dataapprover"], ["label", "Cancel", "styleClass", "p-button-text", 3, "click"], ["label", "Reject", "styleClass", "p-button-text", 3, "click"], ["label", "Approve", "styleClass", "p-button-text", 3, "click"]], template: function ChannelreqComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ChannelreqComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ChannelreqComponent_div_3_Template, 2, 10, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ChannelreqComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.viewApprove = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "This records need approval from you?, please choose wisely");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, ChannelreqComponent_ng_template_9_Template, 3, 0, "ng-template", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p-dialog", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ChannelreqComponent_Template_p_dialog_visibleChange_10_listener($event) { return ctx.viewDisplay = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Channel ID");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Merchant Type");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Channel Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Create Who");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Created Date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Change Who");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Change Date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Channel Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "p", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channelreqlist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channelreqlist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](24, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewApprove)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](25, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewDisplay)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CHANNEL_ID);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.MERCHANT_TYPE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CHANNEL_NAME);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CREATE_WHO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CREATE_DATE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CHANGE_WHO);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CHANGE_DATE);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.selectedChannelReq.CHANNEL_CODE);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_9__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_10__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGFubmVscmVxLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "fNSO":
/*!**************************************!*\
  !*** ./src/app/guard/guard.guard.ts ***!
  \**************************************/
/*! exports provided: GuardGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardGuard", function() { return GuardGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "lGQG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");





class GuardGuard {
    constructor(authService, router, sessionStorage) {
        this.authService = authService;
        this.router = router;
        this.sessionStorage = sessionStorage;
    }
    canActivate(route, state) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // console.log(route.params.config);
            let config = route.params.config;
            if (this.authService.isLoggedIn()) {
                //console.log(' sudah logged in');
                return yield this.checkPage(this.authService, route, state, this.router);
            }
            else {
                if (route.params.config) {
                    this.authService
                        .whoAmiInitialize(route.params.config)
                        .subscribe((value) => {
                        console.log('>>> Token session : ' + JSON.stringify(value));
                        if ((value.status = 200)) {
                            this.sessionStorage.set('accesstoken', value.data);
                            this.authService.setAuthStatus(true);
                            this.authService.setToken(value.data);
                            this.router.navigate(['mgm/home']);
                        }
                        else {
                            this.router.navigate(['auth/login']);
                        }
                    });
                }
                else {
                    this.router.navigate(['/auth/login']);
                    return false;
                }
            }
            // return false;
        });
    }
    checkPage(authService, routeCp, stateCp, router) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const promise = new Promise(function (resolve, reject) {
                // setTimeout(function() {
                authService.whoAmi().subscribe((value) => {
                    resolve(value);
                    // console.log(JSON.stringify(value.data));
                    // let lvlTenant = parseInt(value.data.leveltenant);
                    // if (lvlTenant > 0) {
                    //   let anypage = false;
                    //   value.data.sidemenus.forEach((module: any) => {
                    //     module.items.forEach((menus: any) => {
                    //       if (menus.routerLink) {
                    //         let routerLink = menus.routerLink;
                    //         let mainModule = routerLink.split('/');
                    //         let module = mainModule[2];
                    //         let currentPath = stateCp.url;
                    //         if (
                    //           currentPath.includes(module) ||
                    //           currentPath.includes('profile')
                    //         )
                    //           anypage = true;
                    //       }
                    //     });
                    //   });
                    //   if (stateCp.url === '/home') anypage = true;
                    //   //if (stateCp.url === '/vam') anypage = true;
                    //   console.log('Ada Halaman ' + stateCp.url);
                    //   if (anypage) {
                    //     resolve('Promise returns after 1.5 second!');
                    //   } else {
                    //     router.navigate(['/noauth/err']);
                    //     reject(anypage);
                    //   }
                    // } else {
                    //   resolve('Promise returns after 1.5 second!');
                    // }
                });
            });
            return promise.then(function (value) {
                // console.log(value);
                if (value.exp == true) {
                    router.navigate(['/auth/login']);
                    return false;
                }
                return true;
                // Promise returns after 1.5 second!
            });
        });
    }
}
GuardGuard.ɵfac = function GuardGuard_Factory(t) { return new (t || GuardGuard)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_4__["SessionStorageService"])); };
GuardGuard.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: GuardGuard, factory: GuardGuard.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "fRat":
/*!********************************************************************************!*\
  !*** ./src/app/pages/bpmp/rulesreq/rulesreqdetail/rulesreqdetail.component.ts ***!
  \********************************************************************************/
/*! exports provided: RulesreqdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RulesreqdetailComponent", function() { return RulesreqdetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class RulesreqdetailComponent {
    constructor() { }
    ngOnInit() {
    }
}
RulesreqdetailComponent.ɵfac = function RulesreqdetailComponent_Factory(t) { return new (t || RulesreqdetailComponent)(); };
RulesreqdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: RulesreqdetailComponent, selectors: [["app-rulesreqdetail"]], decls: 2, vars: 0, template: function RulesreqdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "rulesreqdetail works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJydWxlc3JlcWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "hwT4":
/*!*******************************************************************!*\
  !*** ./src/app/pages/root/applications/applications.component.ts ***!
  \*******************************************************************/
/*! exports provided: ApplicationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsComponent", function() { return ApplicationsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class ApplicationsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Applications Management' }
        ];
    }
}
ApplicationsComponent.ɵfac = function ApplicationsComponent_Factory(t) { return new (t || ApplicationsComponent)(); };
ApplicationsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ApplicationsComponent, selectors: [["app-applications"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function ApplicationsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBsaWNhdGlvbnMuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "j149":
/*!**************************************************************!*\
  !*** ./src/app/generic/tablehelper/tablehelper.component.ts ***!
  \**************************************************************/
/*! exports provided: TablehelperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablehelperComponent", function() { return TablehelperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_tooltip__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/tooltip */ "xlun");









const _c0 = ["dt"];
function TablehelperComponent_p_table_0_ng_template_2_button_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 13);
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("label", ctx_r6.addbtnlink.label);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", ctx_r6.addbtnlink.route);
} }
function TablehelperComponent_p_table_0_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_0_ng_template_2_button_1_Template, 1, 2, "button", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 11, 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function TablehelperComponent_p_table_0_ng_template_2_Template_input_input_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5); _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1); return _r2.filterGlobal(_r7.value, "contains"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.actionbtn[0] == 1);
} }
function TablehelperComponent_p_table_0_ng_template_3_th_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-sortIcon", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const head_r12 = ctx.$implicit;
    const i_r13 = ctx.index;
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](ctx_r10.colwidth[i_r13]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("pSortableColumn", head_r12.sort);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", head_r12.label, " ");
} }
const _c1 = function () { return { "width": "200px" }; };
function TablehelperComponent_p_table_0_ng_template_3_th_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Action");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c1));
} }
function TablehelperComponent_p_table_0_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_0_ng_template_3_th_1_Template, 3, 4, "th", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_0_ng_template_3_th_2_Template, 2, 3, "th", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r4.header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r4.isActionBtn);
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0);
} if (rf & 2) {
    const data_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", record_r14[data_r17], " ");
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r20.collinkaction.label);
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_3_i_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 26);
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_3_i_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 27);
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_3_i_0_Template, 1, 0, "i", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_3_i_1_Template, 1, 0, "i", 25);
} if (rf & 2) {
    const data_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r14[data_r17] === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r14[data_r17] === 0);
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](1, "date");
} if (rf & 2) {
    const data_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind3"](1, 1, record_r14[data_r17], "yyyy-MM-dd HH:mm:ss", "UTC"), " ");
} }
function TablehelperComponent_p_table_0_ng_template_4_td_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_1_Template, 1, 1, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_2_Template, 2, 1, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_3_Template, 2, 2, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_p_table_0_ng_template_4_td_1_ng_template_4_Template, 2, 5, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const j_r18 = ctx.index;
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r15.colclasshalign[j_r18]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r18 != ctx_r15.collink && j_r18 != ctx_r15.colmark && j_r18 != ctx_r15.colldate);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r18 == ctx_r15.collink);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r18 == ctx_r15.colmark);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r18 == ctx_r15.colldate);
} }
function TablehelperComponent_p_table_0_ng_template_4_td_2_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_0_ng_template_4_td_2_button_1_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r36); const record_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r34.previewConfirmation(record_r14); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c2 = function (a0) { return [a0]; };
function TablehelperComponent_p_table_0_ng_template_4_td_2_button_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 32);
} if (rf & 2) {
    const record_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;
    const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c2, ctx_r32.addbtnlink.route + "#/" + record_r14.id));
} }
function TablehelperComponent_p_table_0_ng_template_4_td_2_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_0_ng_template_4_td_2_button_3_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40); const record_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r38.deleteConfirmation(record_r14); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_0_ng_template_4_td_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_0_ng_template_4_td_2_button_1_Template, 1, 0, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_0_ng_template_4_td_2_button_2_Template, 1, 3, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_0_ng_template_4_td_2_button_3_Template, 1, 0, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r16.actionbtn[4] == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r16.actionbtn[2] == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r16.actionbtn[3] == 1);
} }
function TablehelperComponent_p_table_0_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_0_ng_template_4_td_1_Template, 5, 6, "td", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_0_ng_template_4_td_2_Template, 4, 3, "td", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r5.colnames);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r5.isActionBtn);
} }
const _c3 = function () { return [5, 10, 25, 50]; };
function TablehelperComponent_p_table_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-table", 2, 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_0_ng_template_2_Template, 6, 1, "ng-template", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_0_ng_template_3_Template, 3, 2, "ng-template", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_p_table_0_ng_template_4_Template, 3, 2, "ng-template", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx_r0.records)("rows", 5)("showCurrentPageReport", true)("rowsPerPageOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c3))("paginator", true)("globalFilterFields", ctx_r0.colnames);
} }
function TablehelperComponent_p_table_1_ng_template_2_button_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 37);
} if (rf & 2) {
    const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("label", ctx_r45.addbtnlink.label);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", ctx_r45.addbtnlink.route);
} }
function TablehelperComponent_p_table_1_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    const _r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_ng_template_2_button_1_Template, 1, 2, "button", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 11, 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function TablehelperComponent_p_table_1_ng_template_2_Template_input_input_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r48); const _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5); _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); const _r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1); return _r41.filterGlobal(_r46.value, "contains"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r42.actionbtn[0] == 1);
} }
function TablehelperComponent_p_table_1_ng_template_3_th_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-sortIcon", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const head_r51 = ctx.$implicit;
    const i_r52 = ctx.index;
    const ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](ctx_r49.colwidth[i_r52]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("pSortableColumn", head_r51.sort);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", head_r51.label, " ");
} }
function TablehelperComponent_p_table_1_ng_template_3_th_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Action");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c1));
} }
function TablehelperComponent_p_table_1_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_ng_template_3_th_1_Template, 3, 4, "th", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_1_ng_template_3_th_2_Template, 2, 3, "th", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r43.header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r43.isActionBtn);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0);
} if (rf & 2) {
    const data_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", record_r54[data_r57], " ");
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r60.collinkaction.label);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 44);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 45);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 46);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 47);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 48);
} }
const _c4 = function () { return [3, 4]; };
const _c5 = function () { return [5, 7]; };
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_0_Template, 1, 0, "i", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_1_Template, 1, 0, "i", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_2_Template, 1, 0, "i", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_3_Template, 1, 0, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_i_4_Template, 1, 0, "i", 43);
} if (rf & 2) {
    const data_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r54[data_r57] === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r54[data_r57] === 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c4).indexOf(record_r54[data_r57]) > -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c5).indexOf(record_r54[data_r57]) > -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r54[data_r57] === 9);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_1_Template, 1, 1, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_2_Template, 2, 1, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_1_4_ng_template_0_td_1_ng_template_3_Template, 5, 7, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const j_r58 = ctx.index;
    const ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](ctx_r55.colwidth[j_r58]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r55.colclasshalign[j_r58]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r58 != ctx_r55.collink && j_r58 != ctx_r55.colmark);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r58 == ctx_r55.collink);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r58 == ctx_r55.colmark);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r82 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_1_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r82); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r80 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r80.previewConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 56);
} if (rf & 2) {
    const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;
    const ctx_r72 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c2, ctx_r72.addbtnlink.route + "#/" + record_r54.id));
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r86 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_3_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r86); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r84 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r84.deleteConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r89 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_4_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r89); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r87 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r87.previewConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_5_Template(rf, ctx) { if (rf & 1) {
    const _r92 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_5_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r92); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r90 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r90.approveConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_6_Template(rf, ctx) { if (rf & 1) {
    const _r95 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_6_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r95); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r93 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r93.approveConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_7_Template(rf, ctx) { if (rf & 1) {
    const _r98 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_7_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r98); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r96 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r96.approveConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_8_Template(rf, ctx) { if (rf & 1) {
    const _r101 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_8_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r101); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r99 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r99.approveConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r104 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_9_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r104); const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit; const ctx_r102 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r102.approveConfirmation(record_r54); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c6 = function () { return [3, 4, 5, 7, 9]; };
function TablehelperComponent_p_table_1_4_ng_template_0_td_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_1_Template, 1, 0, "button", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_2_Template, 1, 3, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_3_Template, 1, 0, "button", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_4_Template, 1, 0, "button", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_5_Template, 1, 0, "button", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_6_Template, 1, 0, "button", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_7_Template, 1, 0, "button", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_8_Template, 1, 0, "button", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TablehelperComponent_p_table_1_4_ng_template_0_td_2_button_9_Template, 1, 0, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const record_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[4] == 1 && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c6).indexOf(record_r54["active"]) == -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[2] == 1 && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c6).indexOf(record_r54["active"]) == -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[3] == 1 && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c6).indexOf(record_r54["active"]) == -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[6] == 1 && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](15, _c5).indexOf(record_r54["active"]));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[5] == 1 && record_r54["active"] === 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[5] == 1 && record_r54["active"] === 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[5] == 1 && record_r54["active"] === 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[5] == 1 && record_r54["active"] === 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r56.actionbtn[5] == 1 && record_r54["active"] === 9);
} }
function TablehelperComponent_p_table_1_4_ng_template_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_4_ng_template_0_td_1_Template, 4, 7, "td", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_1_4_ng_template_0_td_2_Template, 10, 16, "td", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r53.colnames);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.isActionBtn);
} }
function TablehelperComponent_p_table_1_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TablehelperComponent_p_table_1_4_ng_template_0_Template, 3, 2, "ng-template", 6);
} }
function TablehelperComponent_p_table_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-table", 34, 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_p_table_1_ng_template_2_Template, 6, 1, "ng-template", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_p_table_1_ng_template_3_Template, 3, 2, "ng-template", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_p_table_1_4_Template, 1, 0, undefined, 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx_r1.records)("scrollable", true)("globalFilterFields", ctx_r1.colnames);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.actionbtn[1] == 1);
} }
class TablehelperComponent {
    constructor() {
        this.isActionBtn = true;
        this.first = 0;
        this.rows = 5;
        this.nopaging = true;
        this.scrollheight = "300px";
        this.datadeleted = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.datapreview = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.dataapprover = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
        // console.log("colldate>>>>>", this.colldate);
        // console.log("Header "+JSON.stringify(this.header));
        // console.log("actbutton "+JSON.stringify(this.actionbtn));
        // console.log("addbtn "+JSON.stringify(this.addbtnlink));
        // console.log("Scroll heigh : "+JSON.stringify(this.scrollheight));
        let i = 0;
        this.actionbtn.map((dt) => {
            if (dt > 0)
                i++;
        });
        console.log(i);
        if (i < 1) {
            this.isActionBtn = false;
        }
        if (this.records[0].created_date === "No records" ||
            this.records[0].created_date === "N/A")
            this.isActionBtn = false;
    }
    deleteConfirmation(payload) {
        // console.log("Di Emit nih "+JSON.stringify(payload));
        this.datadeleted.emit(payload);
    }
    previewConfirmation(payload) {
        this.datapreview.emit(payload);
    }
    approveConfirmation(payload) {
        this.dataapprover.emit(payload);
    }
    next() {
        this.first = this.first + this.rows;
    }
    prev() {
        this.first = this.first - this.rows;
    }
    reset() {
        this.first = 0;
    }
    isLastPage() {
        return this.records ? this.first === this.records.length - this.rows : true;
    }
    isFirstPage() {
        return this.records ? this.first === 0 : true;
    }
}
TablehelperComponent.ɵfac = function TablehelperComponent_Factory(t) { return new (t || TablehelperComponent)(); };
TablehelperComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TablehelperComponent, selectors: [["app-tablehelper"]], viewQuery: function TablehelperComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dt = _t.first);
    } }, inputs: { records: "records", wsearch: "wsearch", header: "header", colnames: "colnames", colwidth: "colwidth", colclasshalign: "colclasshalign", colmark: "colmark", colldate: "colldate", collink: "collink", collinkaction: "collinkaction", actionbtn: "actionbtn", addbtnlink: "addbtnlink", nopaging: "nopaging", scrollheight: "scrollheight" }, outputs: { datadeleted: "datadeleted", datapreview: "datapreview", dataapprover: "dataapprover" }, decls: 2, vars: 2, consts: [["dataKey", "id", "styleClass", "p-datatable-customs p-datatable-gridlines", "currentPageReportTemplate", "Showing {first} to {last} of {totalRecords} entries", "responsiveLayout", "scroll", 3, "value", "rows", "showCurrentPageReport", "rowsPerPageOptions", "paginator", "globalFilterFields", 4, "ngIf"], ["scrollHeight", "scrollheight", "scrollDirection", "both", 3, "value", "scrollable", "globalFilterFields", 4, "ngIf"], ["dataKey", "id", "styleClass", "p-datatable-customs p-datatable-gridlines", "currentPageReportTemplate", "Showing {first} to {last} of {totalRecords} entries", "responsiveLayout", "scroll", 3, "value", "rows", "showCurrentPageReport", "rowsPerPageOptions", "paginator", "globalFilterFields"], ["dt", ""], ["pTemplate", "caption"], ["pTemplate", "header"], ["pTemplate", "body"], [1, "p-d-flex"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus-circle", "class", "p-primary-btn", 3, "routerLink", "label", 4, "ngIf"], [1, "p-input-icon-right", "p-ml-auto"], [1, "pi", "pi-search"], ["pInputText", "", "type", "text", "placeholder", "Search keyword", 3, "input"], ["myInput", ""], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus-circle", 1, "p-primary-btn", 3, "routerLink", "label"], ["class", "p-text-center", 3, "pSortableColumn", "style", 4, "ngFor", "ngForOf"], ["class", "p-text-center", 3, "style", 4, "ngIf"], [1, "p-text-center", 3, "pSortableColumn"], ["field", "head.sort"], [1, "p-text-center"], ["class", "vertAlignCenter ", 3, "class", 4, "ngFor", "ngForOf"], ["class", "p-text-center", 4, "ngIf"], [1, "vertAlignCenter"], [3, "ngIf"], ["href", "#"], ["class", "pi pi-check-circle", "style", "color: green;", 4, "ngIf"], ["class", "pi pi-times-circle", "style", "color: red;", 4, "ngIf"], [1, "pi", "pi-check-circle", 2, "color", "green"], [1, "pi", "pi-times-circle", 2, "color", "red"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", "class", "p-button-rounded p-button-text cstnoheader", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-pencil", "class", "p-button-rounded p-button-text cstnoheader", 3, "routerLink", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-times", "style", "color: red;", "class", "p-button-rounded p-button-secondary p-button-text cstnoheader", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-pencil", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "routerLink"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-times", 1, "p-button-rounded", "p-button-secondary", "p-button-text", "cstnoheader", 2, "color", "red", 3, "click"], ["scrollHeight", "scrollheight", "scrollDirection", "both", 3, "value", "scrollable", "globalFilterFields"], [4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus-circle", "class", "p-primary-btn", "pTooltip", "Add record", 3, "routerLink", "label", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus-circle", "pTooltip", "Add record", 1, "p-primary-btn", 3, "routerLink", "label"], ["class", "vertAlignCenter ", 3, "class", "style", 4, "ngFor", "ngForOf"], ["class", "pi pi-check-circle", "pTooltip", "Activated", "style", "color: green;", 4, "ngIf"], ["class", "pi pi-times-circle", "pTooltip", "Deactivated", "style", "color: red;", 4, "ngIf"], ["class", "pi pi-clock", "style", "color: orange;", "pTooltip", "Add Data Approval", 4, "ngIf"], ["class", "pi pi-clock", "style", "color: orange;", "pTooltip", "Update Data Approval", 4, "ngIf"], ["class", "pi pi-clock", "style", "color: orange;", "pTooltip", "Delete Data Approval", 4, "ngIf"], ["pTooltip", "Activated", 1, "pi", "pi-check-circle", 2, "color", "green"], ["pTooltip", "Deactivated", 1, "pi", "pi-times-circle", 2, "color", "red"], ["pTooltip", "Add Data Approval", 1, "pi", "pi-clock", 2, "color", "orange"], ["pTooltip", "Update Data Approval", 1, "pi", "pi-clock", 2, "color", "orange"], ["pTooltip", "Delete Data Approval", 1, "pi", "pi-clock", 2, "color", "orange"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", "class", "p-button-rounded p-button-text cstnoheader", "pTooltip", "View detail", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-pencil", "class", "p-button-rounded p-button-text cstnoheader", "pTooltip", "Edit record", 3, "routerLink", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-times", "style", "color: red;", "class", "p-button-rounded p-button-secondary p-button-text cstnoheader", "pTooltip", "Delete record", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-check-circle", "class", "p-button-rounded p-button-text cstnoheader", "pTooltip", "Add data Approval", "tooltipPosition", "left", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-check-circle", "class", "p-button-rounded p-button-text cstnoheader", "pTooltip", "Edit data Approval", "tooltipPosition", "left", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-check-circle", "class", "p-button-rounded p-button-text cstnoheader", "pTooltip", "Delete data Approval", "tooltipPosition", "left", 3, "click", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", "pTooltip", "View detail", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-pencil", "pTooltip", "Edit record", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "routerLink"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-times", "pTooltip", "Delete record", 1, "p-button-rounded", "p-button-secondary", "p-button-text", "cstnoheader", 2, "color", "red", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-check-circle", "pTooltip", "Add data Approval", "tooltipPosition", "left", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-check-circle", "pTooltip", "Edit data Approval", "tooltipPosition", "left", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-check-circle", "pTooltip", "Delete data Approval", "tooltipPosition", "left", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "click"]], template: function TablehelperComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TablehelperComponent_p_table_0_Template, 5, 7, "p-table", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_p_table_1_Template, 5, 4, "p-table", 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.nopaging);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.nopaging);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], primeng_table__WEBPACK_IMPORTED_MODULE_2__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__["InputText"], primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLink"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], primeng_table__WEBPACK_IMPORTED_MODULE_2__["SortableColumn"], primeng_table__WEBPACK_IMPORTED_MODULE_2__["SortIcon"], primeng_tooltip__WEBPACK_IMPORTED_MODULE_7__["Tooltip"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DatePipe"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0YWJsZWhlbHBlci5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "lGQG":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./backend.service */ "cygB");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





class AuthService {
    constructor(sessionStorage, backend, router) {
        this.sessionStorage = sessionStorage;
        this.backend = backend;
        this.router = router;
        this.response = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](null);
        this.authenticated = false;
        this.tokenStorage = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"]('');
        this.sharedMessage = this.tokenStorage.asObservable();
    }
    isLoggedIn() {
        console.log('Apakah sudah is auth ? ' + this.authenticated);
        const token = sessionStorage.getItem('accesstoken');
        if (token !== null)
            this.authenticated = true;
        return this.authenticated;
    }
    loggedOut() {
        this.sessionStorage.clear();
        this.authenticated = false;
    }
    // whoAmi(){
    //   const url = 'adm/auth/who';
    //   return this.backend.get(url);
    // }
    whoAmi() {
        const url = 'adm/auth/who';
        return this.backend.get(url);
    }
    whoAmiInitialize(payload) {
        const url = 'integrate/api/initialtoken';
        return this.backend.post(url, { tokenNaked: payload });
    }
    loginAdmin(payload) {
        const url = 'adm/auth/signviaadmin';
        return this.backend.post(url, payload);
    }
    changeAppLication(payload) {
        const url = 'adm/auth/changeapp';
        return this.backend.post(url, { appid: payload });
    }
    setAuthStatus(status) {
        this.authenticated = status;
    }
    setToken(tokenIn) {
        this.tokenStorage.next(tokenIn);
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_2__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_3__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "lhZ7":
/*!***********************************************************!*\
  !*** ./src/app/pages/bpmp/bpmphome/bpmphome.component.ts ***!
  \***********************************************************/
/*! exports provided: BpmphomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BpmphomeComponent", function() { return BpmphomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

const _c0 = function () { return { width: "400px!important" }; };
class BpmphomeComponent {
    constructor() { }
    ngOnInit() { }
}
BpmphomeComponent.ɵfac = function BpmphomeComponent_Factory(t) { return new (t || BpmphomeComponent)(); };
BpmphomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BpmphomeComponent, selectors: [["app-bpmphome"]], decls: 5, vars: 3, consts: [[1, "p-text-center"], [2, "height", "200px"], ["alt", "commonwealth", "src", "assets/logos/commonwealthbank.png"]], template: function BpmphomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "BPM Portal");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJicG1waG9tZS5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "mPzt":
/*!*********************************************!*\
  !*** ./src/app/env/env.service.provider.ts ***!
  \*********************************************/
/*! exports provided: EnvServiceFactory, EnvServiceProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvServiceFactory", function() { return EnvServiceFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvServiceProvider", function() { return EnvServiceProvider; });
/* harmony import */ var _env_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./env.service */ "usjD");
 // import environment service
const EnvServiceFactory = () => {
    const env = new _env_service__WEBPACK_IMPORTED_MODULE_0__["EnvService"](); // Create env Instance
    // Read environment variables from browser window
    const browserWindow = window || {};
    const browserWindowEnv = browserWindow['env'] || {};
    // Assign environment variables from browser window to env
    // In the current implementation, properties from env.js overwrite defaults from the EnvService.
    // If needed, a deep merge can be performed here to merge properties instead of overwriting them.
    for (const key in browserWindowEnv) {
        // console.log(">>>>>>>>>>>>>> KEY S "+ key);
        if (browserWindowEnv.hasOwnProperty(key)) {
            env[key] = window['env'][key];
        }
    }
    return env;
};
const EnvServiceProvider = {
    provide: _env_service__WEBPACK_IMPORTED_MODULE_0__["EnvService"],
    useFactory: EnvServiceFactory,
    deps: [],
};


/***/ }),

/***/ "mnfZ":
/*!**********************************************************!*\
  !*** ./src/app/interceptors/interceptor-http.service.ts ***!
  \**********************************************************/
/*! exports provided: InterceptorHttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptorHttpService", function() { return InterceptorHttpService; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _loader_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../loader/loader.service */ "t0Il");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "lGQG");





class InterceptorHttpService {
    constructor(loaderService, session, authservice) {
        this.loaderService = loaderService;
        this.session = session;
        this.authservice = authservice;
    }
    intercept(req, next) {
        // if(req.method != 'GET' && !req.headers.has('Content-Type')){
        //   req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        // }
        req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        // console.log(">>>>>>>>>>>>>>>>>>>>>>> MASUK INTERCEPT NIH  "+ req.method)
        this.loaderService.isLoading.next(true);
        req = this.updateAccessToken(req);
        // req = this.updateAT(req);
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["finalize"])(() => {
            this.loaderService.isLoading.next(false);
        }));
    }
    updateAccessToken(req) {
        const authToken = this.session.get("accesstoken");
        if (authToken) {
            req = req.clone({
                headers: req.headers.set("Authorization", `Bearer ${authToken}`)
            });
        }
        return req;
    }
}
InterceptorHttpService.ɵfac = function InterceptorHttpService_Factory(t) { return new (t || InterceptorHttpService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_loader_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"])); };
InterceptorHttpService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: InterceptorHttpService, factory: InterceptorHttpService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "nMyi":
/*!*******************************************************************!*\
  !*** ./src/app/layout/backmenulayout/backmenulayout.component.ts ***!
  \*******************************************************************/
/*! exports provided: BackmenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackmenulayoutComponent", function() { return BackmenulayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");




const _c0 = function () { return { "background": "none", "border": "none!important" }; };
class BackmenulayoutComponent {
    constructor() { }
    ngOnInit() {
    }
}
BackmenulayoutComponent.ɵfac = function BackmenulayoutComponent_Factory(t) { return new (t || BackmenulayoutComponent)(); };
BackmenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BackmenulayoutComponent, selectors: [["app-backmenulayout"]], decls: 15, vars: 3, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], ["id", "wrapper"], ["id", "contentliquid"], ["id", "contentnomenu"], [1, "p-toolbar-group-left", 2, "padding-left", "10px", "padding-top", "10px", "padding-bottom", "10px"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-arrow-left", 1, "p-button-rounded", "p-button-text", "p-button-plain"]], template: function BackmenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Error Page");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
    } }, directives: [primeng_toolbar__WEBPACK_IMPORTED_MODULE_1__["Toolbar"], primeng_button__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJiYWNrbWVudWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "pSR5":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/forgotpassword/verifikasiemail/verifikasiemail.component.ts ***!
  \***********************************************************************************/
/*! exports provided: VerifikasiemailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifikasiemailComponent", function() { return VerifikasiemailComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/forgotpassword/forgotpassword.service */ "Jdzd");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/button */ "jIHw");
















function VerifikasiemailComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function VerifikasiemailComponent_div_13_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function VerifikasiemailComponent_div_13_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Password not valid");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function VerifikasiemailComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, VerifikasiemailComponent_div_13_span_1_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, VerifikasiemailComponent_div_13_span_2_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.validatePass);
} }
function VerifikasiemailComponent_div_19_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Confirm Password does not match with password");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function VerifikasiemailComponent_div_19_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Confirmed Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function VerifikasiemailComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, VerifikasiemailComponent_div_19_span_1_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, VerifikasiemailComponent_div_19_span_2_Template, 2, 0, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.confirmPassword.errors.mustMatch);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.confirmPassword.errors.required);
} }
function VerifikasiemailComponent_ng_template_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "360px" }; };
class VerifikasiemailComponent {
    constructor(messageService, sessionStorage, route, activatedRoute, formBuilder, backend, authservice, forgotpasswordService, location) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.forgotpasswordService = forgotpasswordService;
        this.location = location;
        this.blockedDocument = false;
        this.password = '';
        this.confirmPassword = '';
        this.errorMsg = '';
        this.isProcess = false;
        this.submitted = false;
        this.sama = false;
        this.isPasswordSame = true;
        this.isValidatePass = true;
        this.id = '';
        this.cek = '';
    }
    ngOnInit() {
        this.userForm = this.formBuilder.group({
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        }, { validator: this.checkPassword('password', 'confirmPassword') });
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.userForm.controls;
    }
    get userFormControl() {
        return this.userForm.controls;
    }
    checkPassword(controlName, matchingControlName) {
        return (formGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
                this.isPasswordSame = matchingControl.status == 'VALID' ? true : false;
            }
        };
    }
    onSubmit(data) {
        var _a, _b;
        this.submitted = true;
        this.cek = this.userForm.controls.confirmPassword.status;
        // console.log('this.userForm', this.userForm.controls.confirmPassword.status);
        console.log('this.userForm', this.userForm);
        if (this.cek == 'INVALID') {
            this.submitted = true;
        }
        else {
            this.activatedRoute.params.subscribe((paramsId) => {
                this.id = paramsId.id;
            });
            let payload;
            payload = {
                password: (_a = this.userForm.get('password')) === null || _a === void 0 ? void 0 : _a.value,
                confirmPassword: (_b = this.userForm.get('confirmPassword')) === null || _b === void 0 ? void 0 : _b.value,
                id: this.id,
            };
            // console.log('payload ::', payload);
            this.forgotpasswordService
                .verifikasiemail(payload)
                .subscribe((resp) => {
                console.log(resp);
                if (resp.status === 200) {
                    this.showTopCenterInfo('Password has beed changed');
                    setTimeout(() => {
                        this.route.navigate(['/auth/login']);
                    }, 4000);
                }
                else if (resp.status === 201) {
                    this.showTopCenterErr('Link Expried');
                    setTimeout(() => {
                        this.route.navigate(['/auth/login']);
                    }, 4000);
                }
                else if (resp.status === 422) {
                    console.log('cek');
                    const validateControl = this.userForm.controls.password;
                    validateControl.setErrors({ validatePass: true });
                    this.isValidatePass =
                        validateControl.status == 'VALID' ? true : false;
                }
            });
        }
        //  this.blockDocument();
    }
    showTopCenterInfo(message) {
        this.messageService.add({
            severity: 'info',
            summary: 'Confirmed',
            detail: message,
        });
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: message,
        });
    }
    blockDocument() {
        this.blockedDocument = true;
        //  setTimeout(() => {
        //      this.blockedDocument = false;
        //      this.showTopCenterErr("Invalid user and password!")
        //  }, 3000);
    }
}
VerifikasiemailComponent.ɵfac = function VerifikasiemailComponent_Factory(t) { return new (t || VerifikasiemailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_7__["ForgotpasswordService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"])); };
VerifikasiemailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: VerifikasiemailComponent, selectors: [["app-verifikasiemail"]], decls: 22, vars: 7, consts: [[3, "target", "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], [1, "form", 3, "formGroup", "ngSubmit"], ["header", "Please Insert for Verified", "styleClass", "p-card-shadow p-header-w50"], ["pTemplate", "header"], [1, "p-fluid"], [1, "p-field"], ["for", "password", 1, "labelpb"], ["id", "password", "name", "password", "type", "password", "required", "", "formControlName", "password", "pInputText", ""], ["class", "p-field", 4, "ngIf"], [2, "height", "0.3rem"], ["for", "confirmPassword", 1, "labelpb"], ["id", "confirmPassword", "type", "password", "formControlName", "confirmPassword", "pInputText", ""], ["pTemplate", "footer"], [1, "p-text-center"], ["alt", "tai", "src", "assets/logos/komiportal.png"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], [1, "p-text-right"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Login", 1, "p-primary-btn"]], template: function VerifikasiemailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-blockUI", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function VerifikasiemailComponent_Template_form_ngSubmit_5_listener($event) { return ctx.onSubmit($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, VerifikasiemailComponent_ng_template_7_Template, 2, 0, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "New Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, VerifikasiemailComponent_div_13_Template, 3, 2, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Confirmed Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, VerifikasiemailComponent_div_19_Template, 3, 2, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, VerifikasiemailComponent_ng_template_20_Template, 2, 0, "ng-template", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.userForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](6, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.confirmPassword.errors);
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_9__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_10__["ProgressSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_11__["Card"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__["InputText"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_13__["Messages"], primeng_button__WEBPACK_IMPORTED_MODULE_14__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ2ZXJpZmlrYXNpZW1haWwuY29tcG9uZW50LnNjc3MifQ== */", "body[_ngcontent-%COMP%] {\n        background: #007dc5\n    }\n\n    section[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%)\n    }"] });


/***/ }),

/***/ "pZjL":
/*!*********************************************************!*\
  !*** ./src/app/pages/root/profile/profile.component.ts ***!
  \*********************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../change-password/change-password.component */ "HisP");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/card */ "QIUk");

//import { ProfileConfigComponent } from '../profile-config/profile-config.component';










class ProfileComponent {
    constructor(authservice, 
    // private usermanager: UsermanagerService,
    sessionStorage, service, dialogService, route, messageService) {
        this.authservice = authservice;
        this.sessionStorage = sessionStorage;
        this.service = service;
        this.dialogService = dialogService;
        this.route = route;
        this.messageService = messageService;
        this.ref = new primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DynamicDialogRef"]();
        this.profile = {};
        this.bcitems = [];
        this.home = {};
    }
    ngOnInit() {
        this.refreshData();
        this.bcitems = [{ label: 'Profile' }];
        this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    }
    refreshData() {
        this.authservice.whoAmi().subscribe((data) => {
            if ((data.status = 200)) {
                this.profile.fullname = data.data.fullname;
                this.profile.userid = data.data.userid;
                this.profile.tenant = data.data.tnname;
                this.profile.level = data.data.leveltenant;
            }
        });
    }
    changepassword() {
        this.ref = this.dialogService.open(_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_1__["ChangePasswordComponent"], {
            header: 'Change Password',
            baseZIndex: 10000,
            dismissableMask: false,
        });
        this.ref.onClose.subscribe((value) => {
            if (value === 200) {
                this.refreshData();
                this.showTopSuccess('Password Changed Successfull');
            }
            else {
                this.showTopError('Failed Change Password');
            }
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: message,
        });
    }
    showTopInfo(message) {
        this.messageService.add({
            severity: 'info',
            summary: 'Info',
            detail: message,
        });
    }
    showTopWarning(message) {
        this.messageService.add({
            severity: 'warn',
            summary: 'Warning',
            detail: message,
        });
    }
    showTopError(message) {
        this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: message,
        });
    }
}
ProfileComponent.ɵfac = function ProfileComponent_Factory(t) { return new (t || ProfileComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_4__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["MessageService"])); };
ProfileComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: ProfileComponent, selectors: [["app-profile"]], decls: 35, vars: 5, consts: [[2, "background", "transparent"], [2, "height", "20px"], [3, "model", "home"], [1, "wrapperinside"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-12", "p-md-3"], [1, "p-col-12", "p-md-1"], [1, "p-col-12", "p-md-8"], [3, "routerLink", "click"]], template: function ProfileComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](2, "p-breadcrumb", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "p-card");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, "Fullname");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](10, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "p", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](15, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](16, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](17, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "p", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](20, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](21, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](22, "Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](23, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](24, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](25, "p", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](26, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ProfileComponent_Template_a_click_26_listener() { return ctx.changepassword(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](27, "Change Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](28, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](29, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](30, "Company");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](31, "p", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](32, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](33, "p", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](34);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("model", ctx.bcitems)("home", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.profile.fullname);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.profile.userid);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.profile.tenant);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__["Breadcrumb"], primeng_card__WEBPACK_IMPORTED_MODULE_9__["Card"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLinkWithHref"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9maWxlLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "pjdP":
/*!******************************************************!*\
  !*** ./src/app/services/servbpmp/company.service.ts ***!
  \******************************************************/
/*! exports provided: CompanyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyService", function() { return CompanyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class CompanyService {
    constructor(service) {
        this.service = service;
    }
    getAllCompany() {
        const url = "api/company/getCompany";
        return this.service.baseGet(url);
    }
    insertCompany(payload) {
        const url = "api/company/insertCompany";
        return this.service.basePost(url, payload);
    }
    updateCompany(payload) {
        const url = "api/company/updateCompany";
        return this.service.basePost(url, payload);
    }
    getCompany(id) {
        const url = "api/company/getCompany/" + id;
        return this.service.baseGet(url);
    }
    deleteCompany(payload) {
        console.log(payload);
        const url = "api/company/deleteCompany/" + payload;
        return this.service.baseGet(url);
    }
}
CompanyService.ɵfac = function CompanyService_Factory(t) { return new (t || CompanyService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
CompanyService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: CompanyService, factory: CompanyService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "q50L":
/*!*********************************************************************!*\
  !*** ./src/app/pages/root/resourceusage/resourceusage.component.ts ***!
  \*********************************************************************/
/*! exports provided: ResourceusageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResourceusageComponent", function() { return ResourceusageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class ResourceusageComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Resource Usages' }
        ];
    }
}
ResourceusageComponent.ɵfac = function ResourceusageComponent_Factory(t) { return new (t || ResourceusageComponent)(); };
ResourceusageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ResourceusageComponent, selectors: [["app-resourceusage"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function ResourceusageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXVzYWdlLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "qUOm":
/*!*******************************************************************!*\
  !*** ./src/app/pages/root/smtpaccounts/smtpaccounts.component.ts ***!
  \*******************************************************************/
/*! exports provided: SmtpaccountsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmtpaccountsComponent", function() { return SmtpaccountsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class SmtpaccountsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'SMTP Management' }
        ];
    }
}
SmtpaccountsComponent.ɵfac = function SmtpaccountsComponent_Factory(t) { return new (t || SmtpaccountsComponent)(); };
SmtpaccountsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SmtpaccountsComponent, selectors: [["app-smtpaccounts"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function SmtpaccountsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzbXRwYWNjb3VudHMuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "sBLG":
/*!***************************************************************************!*\
  !*** ./src/app/pages/root/applicationgroup/applicationgroup.component.ts ***!
  \***************************************************************************/
/*! exports provided: ApplicationgroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationgroupComponent", function() { return ApplicationgroupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/group-service.service */ "XfbB");
/* harmony import */ var src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils/aclmenuchecker.service */ "42A2");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");














function ApplicationgroupComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 19);
} }
function ApplicationgroupComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function ApplicationgroupComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.deleteConfirmation($event); })("datapreview", function ApplicationgroupComponent_div_3_Template_app_tablehelper_datapreview_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.previewConfirmation($event); })("dataapprover", function ApplicationgroupComponent_div_3_Template_app_tablehelper_dataapprover_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.approvalData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.groupsData)("header", ctx_r1.grpheader)("wsearch", true)("actionbtn", ctx_r1.grpactionbtn)("colnames", ctx_r1.grpcolname)("colwidth", ctx_r1.grpcolwidth)("colclasshalign", ctx_r1.grpcolhalign)("addbtnlink", ctx_r1.grpaddbtn)("scrollheight", ctx_r1.scrollheight)("nopaging", false)("colmark", 1);
} }
function ApplicationgroupComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.deleteGroup(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ApplicationgroupComponent_ng_template_25_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_25_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r12.displayPrv = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ApplicationgroupComponent_ng_template_31_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_31_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r14.viewApprove = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_31_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r16.approvalSubmit(4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p-button", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_31_Template_p_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r17.approvalSubmit(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
const _c1 = function () { return { width: "50vw" }; };
class ApplicationgroupComponent {
    constructor(authservice, dialogService, messageService, groupService, aclMenuService, router) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.groupService = groupService;
        this.aclMenuService = aclMenuService;
        this.router = router;
        this.display = false;
        this.scrollheight = "400px";
        this.viewApprove = false;
        this.displayPrv = false;
        this.selectedgrp = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.grpheader = [
            { label: "Group Name", sort: "groupname" },
            { label: "Status", sort: "active" },
            { label: "Created At", sort: "created_at" },
        ];
        this.grpcolname = ["groupname", "active", "created_at"];
        this.grpcolhalign = ["", "p-text-center", "p-text-center"];
        this.grpcolwidth = [{ width: "670px" }, { width: "170px" }, ""];
        // grpcolwidth: any = ['', { width: '170px' }];
        this.grpcollinghref = { url: "#", label: "Application" };
        this.grpactionbtn = [1, 1, 1, 1, 1, 1];
        this.grpaddbtn = { route: "detail", label: "Add Data" };
        this.grpanizations = [];
        this.groupsData = [];
    }
    ngOnInit() {
        this.home = { icon: "pi pi-home", routerLink: "/mgm/home" };
        this.breadcrumbs = [{ label: "Group & Acl" }];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
                console.log("MENU ALL ACL Set");
                this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
                    if (JSON.stringify(dataacl.acl) === "{}") {
                        console.log("No ACL Founded");
                    }
                    else {
                        console.log("ACL Founded");
                        console.log(dataacl.acl);
                        this.grpactionbtn[0] = dataacl.acl.create;
                        this.grpactionbtn[1] = dataacl.acl.read;
                        this.grpactionbtn[2] = dataacl.acl.update;
                        this.grpactionbtn[3] = dataacl.acl.delete;
                        this.grpactionbtn[4] = dataacl.acl.view;
                        this.grpactionbtn[5] = dataacl.acl.approval;
                    }
                });
            });
        });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
        console.log(">>>>>>> Refresh group ");
        //   if ((data.status = 200)) {
        //     this.grpanizationService
        //       .retrivegrpByTenant()
        //       .subscribe((grpall: BackendResponse) => {
        //         // console.log('>>>>>>> ' + JSON.stringify(grpall));
        //         this.grpanizations = grpall.data;
        //         if(this.grpanizations.length < 1){
        //           let objtmp = {"grpcode":"No records", "grpname":"No records","grpdescription":"No records","application":"No records","created_by":"No records"}
        //           this.grpanizations = [];
        //           this.grpanizations.push(objtmp);
        //         }
        //        this.isFetching=false;
        //       });
        //   }
        // });
        this.groupService.getAllGroup().subscribe((result) => {
            var _a;
            // console.log('>>>>>>> ' + JSON.stringify(result));
            if (((_a = result.data.userGroup) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                this.groupsData = [];
                this.groupsData = result.data.userGroup;
            }
            else {
                this.groupsData = [];
                let objtmp = { groupname: "No records" };
                this.groupsData.push(objtmp);
            }
            this.isFetching = false;
        });
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedgrp = data;
    }
    previewConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.displayPrv = true;
        this.selectedgrp = data;
    }
    approvalData(data) {
        console.log(data);
        this.viewApprove = true;
        this.selectedgrp = data;
    }
    approvalSubmit(status) {
        console.log(this.selectedgrp);
        console.log(status);
        let payload = {
            id: this.selectedgrp.id,
            oldactive: this.selectedgrp.active,
            isactive: status,
            idapproval: this.selectedgrp.idapproval,
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.groupService
            .editGroupActive(payload)
            .subscribe((result) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status === 200) {
                this.refreshingApp();
                this.viewApprove = false;
            }
        });
    }
    deleteGroup() {
        console.log(this.selectedgrp);
        let group = this.selectedgrp;
        const payload = { group };
        console.log(">> Di delete " + JSON.stringify(payload));
        this.groupService
            .deleteGroup(payload)
            .subscribe((resp) => {
            console.log(">> hasil Delete " + resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
        // this.grpanizationService
        //   .deletegrp(payload)
        //   .subscribe((resp: BackendResponse) => {
        //     console.log(resp);
        //     if (resp.status === 200) {
        //       this.showTopSuccess(resp.data);
        //     }
        //     this.display = false;
        //     this.refreshingApp();
        //   });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: "success",
            summary: "Deleted",
            detail: message,
        });
    }
}
ApplicationgroupComponent.ɵfac = function ApplicationgroupComponent_Factory(t) { return new (t || ApplicationgroupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_4__["GroupServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_utils_aclmenuchecker_service__WEBPACK_IMPORTED_MODULE_5__["AclmenucheckerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"])); };
ApplicationgroupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ApplicationgroupComponent, selectors: [["app-applicationgroup"]], decls: 32, vars: 27, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete Group", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["header", "Preview", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "grpname"], ["id", "grpname", 2, "font-weight", "600"], ["for", "createdat"], ["id", "createdat", 2, "font-weight", "600"], [2, "height", "10px"], ["id", "space", 2, "font-weight", "600"], ["header", "Approval", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [1, "p-fluid"], [1, "p-col-12", 2, "font-size", "18px"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "colclasshalign", "addbtnlink", "scrollheight", "nopaging", "colmark", "datadeleted", "datapreview", "dataapprover"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"], ["label", "OK", "styleClass", "p-button-text", 3, "click"], ["label", "Cancel", "styleClass", "p-button-text", 3, "click"], ["label", "Reject", "styleClass", "p-button-text", 3, "click"], ["label", "Approve", "styleClass", "p-button-text", 3, "click"]], template: function ApplicationgroupComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ApplicationgroupComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ApplicationgroupComponent_div_3_Template, 2, 11, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ApplicationgroupComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Organization?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ApplicationgroupComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p-dialog", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ApplicationgroupComponent_Template_p_dialog_visibleChange_8_listener($event) { return ctx.displayPrv = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Group Name :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Created at :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Attributes : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, ApplicationgroupComponent_ng_template_25_Template, 1, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "p-dialog", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ApplicationgroupComponent_Template_p_dialog_visibleChange_26_listener($event) { return ctx.viewApprove = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "This records need approval from you?, please choose wisely");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, ApplicationgroupComponent_ng_template_31_Template, 3, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.groupsData.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.groupsData.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](24, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](25, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.displayPrv)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("\u00A0", ctx.selectedgrp.groupname, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("\u00A0", ctx.selectedgrp.created_at, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](26, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.viewApprove)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_7__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_9__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_10__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_11__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_12__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBsaWNhdGlvbmdyb3VwLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "t0Il":
/*!******************************************!*\
  !*** ./src/app/loader/loader.service.ts ***!
  \******************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


class LoaderService {
    constructor() {
        this.isLoading = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](false);
    }
}
LoaderService.ɵfac = function LoaderService_Factory(t) { return new (t || LoaderService)(); };
LoaderService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: LoaderService, factory: LoaderService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "tjdq":
/*!****************************************************!*\
  !*** ./src/app/services/servbpmp/rules.service.ts ***!
  \****************************************************/
/*! exports provided: RulesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RulesService", function() { return RulesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class RulesService {
    constructor(service) {
        this.service = service;
    }
    getAllRules() {
        const url = "api/rules/getRules";
        return this.service.baseGet(url);
    }
    insertRules(payload) {
        const url = "api/rules/insertRules";
        return this.service.basePost(url, payload);
    }
    updateRules(payload) {
        const url = "api/rules/updateRules";
        return this.service.basePost(url, payload);
    }
    getRules(id) {
        const url = "api/rules/getRules/" + id;
        return this.service.baseGet(url);
    }
    deleteRules(payload) {
        console.log(payload);
        const url = "api/rules/deleteRules/" + payload;
        return this.service.baseGet(url);
    }
}
RulesService.ɵfac = function RulesService_Factory(t) { return new (t || RulesService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
RulesService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: RulesService, factory: RulesService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "u0Pv":
/*!*******************************************************!*\
  !*** ./src/app/services/root/organization.service.ts ***!
  \*******************************************************/
/*! exports provided: OrganizationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganizationService", function() { return OrganizationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class OrganizationService {
    constructor(service) {
        this.service = service;
    }
    retriveOrgByTenant() {
        const url = "adm/organization/allbytenantid";
        return this.service.get(url);
    }
    insertOrg(payload) {
        const url = "adm/organization/addorganization";
        return this.service.post(url, payload);
    }
    retriveOrgByTenantAndOrgId(orgId) {
        const url = `adm/organization/retriveByTenantAndOrgId/${orgId}`;
        return this.service.get(url);
    }
    editOrg(payload) {
        const url = "adm/organization/editorganization";
        return this.service.post(url, payload);
    }
    deleteOrg(payload) {
        const url = "adm/organization/deleteorganization";
        return this.service.post(url, payload);
    }
}
OrganizationService.ɵfac = function OrganizationService_Factory(t) { return new (t || OrganizationService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
OrganizationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: OrganizationService, factory: OrganizationService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "usjD":
/*!************************************!*\
  !*** ./src/app/env/env.service.ts ***!
  \************************************/
/*! exports provided: EnvService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvService", function() { return EnvService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class EnvService {
    constructor() {
        this.apiUrl = "http://localhost:3000/";
        this.baseUrl = "http://localhost:3014/";
    }
}
EnvService.ɵfac = function EnvService_Factory(t) { return new (t || EnvService)(); };
EnvService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: EnvService, factory: EnvService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./guard/guard.guard */ "fNSO");
/* harmony import */ var _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout/backmenulayout/backmenulayout.component */ "nMyi");
/* harmony import */ var _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout/mainmenulayout/mainmenulayout.component */ "SuKp");
/* harmony import */ var _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layout/nomenulayout/nomenulayout.component */ "WQ6N");
/* harmony import */ var _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/errorpage/errorpage.component */ "bzlq");
/* harmony import */ var _pages_bpmp_bpmphome_bpmpintercept_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/bpmp/bpmphome/bpmpintercept.component */ "AUuv");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/login/login.component */ "D8EZ");
/* harmony import */ var _pages_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/forgotpassword/forgotpassword.component */ "QdSJ");
/* harmony import */ var _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationdetail/applicationdetail.component */ "7YUa");
/* harmony import */ var _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationgroup.component */ "sBLG");
/* harmony import */ var _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/root/users/userdetail/userdetail.component */ "z+Ab");
/* harmony import */ var _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/root/users/users.component */ "brkH");
/* harmony import */ var _pages_root_systemparam_systemparam_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pages/root/systemparam/systemparam.component */ "AYsR");
/* harmony import */ var _pages_root_systemparam_systemparamdetail_systemparamdetail_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pages/root/systemparam/systemparamdetail/systemparamdetail.component */ "e3kg");
/* harmony import */ var _pages_root_profile_profile_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/root/profile/profile.component */ "pZjL");
/* harmony import */ var _pages_forgotpassword_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/forgotpassword/resetpassword/resetpassword.component */ "+G24");
/* harmony import */ var _pages_forgotpassword_verifikasiemail_verifikasiemail_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/forgotpassword/verifikasiemail/verifikasiemail.component */ "pSR5");
/* harmony import */ var _pages_bpmp_bpmphome_bpmphome_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/bpmp/bpmphome/bpmphome.component */ "lhZ7");
/* harmony import */ var _pages_bpmp_company_company_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pages/bpmp/company/company.component */ "JxjP");
/* harmony import */ var _pages_bpmp_company_companydetail_companydetail_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./pages/bpmp/company/companydetail/companydetail.component */ "6t8E");
/* harmony import */ var _pages_bpmp_channel_channeldetail_channeldetail_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./pages/bpmp/channel/channeldetail/channeldetail.component */ "YBn2");
/* harmony import */ var _pages_bpmp_channel_channel_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./pages/bpmp/channel/channel.component */ "UZDb");
/* harmony import */ var _pages_bpmp_companyreq_companyreqdetail_companyreqdetail_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./pages/bpmp/companyreq/companyreqdetail/companyreqdetail.component */ "ZgGJ");
/* harmony import */ var _pages_bpmp_companyreq_companyreq_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./pages/bpmp/companyreq/companyreq.component */ "dJaT");
/* harmony import */ var _pages_bpmp_channelreq_channelreq_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pages/bpmp/channelreq/channelreq.component */ "eUMV");
/* harmony import */ var _pages_bpmp_rulesreq_rulesreq_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./pages/bpmp/rulesreq/rulesreq.component */ "INgj");
/* harmony import */ var _pages_bpmp_rulesreq_rulesreqdetail_rulesreqdetail_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./pages/bpmp/rulesreq/rulesreqdetail/rulesreqdetail.component */ "fRat");
/* harmony import */ var _pages_bpmp_rules_rules_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./pages/bpmp/rules/rules.component */ "FGci");
/* harmony import */ var _pages_bpmp_rules_rulesdetail_rulesdetail_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./pages/bpmp/rules/rulesdetail/rulesdetail.component */ "7LVI");
/* harmony import */ var _pages_bpmp_transactionmonitoring_transactionmonitoring_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./pages/bpmp/transactionmonitoring/transactionmonitoring.component */ "Es3d");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/core */ "fXoL");

































const routes = [
    { path: "", redirectTo: "auth/login", pathMatch: "full" },
    {
        path: ":config",
        canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
        component: _pages_bpmp_bpmphome_bpmpintercept_component__WEBPACK_IMPORTED_MODULE_6__["BpmpinterceptComponent"],
    },
    {
        path: ":config",
        canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
        component: _pages_bpmp_bpmphome_bpmpintercept_component__WEBPACK_IMPORTED_MODULE_6__["BpmpinterceptComponent"],
    },
    {
        path: "mgm",
        canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
        component: _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_3__["MainmenulayoutComponent"],
        children: [
            { path: "home", component: _pages_bpmp_bpmphome_bpmphome_component__WEBPACK_IMPORTED_MODULE_18__["BpmphomeComponent"] },
            { path: "profile", component: _pages_root_profile_profile_component__WEBPACK_IMPORTED_MODULE_15__["ProfileComponent"] },
            {
                path: "user",
                canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
                children: [
                    { path: "userslist", component: _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_12__["UsersComponent"] },
                    {
                        path: "userslist",
                        children: [
                            { path: "detail", component: _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_11__["UserdetailComponent"] },
                            { path: "detail#/:id", component: _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_11__["UserdetailComponent"] },
                        ],
                    },
                ],
            },
            {
                path: "acl",
                canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
                children: [
                    { path: "grouplist", component: _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_10__["ApplicationgroupComponent"] },
                    {
                        path: "grouplist",
                        children: [
                            { path: "detail", component: _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_9__["ApplicationdetailComponent"] },
                            { path: "detail#/:id", component: _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_9__["ApplicationdetailComponent"] },
                        ],
                    },
                ],
            },
            {
                path: "system",
                canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
                children: [
                    { path: "sysparam", component: _pages_root_systemparam_systemparam_component__WEBPACK_IMPORTED_MODULE_13__["SystemparamComponent"] },
                    {
                        path: "sysparam",
                        children: [
                            { path: "detail", component: _pages_root_systemparam_systemparamdetail_systemparamdetail_component__WEBPACK_IMPORTED_MODULE_14__["SystemparamdetailComponent"] },
                            { path: "detail#/:id", component: _pages_root_systemparam_systemparamdetail_systemparamdetail_component__WEBPACK_IMPORTED_MODULE_14__["SystemparamdetailComponent"] },
                        ],
                    },
                ],
            },
            {
                path: "ubp",
                canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]],
                children: [
                    {
                        path: "transactionlist",
                        component: _pages_bpmp_transactionmonitoring_transactionmonitoring_component__WEBPACK_IMPORTED_MODULE_30__["TransactionmonitoringComponent"],
                    },
                    { path: "companytlist", component: _pages_bpmp_company_company_component__WEBPACK_IMPORTED_MODULE_19__["CompanyComponent"] },
                    {
                        path: "companytlist",
                        children: [
                            { path: "detail", component: _pages_bpmp_company_companydetail_companydetail_component__WEBPACK_IMPORTED_MODULE_20__["CompanydetailComponent"] },
                            { path: "detail#/:id", component: _pages_bpmp_company_companydetail_companydetail_component__WEBPACK_IMPORTED_MODULE_20__["CompanydetailComponent"] },
                        ],
                    },
                    { path: "channellist", component: _pages_bpmp_channel_channel_component__WEBPACK_IMPORTED_MODULE_22__["ChannelComponent"] },
                    {
                        path: "channellist",
                        children: [
                            { path: "detail", component: _pages_bpmp_channel_channeldetail_channeldetail_component__WEBPACK_IMPORTED_MODULE_21__["ChanneldetailComponent"] },
                            { path: "detail#/:id", component: _pages_bpmp_channel_channeldetail_channeldetail_component__WEBPACK_IMPORTED_MODULE_21__["ChanneldetailComponent"] },
                        ],
                    },
                    { path: "companyreq", component: _pages_bpmp_companyreq_companyreq_component__WEBPACK_IMPORTED_MODULE_24__["CompanyreqComponent"] },
                    {
                        path: "companyreq",
                        component: _pages_bpmp_companyreq_companyreq_component__WEBPACK_IMPORTED_MODULE_24__["CompanyreqComponent"],
                    },
                    {
                        path: "companyreq",
                        children: [
                            { path: "detail", component: _pages_bpmp_companyreq_companyreqdetail_companyreqdetail_component__WEBPACK_IMPORTED_MODULE_23__["CompanyreqdetailComponent"] },
                            { path: "detail#/:id", component: _pages_bpmp_companyreq_companyreqdetail_companyreqdetail_component__WEBPACK_IMPORTED_MODULE_23__["CompanyreqdetailComponent"] },
                        ],
                    },
                    {
                        path: "channelreq",
                        component: _pages_bpmp_channelreq_channelreq_component__WEBPACK_IMPORTED_MODULE_25__["ChannelreqComponent"],
                    },
                    {
                        path: "rules",
                        component: _pages_bpmp_rules_rules_component__WEBPACK_IMPORTED_MODULE_28__["RulesComponent"],
                    },
                    {
                        path: "rules",
                        children: [
                            {
                                path: "detail",
                                component: _pages_bpmp_rules_rulesdetail_rulesdetail_component__WEBPACK_IMPORTED_MODULE_29__["RulesdetailComponent"],
                            },
                            {
                                path: "detail#/:id",
                                component: _pages_bpmp_rules_rulesdetail_rulesdetail_component__WEBPACK_IMPORTED_MODULE_29__["RulesdetailComponent"],
                            },
                        ],
                    },
                    {
                        path: "rulesreq",
                        component: _pages_bpmp_rulesreq_rulesreq_component__WEBPACK_IMPORTED_MODULE_26__["RulesreqComponent"],
                    },
                    {
                        path: "rulesreq",
                        children: [
                            {
                                path: "detail",
                                component: _pages_bpmp_rulesreq_rulesreqdetail_rulesreqdetail_component__WEBPACK_IMPORTED_MODULE_27__["RulesreqdetailComponent"],
                            },
                            {
                                path: "detail#/:id",
                                component: _pages_bpmp_rulesreq_rulesreqdetail_rulesreqdetail_component__WEBPACK_IMPORTED_MODULE_27__["RulesreqdetailComponent"],
                            },
                        ],
                    },
                ],
            },
        ],
    },
    {
        path: "nopage",
        component: _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_2__["BackmenulayoutComponent"],
        children: [{ path: "404", component: _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_5__["ErrorpageComponent"] }],
    },
    {
        path: "auth",
        data: { title: "Login" },
        component: _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_4__["NomenulayoutComponent"],
        children: [
            { path: "login", component: _pages_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] },
            { path: "forgotpassword", component: _pages_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_8__["ForgotpasswordComponent"] },
            { path: "resetpassword/:id", component: _pages_forgotpassword_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_16__["ResetpasswordComponent"] },
            { path: "verifikasiemail/:id", component: _pages_forgotpassword_verifikasiemail_verifikasiemail_component__WEBPACK_IMPORTED_MODULE_17__["VerifikasiemailComponent"] },
        ],
    },
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_31__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_31__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_31__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "wite":
/*!*********************************************************!*\
  !*** ./src/app/services/servbpmp/trxmonitor.service.ts ***!
  \*********************************************************/
/*! exports provided: TrxmonitorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrxmonitorService", function() { return TrxmonitorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class TrxmonitorService {
    constructor(service) {
        this.service = service;
    }
    getAllTrxMon() {
        const url = "api/trx/getTransaction";
        return this.service.baseGet(url);
    }
    postParam(payload) {
        // const url = 'komi/montrx/getbyparam';
        const url = "api/trx/getTransactionByParam";
        return this.service.basePost(url, payload);
    }
}
TrxmonitorService.ɵfac = function TrxmonitorService_Factory(t) { return new (t || TrxmonitorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
TrxmonitorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TrxmonitorService, factory: TrxmonitorService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "xjfA":
/*!******************************************************!*\
  !*** ./src/app/services/servbpmp/channel.service.ts ***!
  \******************************************************/
/*! exports provided: ChannelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChannelService", function() { return ChannelService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class ChannelService {
    constructor(service) {
        this.service = service;
    }
    getAllChannel() {
        const url = "api/channel/getChannel";
        return this.service.baseGet(url);
    }
    insertChannel(payload) {
        const url = "api/channel/insertChannel";
        return this.service.basePost(url, payload);
    }
    updateChannel(payload) {
        const url = "api/channel/updateChannel";
        return this.service.basePost(url, payload);
    }
    getChannel(id) {
        const url = "api/channel/getChannel/" + id;
        return this.service.baseGet(url);
    }
    deleteChannel(payload) {
        console.log(payload);
        const url = "api/channel/deleteChannel";
        return this.service.basePost(url, payload);
    }
}
ChannelService.ɵfac = function ChannelService_Factory(t) { return new (t || ChannelService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
ChannelService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ChannelService, factory: ChannelService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "xlh6":
/*!**************************************!*\
  !*** ./src/app/_files/appadmin.json ***!
  \**************************************/
/*! exports provided: 0, 1, 2, 3, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"label\":\"Dashboard\",\"icon\":\"pi pi-fw pi-home\",\"routerLink\":\"/mgm/root\"},{\"label\":\"Settings\",\"expanded\":true,\"items\":[{\"label\":\"BIC Administrator\",\"icon\":\"pi pi-fw pi-microsoft\",\"routerLink\":\"/mgm/root/appgroup\"},{\"label\":\"Proxy Manager\",\"icon\":\"pi pi-fw pi-bookmark\",\"routerLink\":\"/mgm/root/users\"}]},{\"label\":\"Transaction Monitor\",\"icon\":\"pi pi-fw pi-chart-line\",\"routerLink\":\"/mgm/root/appgroup\"},{\"label\":\"Reports\",\"expanded\":true,\"items\":[{\"label\":\"Transactions\",\"icon\":\"pi pi-fw pi-download\",\"routerLink\":\"/mgm/root/eventlog\"}]}]");

/***/ }),

/***/ "z+Ab":
/*!*********************************************************************!*\
  !*** ./src/app/pages/root/users/userdetail/userdetail.component.ts ***!
  \*********************************************************************/
/*! exports provided: UserdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserdetailComponent", function() { return UserdetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/usermanager.service */ "4nGI");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/root/group-service.service */ "XfbB");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/forgotpassword/forgotpassword.service */ "Jdzd");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/autocomplete */ "V5BG");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/dropdown */ "arFO");


















function UserdetailComponent_form_3_div_10_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "User Name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_3_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_3_div_10_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r1.f.username.errors.required);
} }
function UserdetailComponent_form_3_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Email is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_3_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_3_div_16_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r2.f.emailname.errors.required);
} }
function UserdetailComponent_form_3_div_22_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Fullname is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_3_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_3_div_22_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r3.f.fullname.errors.required);
} }
function UserdetailComponent_form_3_div_30_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Group Menus & Acl are required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_3_div_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_3_div_30_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r4.f.orgMlt.errors.required);
} }
function UserdetailComponent_form_3_div_36_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Initial Secret is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_3_div_36_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_3_div_36_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r5.f.temppass.errors.required);
} }
function UserdetailComponent_form_3_div_38_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Activate user :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](4, "p-dropdown", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("options", ctx_r6.stateOptions);
} }
function UserdetailComponent_form_3_div_39_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Activate user :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](4, "p-dropdown", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("options", ctx_r7.stateOptionsEdit);
} }
function UserdetailComponent_form_3_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "form", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngSubmit", function UserdetailComponent_form_3_Template_form_ngSubmit_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r13.onSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "p-card", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "label", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, "User name * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "input", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, UserdetailComponent_form_3_div_10_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](11, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "label", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](14, "Email * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](15, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](16, UserdetailComponent_form_3_div_16_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](17, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "label", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](20, "Full name * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](21, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](22, UserdetailComponent_form_3_div_22_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](23, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](25, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](26, "label", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](27, "Group Menus & Acl * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](28, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](29, "p-autoComplete", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("completeMethod", function UserdetailComponent_form_3_Template_p_autoComplete_completeMethod_29_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r14); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r15.filterOrg($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](30, UserdetailComponent_form_3_div_30_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](31, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](32, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](33, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](34, "Initial Secret * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](35, "input", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](36, UserdetailComponent_form_3_div_36_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](37, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](38, UserdetailComponent_form_3_div_38_Template, 5, 1, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](39, UserdetailComponent_form_3_div_39_Template, 5, 1, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](40, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](41, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](42, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](43, "button", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("formGroup", ctx_r0.groupForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.username.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.emailname.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.fullname.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("suggestions", ctx_r0.orgSuggest)("multiple", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.orgMlt.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.temppass.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !ctx_r0.isEdit);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.isEdit);
} }
class UserdetailComponent {
    constructor(router, activatedRoute, formBuilder, umService, authservice, groupService, filterService, messageService, location, verifikasiService) {
        var _a;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.umService = umService;
        this.authservice = authservice;
        this.groupService = groupService;
        this.filterService = filterService;
        this.messageService = messageService;
        this.location = location;
        this.verifikasiService = verifikasiService;
        this.extraInfo = {};
        this.isEdit = false;
        this.userId = null;
        this.stateOptions = [];
        this.stateOptionsEdit = [];
        this.leveltenant = 0;
        this.userInfo = {};
        this.selectedApps = [];
        this.tokenID = '';
        this.submitted = false;
        this.orgsData = [];
        this.appInfoActive = {};
        this.orgSuggest = {};
        this.user = {};
        this.formatedOrg = [];
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
        console.log('>>>>>>>>>>> ' + this.extraInfo);
        console.log(checkurl);
        if (checkurl)
            this.isEdit = true;
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
        this.breadcrumbs = [
            {
                label: 'Users Management',
                command: (event) => {
                    this.location.back();
                },
                url: '',
            },
            { label: this.isEdit ? 'Edit data' : 'Add data' },
        ];
        this.stateOptions = [
            { label: 'Direct activated', value: 1 },
            { label: 'Activision link', value: 0 },
        ];
        this.stateOptionsEdit = [
            { label: 'Active', value: 1 },
            { label: 'Deactive', value: 0 },
        ];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.leveltenant = this.userInfo.leveltenant;
            this.groupForm = this.formBuilder.group({
                username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                emailname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                fullname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                orgMlt: [[], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                temppass: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                isactive: [0],
            });
            this.groupService
                .getAllGroupForData()
                .subscribe((grpall) => {
                this.orgsData = grpall.data.userGroup;
                // console.log(JSON.stringify(this.orgsData));
                this.formatOrgData(this.orgsData);
            });
            if (this.isEdit) {
                if (this.activatedRoute.snapshot.paramMap.get('id')) {
                    this.userId = this.activatedRoute.snapshot.paramMap.get('id');
                    this.umService
                        .retriveUsersById(this.userId)
                        .subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        // console.log('Data edit user ' + JSON.stringify(result.data));
                        // if (this.isOrganization) {
                        this.user.username = result.data.userid;
                        this.user.fullname = result.data.fullname;
                        this.user.emailname = result.data.bioemailactive;
                        this.user.org = result.data.id;
                        this.user.password = result.data.pwd;
                        this.user.active = result.data.active;
                        this.groupForm.patchValue({
                            username: this.user.username,
                            fullname: this.user.fullname,
                            emailname: this.user.emailname,
                            //groupobj: this.user.group,
                            //orgobj: this.user.org,
                            orgMlt: result.data.group,
                            temppass: this.user.password,
                            isactive: this.user.active,
                        });
                        this.groupForm.controls['username'].disable();
                        // console.log(this.user);
                    }));
                }
            }
        });
    }
    get f() {
        return this.groupForm.controls;
    }
    formatOrgData(data) {
        data.map((dt) => {
            let formated = {};
            formated.name = dt.groupname;
            formated.id = dt.id;
            this.formatedOrg.push(formated);
        });
    }
    filterOrg(event) {
        let filtered = [];
        let query = event.query;
        for (let i = 0; i < this.formatedOrg.length; i++) {
            let country = this.formatedOrg[i];
            if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
                filtered.push(country);
            }
        }
        this.orgSuggest = filtered;
    }
    onSubmit() {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
        this.submitted = true;
        // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
        // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
        if (this.groupForm.valid) {
            // event?.preventDefault;
            var groupacl = (_a = this.groupForm.get('orgobj')) === null || _a === void 0 ? void 0 : _a.value;
            let payload = {};
            let payload2 = {};
            if (!this.isEdit) {
                payload = {
                    fullname: (_b = this.groupForm.get('fullname')) === null || _b === void 0 ? void 0 : _b.value,
                    userid: (_c = this.groupForm.get('username')) === null || _c === void 0 ? void 0 : _c.value,
                    password: (_d = this.groupForm.get('temppass')) === null || _d === void 0 ? void 0 : _d.value,
                    group: (_e = this.groupForm.get('orgMlt')) === null || _e === void 0 ? void 0 : _e.value,
                    isactive: (_f = this.groupForm.get('isactive')) === null || _f === void 0 ? void 0 : _f.value,
                    emailname: (_g = this.groupForm.get('emailname')) === null || _g === void 0 ? void 0 : _g.value,
                };
                payload2 = {
                    email: this.groupForm.value['emailname'],
                };
                // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
                this.umService.insertByAdmin(payload).subscribe((result) => {
                    if (result.status === 200) {
                        // this.verifikasiService.verifikasi(payload2).subscribe(
                        //   (result: BackendResponse) => {
                        //     if (result.status === 200) {
                        this.location.back();
                        //     }
                        //   },
                        //   (err) => {
                        //     console.log(err);
                        //     this.showTopCenterErr(err.error.data);
                        //   }
                        // );
                    }
                }, (err) => {
                    console.log(err);
                    this.showTopCenterErr(err.error.data);
                });
            }
            else {
                payload = {
                    fullname: (_h = this.groupForm.get('fullname')) === null || _h === void 0 ? void 0 : _h.value,
                    userid: this.userId,
                    group: (_j = this.groupForm.get('orgMlt')) === null || _j === void 0 ? void 0 : _j.value,
                    isactive: (_k = this.groupForm.get('isactive')) === null || _k === void 0 ? void 0 : _k.value,
                    emailname: (_l = this.groupForm.get('emailname')) === null || _l === void 0 ? void 0 : _l.value,
                };
                console.log('>>>>>>>> payload ' + JSON.stringify(payload));
                this.umService
                    .updatebyAdmin(payload)
                    .subscribe((result) => {
                    // console.log(">>>>>>>> return "+JSON.stringify(result));
                    if (result.status === 200) {
                        this.location.back();
                    }
                });
            }
        }
        // console.log(this.groupForm.valid);
    }
    showTopCenterErr(message) {
        this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: message,
        });
    }
}
UserdetailComponent.ɵfac = function UserdetailComponent_Factory(t) { return new (t || UserdetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__["UsermanagerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_root_group_service_service__WEBPACK_IMPORTED_MODULE_6__["GroupServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["FilterService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_forgotpassword_forgotpassword_service__WEBPACK_IMPORTED_MODULE_9__["ForgotpasswordService"])); };
UserdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: UserdetailComponent, selectors: [["app-userdetail"]], decls: 4, vars: 3, consts: [[3, "model", "home"], [1, "wrapperinside"], ["style", "padding: 2px;", 3, "formGroup", "ngSubmit", 4, "ngIf"], [2, "padding", "2px", 3, "formGroup", "ngSubmit"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "username", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "username", "formControlName", "username", "id", "username", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "p-field", 4, "ngIf"], [2, "height", "15px"], ["for", "emailname", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "emailname", "formControlName", "emailname", "id", "emailname", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "fullname", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "fullname", "formControlName", "fullname", "id", "fullname", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "orgMlt", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-10"], [2, "height", "8px"], ["formControlName", "orgMlt", "field", "name", 1, "autosrc", 3, "suggestions", "multiple", "completeMethod"], ["for", "temppass", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "temppass", "formControlName", "temppass", "id", "temppass", "type", "password", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "box", 4, "ngIf"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success"], [1, "p-field"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], ["id", "isactive", "formControlName", "isactive", "optionLabel", "label", "optionValue", "value", 3, "options"]], template: function UserdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, UserdetailComponent_form_3_Template, 44, 10, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("model", ctx.breadcrumbs)("home", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.groupForm);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["Breadcrumb"], primeng_messages__WEBPACK_IMPORTED_MODULE_11__["Messages"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_12__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], primeng_autocomplete__WEBPACK_IMPORTED_MODULE_14__["AutoComplete"], primeng_button__WEBPACK_IMPORTED_MODULE_15__["ButtonDirective"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_16__["Dropdown"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2VyZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map